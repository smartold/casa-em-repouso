// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

//SE FOR FAZER UM DEBUG PRA MOBILE, NÃO ESQUEÇA DE TROCAR A URL DA API PARA AQUELA DEFINIDA NO Program.cs

export const environment = {
  production: false,
  urlApi: 'http://localhost:5000/',
  urlFront: 'http://localhost:4200/',
  webSocket: 'ws://localhost:5000/'
};

// export const environment = {
//   production: true,
//   urlApi: 'https://doctorapi.azurewebsites.net/',
//   urlFront: 'https://doctorweb.azurewebsites.net/'
// };


//Para Debug usando o seu IP
// export const environment = {
//   production: false,
//   urlApi: 'http://192.168.0.228:5000/',
//   urlFront: 'http://192.168.0.228:4200/'
// };