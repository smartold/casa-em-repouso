export const environment = {
  production: true,
  urlApi: 'http://localhost:5000/',
  urlFront: 'http://localhost:4200/',
  webSocket: 'ws://localhost:5000/'
};
