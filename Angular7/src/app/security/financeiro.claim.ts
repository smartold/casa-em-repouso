import { Injectable } from "@angular/core";
import { Menu } from "./menu";

@Injectable()
export class FinanceiroClaims {

    private menu: Menu;

    constructor() {

        this.menu = new Menu();
    }

    public definirMenu(): Menu {

        this.menu.novoOrcamento = false;

        //cadastros
        this.menu.cadastros.atendente = false;
        this.menu.cadastros.supervisor = false;
        this.menu.cadastros.tecnico = false;
        this.menu.cadastros.usuario = false;

        //vendas
        this.menu.vendas.cliente = true;
        this.menu.vendas.comissometro = false;
        this.menu.vendas.ordemServico = true;
        this.menu.vendas.resumo = false;
        this.menu.vendas.baixapedidoagendamento = true;
        this.menu.vendas.simulador = true;
        this.menu.vendas.controleLoja = true;
        this.menu.vendas.margenspreco = false;
        this.menu.vendas.enviodelink = false;

        //institucional
        this.menu.institucional.tutorial = true;
        this.menu.institucional.playlists = true;
        this.menu.institucional.videos = true;
        this.menu.institucional.alertas = true;
        this.menu.institucional.unidades = false;
        this.menu.institucional.modeloEmail = true;

        //marketing
        this.menu.marketing.cupomDesconto = false;

        //relatorios
        this.menu.relatorios.vendas = true;
        this.menu.relatorios.descontos = false;
        this.menu.relatorios.orcamentos = true;
        this.menu.relatorios.produtividadeAtendentes = true;
        this.menu.relatorios.produtividadeTecnicos = true;
        this.menu.relatorios.produtividadeSomadas = true;
        this.menu.relatorios.whatsapp = false;
        this.menu.relatorios.fechamentoMensal = true;
        this.menu.relatorios.confirmacao = true;
        this.menu.relatorios.relatorioNii = true;
        this.menu.relatorios.relatorioPagamento = true;
        this.menu.relatorios.comissaoAtendentes = true;
        this.menu.relatorios.comissaoTecnicos = true;
        this.menu.relatorios.produtividadeServicoExterno = true;
        this.menu.relatorios.perdasPorAgendamento = true;
        this.menu.relatorios.perdasPorItem = true;
        this.menu.relatorios.perdasPorOs = true;
        this.menu.relatorios.relatorioEvitarPerdas = true;
        this.menu.relatorios.clientePrePosRota = true;
        this.menu.relatorios.origemCliente = true;
        this.menu.relatorios.recorrenciaCliente = true;
        this.menu.relatorios.ageAgendamento = true;
        this.menu.relatorios.conciliadora = true;

        //Configuracao
        this.menu.configuracao.tasks = false;
        this.menu.configuracao.token = false;

        return this.menu;
    }
}