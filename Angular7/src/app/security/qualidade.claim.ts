import { Injectable } from "@angular/core";
import { Menu } from "./menu";

@Injectable()
export class QualidadeClaims {

    private menu: Menu;

    constructor() {

        this.menu = new Menu();
    }

    public definirMenu(): Menu {

        this.menu.novoOrcamento = true;

        //cadastros
        this.menu.cadastros.atendente = true;
        this.menu.cadastros.supervisor = false;
        this.menu.cadastros.tecnico = false;
        this.menu.cadastros.usuario = false;

        //vendas
        this.menu.vendas.cliente = true;
        this.menu.vendas.comissometro = false;
        this.menu.vendas.ordemServico = true;
        this.menu.vendas.resumo = false;
        this.menu.vendas.baixapedidoagendamento = false;
        this.menu.vendas.simulador = false;
        this.menu.vendas.controleLoja = true;
        this.menu.vendas.margenspreco = false;
        this.menu.vendas.enviodelink = false;

        //institucional
        this.menu.institucional.tutorial = true;
        this.menu.institucional.playlists = true;
        this.menu.institucional.videos = true;
        this.menu.institucional.alertas = true;
        this.menu.institucional.unidades = true;
        this.menu.institucional.modeloEmail = true;

        //marketing
        this.menu.marketing.cupomDesconto = true;

        //relatorios
        this.menu.relatorios.vendas = true;
        this.menu.relatorios.descontos = true;
        this.menu.relatorios.orcamentos = true;
        this.menu.relatorios.produtividadeAtendentes = false;
        this.menu.relatorios.produtividadeTecnicos = false;
        this.menu.relatorios.produtividadeSomadas = false;
        this.menu.relatorios.whatsapp = false;
        this.menu.relatorios.fechamentoMensal = false;
        this.menu.relatorios.relatorioNii = true;
        this.menu.relatorios.relatorioPagamento = false;
        this.menu.relatorios.comissaoAtendentes = true;
        this.menu.relatorios.comissaoTecnicos = true;
        this.menu.relatorios.confirmacao = true;
        this.menu.relatorios.produtividadeServicoExterno = true;
        this.menu.relatorios.perdasPorAgendamento = true;
        this.menu.relatorios.perdasPorItem = true;
        this.menu.relatorios.perdasPorOs = true;
        this.menu.relatorios.relatorioEvitarPerdas = true;
        this.menu.relatorios.clientePrePosRota = true;
        this.menu.relatorios.origemCliente = true;
        this.menu.relatorios.recorrenciaCliente = true;
        this.menu.relatorios.ageAgendamento = true;

        //logistica
        this.menu.logistica.agenda = true;
        this.menu.logistica.bloqueio = true;
        this.menu.logistica.compromisso = true;
        this.menu.logistica.rotas = true;
        this.menu.logistica.calendarioTecnico = true;
        this.menu.logistica.reportTecnico = true;

        //Configuracao
        this.menu.configuracao.tasks = false;
        this.menu.configuracao.token = false;

        return this.menu;
    }
}