import { Injectable } from "@angular/core";
import { Menu } from "./menu";

@Injectable()
export class AtendenteLiderClaims {

    private menu: Menu;

    constructor() {

        this.menu = new Menu();
    }

    public definirMenu(): Menu {

        this.menu.novoOrcamento = true;

        //cadastros
        this.menu.cadastros.atendente = true;
        this.menu.cadastros.supervisor = false;
        this.menu.cadastros.tecnico = true;
        this.menu.cadastros.usuario = false;

        //vendas
        this.menu.vendas.cliente = true;
        this.menu.vendas.comissometro = true;
        this.menu.vendas.ordemServico = true;
        this.menu.vendas.resumo = false;
        this.menu.vendas.baixapedidoagendamento = false;
        this.menu.vendas.simulador = false;
        this.menu.vendas.controleLoja = true;
        this.menu.vendas.margenspreco = false;
        this.menu.vendas.enviodelink = false;

        //institucional
        this.menu.institucional.tutorial = true;
        this.menu.institucional.playlists = true;
        this.menu.institucional.videos = true;
        this.menu.institucional.alertas = true;
        this.menu.institucional.unidades = false;
        this.menu.institucional.modeloEmail = true;

        //marketing
        this.menu.marketing.cupomDesconto = true;

        //relatorios
        this.menu.relatorios.vendas = true;
        this.menu.relatorios.descontos = true;
        this.menu.relatorios.orcamentos = true;
        this.menu.relatorios.produtividadeAtendentes = true;
        this.menu.relatorios.produtividadeTecnicos = false;
        this.menu.relatorios.produtividadeSomadas = false;
        this.menu.relatorios.comissaoAtendentes = true;
        this.menu.relatorios.comissaoTecnicos = false;
        this.menu.relatorios.produtividadeServicoExterno = false;

        //logistica
        this.menu.logistica.agenda = true;
        this.menu.logistica.bloqueio = true;
        this.menu.logistica.compromisso = true;
        this.menu.logistica.rotas = true;
        this.menu.relatorios.confirmacao = false;
        this.menu.logistica.calendarioTecnico = false;
        this.menu.logistica.reportTecnico = false;

        //Configuracao
        this.menu.configuracao.tasks = false;
        this.menu.configuracao.token = false;

        return this.menu;
    }
}