import { Injectable } from '@angular/core';
import { RESOURCE_CACHE_PROVIDER } from '@angular/platform-browser-dynamic';

export class Menu {

    novoOrcamento: any;
    cadastros: Cadastros;
    vendas: Vendas;
    institucional: Institucional;
    marketing: Marketing;
    relatorios: Relatorios;
    logistica: Logistica;
    configuracao: Configuracao;

    constructor() {

        this.novoOrcamento = false;
        this.cadastros = new Cadastros();
        this.vendas = new Vendas();
        this.institucional = new Institucional();
        this.marketing = new Marketing();
        this.relatorios = new Relatorios();
        this.logistica = new Logistica();
        this.configuracao = new Configuracao();
    }
}

export class Cadastros {

    atendente: boolean;
    supervisor: boolean;
    tecnico: boolean;
    usuario: boolean;
    consultor: boolean;

    constructor() {

        this.atendente = false;
        this.supervisor = false;
        this.tecnico = false;
        this.usuario = false;
        this.consultor = false;
    }

}

export class Vendas {

    cliente: boolean;
    comissometro: boolean;
    ordemServico: boolean;
    resumo: boolean;
    baixapedidoagendamento: boolean;
    simulador: boolean;
    controleLoja: boolean;
    enviodelink: boolean;
    margenspreco: boolean;

    constructor() {

        this.cliente = false;
        this.comissometro = false;
        this.ordemServico = false;
        this.resumo = false;
        this.baixapedidoagendamento = false;
        this.simulador = false;
        this.controleLoja = true;
        this.enviodelink = false;
        this.margenspreco = false;
    }
}

export class Institucional {

    alertas: boolean;
    tutorial: boolean;
    playlists: boolean;
    videos: boolean;
    unidades: boolean;
    modeloEmail: boolean;

    constructor() {

        this.alertas = false;
        this.tutorial = false;
        this.playlists = false;
        this.videos = false;
        this.unidades = false;
        this.modeloEmail = false;
    }
}

export class Marketing {

    cupomDesconto: boolean;

    constructor() {

        this.cupomDesconto = false;
    }
}

export class Relatorios {

    vendas: boolean;
    descontos: boolean;
    orcamentos: boolean;
    produtividadeAtendentes: boolean;
    produtividadeTecnicos: boolean;
    produtividadeSomadas: boolean;
    produtividadeServicoExterno: boolean;
    whatsapp: boolean;
    fechamentoMensal: boolean;
    confirmacao: boolean;
    relatorioNii: boolean;
    relatorioPagamento: boolean;
    comissaoAtendentes: boolean;
    comissaoTecnicos: boolean;
    perdasPorAgendamento: boolean;
    perdasPorItem: boolean;
    perdasPorOs: boolean;
    relatorioEvitarPerdas: boolean;
    clientePrePosRota: boolean;
    origemCliente: boolean;
    recorrenciaCliente: boolean;
    ageAgendamento: boolean;
    conciliadora: boolean
    constructor() {

        this.vendas = false;
        this.descontos = false;
        this.orcamentos = false;
        this.produtividadeAtendentes = false;
        this.produtividadeTecnicos = false;
        this.produtividadeSomadas = false;
        this.whatsapp = false;
        this.fechamentoMensal = false;
        this.confirmacao = false;
        this.relatorioNii = false;
        this.relatorioPagamento = false;
        this.comissaoAtendentes = false;
        this.comissaoTecnicos = false;
        this.produtividadeServicoExterno = false;
        this.perdasPorAgendamento = false;
        this.perdasPorItem = false;
        this.perdasPorOs = false;
        this.relatorioEvitarPerdas = false;
        this.clientePrePosRota = false;
        this.origemCliente = false;
        this.recorrenciaCliente = false;
        this.ageAgendamento = false;
        this.conciliadora = false;
    }
}

export class Logistica {

    rotas: boolean;
    bloqueio: boolean;
    compromisso: boolean;
    agenda: boolean;
    bloqueioAreaRisco: boolean;
    calendarioTecnico: boolean;
    reportTecnico: boolean;

    constructor() {

        this.rotas = false;
        this.bloqueio = false;
        this.compromisso = false;
        this.agenda = false;
        this.bloqueioAreaRisco = false;
        this.calendarioTecnico = false;
        this.reportTecnico = false;
    }
}

export class Configuracao {

    tasks: boolean;
    token: boolean;

    constructor() {

        this.tasks = false;
        this.token = false;
    }
}