import { Injectable } from "@angular/core";
import { Menu } from "./menu";

@Injectable()
export class FranquiaClaims {

    private menu: Menu;

    constructor() {

        this.menu = new Menu();
    }

    public definirMenu(): Menu {

        this.menu.novoOrcamento = true;

        //cadastros
        this.menu.cadastros.atendente = false;
        this.menu.cadastros.supervisor = false;
        this.menu.cadastros.tecnico = true;
        this.menu.cadastros.usuario = false;

        //vendas
        this.menu.vendas.cliente = true;
        this.menu.vendas.comissometro = false;
        this.menu.vendas.ordemServico = true;
        this.menu.vendas.resumo = true;
        this.menu.vendas.baixapedidoagendamento = false;
        this.menu.vendas.simulador = true;
        this.menu.vendas.controleLoja = true;
        this.menu.vendas.margenspreco = false;
        this.menu.vendas.enviodelink = false;

        //institucional
        this.menu.institucional.tutorial = true;
        this.menu.institucional.playlists = true;
        this.menu.institucional.videos = true;
        this.menu.institucional.alertas = true;
        this.menu.institucional.unidades = false;
        this.menu.institucional.modeloEmail = false;

        //marketing
        this.menu.marketing.cupomDesconto = false;

        //relatorios
        this.menu.relatorios.vendas = true;
        this.menu.relatorios.descontos = true;
        this.menu.relatorios.orcamentos = true;
        this.menu.relatorios.produtividadeAtendentes = true;
        this.menu.relatorios.produtividadeTecnicos = true;
        this.menu.relatorios.produtividadeSomadas = true;
        this.menu.relatorios.whatsapp = true;
        this.menu.relatorios.fechamentoMensal = true;
        this.menu.relatorios.confirmacao = true;
        this.menu.relatorios.relatorioNii = true;
        this.menu.relatorios.relatorioPagamento = false;
        this.menu.relatorios.comissaoAtendentes = false;
        this.menu.relatorios.comissaoTecnicos = false;
        this.menu.relatorios.perdasPorAgendamento = true;
        this.menu.relatorios.perdasPorItem = true;
        this.menu.relatorios.perdasPorOs = true;
        this.menu.relatorios.relatorioEvitarPerdas = true;
        this.menu.relatorios.clientePrePosRota = true;
        this.menu.relatorios.origemCliente = true;
        this.menu.relatorios.recorrenciaCliente = true;
        this.menu.relatorios.ageAgendamento = true;

        //Configuracao
        this.menu.configuracao.tasks = false;
        this.menu.configuracao.token = false;

        return this.menu;
    }
}