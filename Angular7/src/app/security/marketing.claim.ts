import { Injectable } from "@angular/core";
import { Menu } from "./menu";

@Injectable()
export class MarketingClaims {

    private menu: Menu;

    constructor() {

        this.menu = new Menu();
    }

    public definirMenu(): Menu {

        this.menu.novoOrcamento = false;

        //cadastros
        this.menu.cadastros.atendente = false;
        this.menu.cadastros.supervisor = false;
        this.menu.cadastros.tecnico = false;
        this.menu.cadastros.usuario = false;

        //vendas
        this.menu.vendas.cliente = true;
        this.menu.vendas.comissometro = false;
        this.menu.vendas.ordemServico = true;
        this.menu.vendas.resumo = false;
        this.menu.vendas.baixapedidoagendamento = false;
        this.menu.vendas.simulador = true;
        this.menu.vendas.controleLoja = true;
        this.menu.vendas.margenspreco = false;
        this.menu.vendas.enviodelink = false;

        //institucional
        this.menu.institucional.tutorial = true;
        this.menu.institucional.playlists = true;
        this.menu.institucional.videos = true;
        this.menu.institucional.alertas = true;
        this.menu.institucional.unidades = true;
        this.menu.institucional.modeloEmail = true;

        //marketing
        this.menu.marketing.cupomDesconto = true;

        //relatorios
        this.menu.relatorios.vendas = true;
        this.menu.relatorios.descontos = true;
        this.menu.relatorios.orcamentos = true;
        this.menu.relatorios.produtividadeAtendentes = false;
        this.menu.relatorios.produtividadeTecnicos = false;
        this.menu.relatorios.produtividadeSomadas = false;
        this.menu.relatorios.whatsapp = false;
        this.menu.relatorios.fechamentoMensal = true;
        this.menu.relatorios.confirmacao = true;
        this.menu.relatorios.comissaoAtendentes = false;
        this.menu.relatorios.comissaoTecnicos = false;

        //Configuracao
        this.menu.configuracao.tasks = false;
        this.menu.configuracao.token = false;

        return this.menu;
    }
}