import { Injectable } from "@angular/core";
import { Menu } from "./menu";

@Injectable()
export class AtendenteClaims {

    private menu: Menu;

    constructor() {

        this.menu = new Menu();
    }

    public definirMenu(): Menu {

        this.menu.novoOrcamento = true;

        //cadastros
        this.menu.cadastros.atendente = true;
        this.menu.cadastros.supervisor = false;
        this.menu.cadastros.tecnico = false;
        this.menu.cadastros.usuario = false;

        //vendas
        this.menu.vendas.cliente = true;
        this.menu.vendas.comissometro = true;
        this.menu.vendas.ordemServico = true;
        this.menu.vendas.resumo = false;
        this.menu.vendas.baixapedidoagendamento = false;
        this.menu.vendas.simulador = false;
        this.menu.vendas.controleLoja = false;
        this.menu.vendas.margenspreco = false;
        this.menu.vendas.enviodelink = false;

        //institucional
        this.menu.institucional.tutorial = true;
        this.menu.institucional.playlists = false;
        this.menu.institucional.videos = false;
        this.menu.institucional.alertas = true;
        this.menu.institucional.unidades = false;
        this.menu.institucional.modeloEmail = false;

        //marketing
        this.menu.marketing.cupomDesconto = true;

        //relatorios
        this.menu.relatorios.vendas = true;
        this.menu.relatorios.descontos = false;
        this.menu.relatorios.orcamentos = false;
        this.menu.relatorios.produtividadeAtendentes = false;
        this.menu.relatorios.produtividadeTecnicos = false;
        this.menu.relatorios.produtividadeSomadas = false;
        this.menu.relatorios.confirmacao = false;
        this.menu.relatorios.comissaoAtendentes = false;
        this.menu.relatorios.comissaoTecnicos = false;
        this.menu.relatorios.produtividadeServicoExterno = false;

        //Configuracao
        this.menu.configuracao.tasks = false;
        this.menu.configuracao.token = false;

        return this.menu;
    }
}