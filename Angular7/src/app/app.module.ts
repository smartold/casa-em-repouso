import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
import { RegistroComponent } from "./pages/registro/registro.component";
import { AppRoutingModule } from "./app-routing.module";
import { LoginComponent } from "./pages/login/login.component";
import { HomeComponent } from "./pages/home/home.component";
import { ContentComponent } from "./components/shared/content/content.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DataService } from "./services/data.service";
import { BlockUiComponentsComponent } from "./components/shared/block-ui/block-ui-components/block-ui-components.component";
import { RegistroService } from "./services/registro.service";
import { ErroService } from "./services/erro.service";
import { AuthService } from "./services/auth.service";
import { ValuesService } from "./services/values.service";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { ToastrModule } from "ngx-toastr";
import { ListaPacienteComponent } from "./pages/paciente/lista-paciente/lista-paciente.component";
import { IdosoeComponent } from "./pages/paciente/idosoe-page/idosoe-page.component";
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { EventosComponent } from './pages/eventos/eventos.component';
import { ActionMenuComponent } from './components/shared/action-menu/action-menu.component';
import { SubMenuComponent } from './components/shared/sub-menu/sub-menu.component';
import { EventosService } from './services/eventos.service';
import { SegmentComponent } from './components/shared/segment/segment.component';
import { UsuarioService } from './services/usuario.service';
import { DadosResponsavelComponent } from './components/shared/dados-responsavel/dados-responsavel.component';
import { SegmentActionMenuComponent } from './components/shared/segment-action-menu/segment-action-menu.component';
import { DataTablesModule } from "angular-datatables";
import { ModalExcluirComponent } from './components/shared/modais/modal-excluir/modal-excluir.component';
import { SuiModule } from 'ng2-semantic-ui';
import { CuidadoraComponent } from './pages/atendimento/cuidadora/cuidadora.component';
import { EtapasAtendimentoComponent } from './pages/atendimento/etapas-atendimento/etapas-atendimento.component';
import { IdentificacaoIdosoComponent } from './pages/atendimento/identificacao-idoso/identificacao-idoso.component';
import { ModalRemedioComponent } from './components/shared/modais/modal-remedio/modal-remedio.component';
import { ProntuarioComponent } from './pages/atendimento/prontuario/prontuario.component';
import { RelatoriosComponent } from './pages/relatorios/relatorios.component';
import { CuidadoraeComponent } from './pages/cuidadora/cuidadorae/cuidadorae.component';
import { CuidadoralComponent } from './pages/cuidadora/cuidadoral/cuidadoral.component';
import { ProntuariolComponent } from './pages/atendimento/prontuario/prontuariol/prontuariol.component';

export interface IContext {
  data: string;
}

@NgModule({
  declarations: [
    AppComponent,
    RegistroComponent,
    LoginComponent,
    HomeComponent,
    ContentComponent,
    ListaPacienteComponent,
    IdosoeComponent,
    NavbarComponent,
    EventosComponent,
    ActionMenuComponent,
    SubMenuComponent,
    SegmentComponent,
    DadosResponsavelComponent,
    SegmentActionMenuComponent,
    ModalExcluirComponent,
    CuidadoraComponent,
    EtapasAtendimentoComponent,
    IdentificacaoIdosoComponent,
    ModalRemedioComponent,
    ProntuarioComponent,
    RelatoriosComponent,
    CuidadoraeComponent,
    CuidadoralComponent,
    ProntuariolComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: "toast-top-right"
    }),
    DataTablesModule,
    SuiModule
  ],
  providers: [
    DataService,
    ValuesService,
    AuthService,
    ErroService,
    BlockUiComponentsComponent,
    EventosService,
    UsuarioService,
    SuiModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
