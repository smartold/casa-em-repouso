import { Injectable } from '@angular/core';
import { DataService } from "./data.service";
import { HttpParams } from "@angular/common/http";

@Injectable()

export class EventosService {

  constructor(private dataService: DataService) { }

  public obterEvento() {

    return this.dataService.get('api/evento', new HttpParams());
  }

  public obterEventoPorGuid(guid: string) {

    let param = new HttpParams().
      set('guid', guid);

    return this.dataService.get("api/evento/guid", param);
  }

  public postImagem(files: any) {

    let formData = new FormData();

    formData.append("files", files[0]);

    return this.dataService.postImage("api/evento/salvarfoto", formData);
  }



  public getImagem(guid) {

    let param = new HttpParams()
      .set('guid', guid);

    return this.dataService.getImage("api/evento/carregarfoto", param);
  }

  public salvar(evento) {

    return this.dataService.post("api/evento", evento);
  }

  public atualizar(evento) {

    return this.dataService.put("api/evento", evento);
  }

  public excluirEvento(guid) {

    let param = new HttpParams()
      .set('guid', guid);

    return this.dataService.delete("api/evento", param);
  }
}
