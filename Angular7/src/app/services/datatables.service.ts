import { Injectable } from '@angular/core';

@Injectable()

export class DatatablesService {

  constructor() { }
  public rerender(dtElement, dtTrigger) {

    if (dtElement.dtInstance != undefined) {

      dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();

        dtTrigger.next();
      });
    }
    else {

      dtTrigger.next();
    }
  }

  public destroy(dtElement, dtTrigger) {

    if (dtElement.dtInstance != undefined) {

      dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
    }
  }

  public getTraducaoTable() {

    let lng: DataTables.LanguageSettings = {};
    let pag: DataTables.LanguagePaginateSettings = {

      first: "Primeiro",
      previous: "Anterior",
      next: "Próximo",
      last: "Último"
    };

    lng.emptyTable = "Nenhum registro encontrado";
    lng.info = "Mostrando de _START_ até _END_ de _TOTAL_ registros";
    lng.infoEmpty = "Mostrando 0 até 0 de 0 registros";
    lng.infoFiltered = "(Filtrados de _MAX_ registros)";
    lng.infoPostFix = "";
    lng.thousands = ".";
    lng.lengthMenu = "_MENU_ resultados por página";
    lng.loadingRecords = "Carregando...";
    lng.processing = "Processando...";
    lng.zeroRecords = "Nenhum registro encontrado";
    lng.search = "Pesquisar ";

    lng.paginate = pag;

    return lng;
  }
}
