import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { DataService } from './data.service';

@Injectable()

export class PacienteService {
  constructor(private dataService: DataService) { }

  public listarIdosos() {
    return this.dataService.get('api/paciente/obterListagemPacientes', new HttpParams());
  }


  public obterPacientePorGuid(guid: string) {

    let param = new HttpParams().
      set('guid', guid);

    return this.dataService.get("api/paciente/guid", param);
  }

  public postImagem(files: any) {

    let formData = new FormData();

    formData.append("files", files[0]);

    return this.dataService.postImage("api/v1/evento/salvarfoto", formData);
  }

  public getImagem(guid) {

    let param = new HttpParams()
      .set('guid', guid);

    return this.dataService.getImage("api/v1/evento/carregarfoto", param);
  }

  public salvar(paciente) {
    return this.dataService.post("api/paciente/salvar", paciente);
  }

  public atualizar(paciente) {

    return this.dataService.put("api/paciente/atualizar", paciente);
  }

  public excluirPaciente(guid) {

    let param = new HttpParams()
      .set('guid', guid);

    return this.dataService.delete("api/paciente", param);
  }

  public identificarPaciente(idoso) {
    let param = new HttpParams()

      .set('cpf', idoso.cpf == undefined ? "" : idoso.cpf)
      .set('nome', idoso.nome == undefined ? "" : idoso.nome);

    return this.dataService.get("api/paciente/identificar", param);
  }
}
