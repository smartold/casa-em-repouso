import { DataService } from "./data.service";
import { SecurityService } from "./security.service";
import { Injectable } from "@angular/core";

import { Observable } from 'rxjs';
import { HttpParams } from "@angular/common/http";
import { Router } from "@angular/router";

@Injectable()
export class UsuarioService {

    private _usuario: any;
    private menu: any = {};
    private securityService: SecurityService;

    constructor(private dataService: DataService,
        private router: Router, ) {

        this.securityService = new SecurityService();
    }

    public obterDadosUsuarioLocalStorage() {

        let usuarioLocalStorage = localStorage.getItem('user');

        if (usuarioLocalStorage == undefined)
            this.router.navigate(['/login']);
        else
            return JSON.parse(usuarioLocalStorage);
    }

    // public obterNivelAcessoUsuario() {

    //     return this.dataService.get('api/v1/usuarios/nivelAcesso', new HttpParams());
    // }

    public listarUsuarios() {

        return this.dataService.get('api/v1/usuarios/listarUsuarios', new HttpParams());
    }

    public listarUsuariosSemVinculoAtendente() {

        return this.dataService.get('api/v1/usuarios/semAtendentes', new HttpParams());
    }

    public salvar(usuario) {

        return this.dataService.post('api/v1/usuarios/criarUsuario', usuario);
    }

    public atualizar(usuario) {

        return this.dataService.put('api/v1/usuarios', usuario);
    }

    public carregarUsuarioPorGuid(guid: string) {

        let param = new HttpParams().
            set('guid', guid);

        return this.dataService.get("api/v1/usuarios/guid", param);
    }

    /**
     * Obtem um item salvo no localstorage
     * @param key Nome do item salvo no localstorage
     */
    public getUsuarioLogado(): Observable<any> {

        // if (this._usuario != undefined)
        //     return this._usuario;

        let localStorage = this.obterDadosUsuarioLocalStorage();

        return this.carregarUsuarioPorGuid(localStorage.guid);
    }

    public setUsuarioLogado(usuario) {

        this._usuario = usuario;
    }

    /**
     * Realiza a autenticação do usuario na Api
     */
    public autenticarUsuario(usuario: string, senha: string) {

        return this.dataService.autenticar(usuario, senha);
    }

    public validarNovoAcesso(guid: string, cpf: string) {

        let param = new HttpParams()
            .set('guid', guid)
            .set('cpf', cpf);


        return this.dataService.get("api/v1/usuarios/novoAcesso", param);
    }

    public AlterarSenha(guid, novaSenha, senhaAtual) {

        let param = new HttpParams()
            .set('guid', guid)
            .set('senhaAtual', senhaAtual)
            .set('novaSenha', novaSenha);

        return this.dataService.get("api/v1/usuarios/alterarSenha", param);
    }

    public gerarNovoCodAutenticacao(guid) {

        let param = new HttpParams()
            .set('guid', guid);

        return this.dataService.put("api/v1/usuarios/autenticacao", param);
    }

    public excluirUsuario(guid) {

        let param = new HttpParams()
            .set('guid', guid);

        return this.dataService.delete("api/v1/usuarios", param);
    }

    public postImagem(files: any) {
        let formData = new FormData();

        formData.append("files", files[0]);

        return this.dataService.postImage("api/v1/usuarios/salvarfoto", formData);
    }

    public getImagem() {

        return this.dataService.getImage("api/v1/usuarios/carregarfoto", '');
    }

    public obterUsuarioIdLogado() {
        return this.dataService.get('api/v1/usuarios/obterUsuarioIdLogado', new HttpParams());
    }
    ///mudei
    public esqueciMinhaSenha(email, cpf) {

        let param = new HttpParams()
            .set('emailUsuario', email)
            .set('cpf', cpf);

        return this.dataService.get("api/v1/usuarios/esqueciminhasenha", param);
    }

    public novaSenha(guidUsuario, token, novaSenha) {

        let param = new HttpParams()
            .set('guidUsuario', guidUsuario)
            .set('token', token)
            .set('novaSenha', novaSenha);

        return this.dataService.get("api/v1/usuarios/novasenha", param);
    }

    public obterListaUsuariosAuditoria(ids) {

        let param = new HttpParams()
            .set('ids', ids);

        return this.dataService.get("api/v1/usuarios/usuarioAuditoria", param);
    }
}