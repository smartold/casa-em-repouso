import { environment } from '../../environments/environment.dev';
import { Injectable } from "@angular/core";

@Injectable()
export class ValuesService {

    private readonly storage: string = 'https://doctorerpstorage.blob.core.windows.net/';
    private readonly googleApiKey: string = '&key=AIzaSyCL6cdfLQfgMHUi0jgWpbdEiKDVsJKZ5xM';
    private readonly googleGeocodeUrl: string = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
    private readonly climaTempoKey: string = 'b54096a739018e5695427b0a346c995e';
    private readonly climaTempoUrl: string = 'http://apiadvisor.climatempo.com.br/api/v1/';

    public getApiUrl(): string {

        return environment.urlApi;
    }

    public getWebSocketURL(): string {

        return environment.webSocket;
    }

    public getGoogleApi() {

        return this.googleApiKey;
    }

    public getGoogleGeocodeUrl(): string {

        return this.googleGeocodeUrl;
    }

    public getStorageUrl(): string {

        return this.storage;
    }

    public getClimaTempoKey(): string {

        return this.climaTempoKey;
    }

    public getClimaTempoUrl(): string {

        return this.climaTempoUrl;
    }

    public getTraducaoTable() {

        return {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar ",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            }
        }
    }

    public getCodigoCidadeClimaTempo(unidadeId) {

        let codigosClimaTempo = [
            { unidadeId: 1, id: 6879, cidade: 'Belo Horizonte - MG' },
            { unidadeId: 2, id: 3477, cidade: 'São Paulo - SP' },
            { unidadeId: 3, id: 5959, cidade: 'Rio de Janeiro - RJ' },
            { unidadeId: 4, id: 6861, cidade: 'Goiânia - GO' },
            { unidadeId: 5, id: 3877, cidade: 'Jundiaí - SP' },
            { unidadeId: 6, id: 6731, cidade: 'Curitiba - PR' },
            { unidadeId: 7, id: 7140, cidade: 'Recife - PE' },
            { unidadeId: 8, id: 3694, cidade: 'São José dos Campos - SP' },
            { unidadeId: 9, id: 8173, cidade: 'Brasília - DF' },
            { unidadeId: 10, id: 6910, cidade: 'Ponte Nova - MG' }
        ]

        return codigosClimaTempo.filter(x => x.unidadeId == unidadeId);
    }
}