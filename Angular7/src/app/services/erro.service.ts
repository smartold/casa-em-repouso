import { ToastrService } from "ngx-toastr";
import { Injectable } from "@angular/core";
import { Router } from "../../../node_modules/@angular/router";
import { BlockUiComponentsComponent } from "../components/shared/block-ui/block-ui-components/block-ui-components.component";

@Injectable()
export class ErroService {

    /**
     *Classe responsavel por tratar e exibir erros no sistema
     */
    constructor(private toastr: ToastrService, private router: Router) {

    }

    public tratarErro(response: any) {
        if (response.error != undefined) {

            try {

                if (response.error.length > 0) {

                    response.error.forEach(item => {

                        this.toastr.error(item.descricao[1], "Ops! Algo de errado não está certo!");
                    });
                }

            } catch (error) {
                this.toastr.error(error, "Erro");
            }
        }
        else {

        }
    }

    public exibirErroValidacaoCampos() {

        this.toastr.error('Existem campos na tela que não foram preenchidos corretamente', 'Ops! Algo de errado não está certo');
    }

    public exibirMensagemAreaRisco() {
        this.toastr.error('ATENÇÃO!!! Esse cliente mora em uma região potencialmente de risco. Antes de prosseguir verifique no Google Maps a localização exata.')
    }

    public exibirErroValidacaoCpf() {
        this.toastr.error('O Cpf foi digitado em formato incorreto', 'Ops! Algo de errado não está certo');
    }

    public exibirErroModificado(erro) {
        this.toastr.error(erro, 'Ops! Algo de errado não está certo');
    }


    public tratarErroInterceptor(error) {
        if (error.status === 401) {

            this.toastr.error("Sua sessão expirou, efetue o login novamente para acessar o sistema" + this.obterCodigoErro(error.status), "Ops! Algo de errado não está certo");
            localStorage.clear();
            this.router.navigate(['/login']);
        }
        else if (error.status === 403) {

            this.toastr.error("No momento você não possui permissão para ver o conteúdo desta página" + this.obterCodigoErro(error.status), "Ops! Algo de errado não está certo")
            this.router.navigate(['/home']);
        }
        else if (error.status === 404) {

            this.toastr.error("A página solicitada não pode ser encontrada, não se preocupe o setor de TI foi informado e já está trabalhando para corrigir esse erro" + this.obterCodigoErro(error.status), "Ops! Algo de errado não está certo")
            // this.router.navigate(['/home']);
        } else if (error.status === 0) {
            this.toastr.error("Erro de conexão, falha na API" + this.obterCodigoErro(error.status), "Ops! Algo de errado não está certo")
        }

        BlockUiComponentsComponent.forceStop();
        this.tratarErro(error);
        // } else if (error.status !== 200) {
        //     this.toastr.error("Ocorreu um erro inesperado" + this.obterCodigoErro(error.status), "Ops! Algo de errado não está certo")
        // }

    }

    private obterCodigoErro(codigo) {
        return ""; // " - COD: " + codigo;
    }
}