import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private dataService: DataService) {

  }

  /** consultar */
  public consultar(frase: string) {

    let param = new HttpParams().set('frase', frase);

    return this.dataService.get('api/v1/search/termo', param);
  }

  /** abrir arquivo */
  public abrirArquivo(arquivo: string) {

    let param = new HttpParams().set('arquivo', arquivo);

    return this.dataService.get('api/v1/search/arquivo', param);
  }
}

