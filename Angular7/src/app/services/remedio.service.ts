import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { HttpParams } from '@angular/common/http';

@Injectable()

export class RemedioService {

  constructor(private dataService: DataService) { }

  public salvar(remedio) {
    return this.dataService.post("api/remedio/salvar", remedio);
  }

  public ObterListaQuantidade() {
    return this.dataService.get("api/remedio/obterListagemQuantidade", new HttpParams());
  }

  public ObterListaDosagem() {
    return this.dataService.get('api/remedio/obterListagemDosagem', new HttpParams());
  }

  public obterMedicamentoPorIdosoId(id) {

    let param = new HttpParams().
      set('id', id);

    return this.dataService.get("api/remedio/obterListaRemedioIdosoId", param);
  }

}
