import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { ToastrService } from "ngx-toastr";

@Injectable()

export class AuthService {

  constructor(private router: Router, private toastr: ToastrService) { }

  canActivate() {
    if (!localStorage.getItem('token')) {

        this.router.navigate(['/login']);
        this.toastr.error('Informe suas credenciais para acessar novamente o sistema', 'Sessão Encerrada');

        return false;
    }

    return true;
}

}
