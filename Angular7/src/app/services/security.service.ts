import { Injectable } from "@angular/core";
import { DevClaims } from "../security/dev.claim";
import { MasterClaims } from "../security/master.claim";
import { AdmClaims } from "../security/adm.claim";
import { AtendenteClaims } from "../security/atendente.claim";
import { AtendenteLiderClaims } from "../security/atendente_lider.claim";
import { Menu } from "../security/menu";
import { FinanceiroAdmClaims } from "../security/financeiro_adm.claim";
import { FinanceiroClaims } from "../security/financeiro.claim";
import { FranquiaAdmClaims } from "../security/franquia_adm.claim";
import { FranquiaClaims } from "../security/franquia.claim";
import { QualidadeClaims } from "../security/qualidade.claim";
import { RotasClaims } from "../security/rotas.claim";
import { MarketingAdmClaims } from "../security/marketing_adm.claim";
import { MarketingClaims } from "../security/marketing.claim";
import { RhAdmClaims } from "../security/rh_adm.claim";
import { RhClaims } from "../security/rh.claim";

@Injectable({
  providedIn: 'root'
})
export class SecurityService {
  private menu: Menu = new Menu();

  private devClaims: DevClaims = new DevClaims();
  private masterClaims: MasterClaims = new MasterClaims();
  private admClaims: AdmClaims = new AdmClaims();
  private atendenteClaims: AtendenteClaims = new AtendenteClaims();
  private atendenteLiderClaims: AtendenteLiderClaims = new AtendenteLiderClaims();
  private financeiroAdmClaims: FinanceiroAdmClaims = new FinanceiroAdmClaims();
  private financeiroClaims: FinanceiroClaims = new FinanceiroClaims();
  private franquiaAdmClaims: FranquiaAdmClaims = new FranquiaAdmClaims();
  private franquiaClaims: FranquiaClaims = new FranquiaClaims();
  private qualidadeClaims: QualidadeClaims = new QualidadeClaims();
  private rotasClaims: RotasClaims = new RotasClaims();
  private marketingAdmClaims: MarketingAdmClaims = new MarketingAdmClaims();
  private marketingClaims: MarketingClaims = new MarketingClaims();
  private rhAdmClaims: RhAdmClaims = new RhAdmClaims();
  private rhClaims: RhClaims = new RhClaims();

  constructor() {
    this.devClaims = new DevClaims();
  }


  public definirMenuUsuario(claim: string): Menu {

    let menu: Menu;

    switch (claim) {
      case "dev":
        menu = this.devClaims.definirMenu();
        break;
      case "master":
        menu = this.masterClaims.definirMenu();
        break;
      case "adm":
        menu = this.admClaims.definirMenu();
        break;
      case "atendente":
        menu = this.atendenteClaims.definirMenu();
        break;
      case "atendente_lider":
        menu = this.atendenteLiderClaims.definirMenu();
        break;
      case "financeiro_adm":
        menu = this.financeiroAdmClaims.definirMenu();
        break
      case "financeiro":
        menu = this.financeiroClaims.definirMenu();
        break
      case "franquia_adm":
        menu = this.franquiaAdmClaims.definirMenu();
        break
      case "franquia":
        menu = this.franquiaClaims.definirMenu();
        break
      case "qualidade":
        menu = this.qualidadeClaims.definirMenu();
        break
      case "rotas":
        menu = this.rotasClaims.definirMenu();
        break
      case "marketing_adm":
        menu = this.marketingAdmClaims.definirMenu();
        break
      case "marketing":
        menu = this.marketingClaims.definirMenu();
        break
      case "rh_adm":
        menu = this.rhAdmClaims.definirMenu();
        break
      case "rh":
        menu = this.rhClaims.definirMenu();
        break

      default:
        break;
    }

    return menu;
  }

}
