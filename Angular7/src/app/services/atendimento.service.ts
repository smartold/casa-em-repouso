import { DataService } from "./data.service";
import { Injectable } from "@angular/core";

import { Observable } from 'rxjs';
import { HttpParams } from "@angular/common/http";

@Injectable()
export class AtendimentoService {

  constructor(private dataService: DataService) {

  }

  public salvar(atendimento) {

    return this.dataService.post('api/atendimento', atendimento);
  }

  public atualizar(atendimento) {

    return this.dataService.put('api/atendimento', atendimento);
  }

  public encerrar(atendimento) {

    return this.dataService.put('api/atendimento/encerrar', atendimento);
  }

  public obterAtendimentoPendente() {

    return this.dataService.get('api/atendimento/pendente', new HttpParams());
  }

  public obterAtendimentoPendenteFinalizacao() {

    return this.dataService.get('api/atendimento/pendentefinalizar', new HttpParams());
  }

  public obterHistoricoAtendimento(clienteId) {

    let param = new HttpParams()
      .set('clienteId', clienteId);

    return this.dataService.get('api/atendimento/historico', param);
  }

  public ObterResumoAtendimentoPorAtendenteId(atendenteId) {

    let param = new HttpParams()
      .set('atendenteId', atendenteId);

    return this.dataService.get('api/atendimento/resumoAtendente', param);
  }

  public atualizarDadosFinalizacaoAtendimento(obj) {

    return this.dataService.post('api/atendimento/finalizacaoAtendimento', obj);
  }
}