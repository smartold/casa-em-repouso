import { Injectable } from '@angular/core';
import { DataService } from "./data.service";

@Injectable()
export class RegistroService {


  constructor(private dataService: DataService) { }

  public registrar(formularioRegistro) {

    console.log("Ver oque vai passar pro back end", formularioRegistro)

    return this.dataService.post("api/registro", formularioRegistro);

  }
}
