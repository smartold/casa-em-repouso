import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.dev';
import { ValuesService } from "./values.service";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';
import { Md5 } from 'ts-md5/dist/md5';


@Injectable()
export class DataService {

    private readonly apiUrl: string;
    private token: string;

    constructor(private http: HttpClient, private values: ValuesService) {

        this.apiUrl = values.getApiUrl();
    }

    private getToken() {

        return localStorage.getItem('token');
    }

    public postImage(metodo: string, formData: any) {

        let options = {
            headers: new HttpHeaders().set('Authorization', `Bearer ${this.getToken()}`),
        }

        return this.http.post(this.apiUrl + metodo, formData, options);
    }

    public post(metodo: string, data: any) {

        let options = this.createRequestOptions();
        let body = JSON.stringify(data);

        return this.http.post(this.apiUrl + metodo, body, options);
    }

    public get(metodo: string, params: any) {

        var options = this.createRequestOptions(params);

        return this.http.get<any>(this.apiUrl + metodo, options);
    }

    public getGenerico(url: string) {
        return this.http.get<any>(url);
    }

    public getViaCep(cep) {

        let url = "https://viacep.com.br/ws/" + cep + "/json/";

        return this.http.get<any>(url);
    }

    public getClickSign() {
        //let queryString = 'access_token=6598d571-3ec5-4822-95d7-6758a034d8bc';

        let url = "https://sandbox.clicksign.com/api/v1/documents";

        let params = new HttpParams().set('access_token', '6598d571-3ec5-4822-95d7-6758a034d8bc');
        let httpOptions = {

            headers: new HttpHeaders()
                .set('Accept', 'application/json')
                .set('Content-Type', 'application/json')
            ,
            params
        }

        return this.http.get(url, httpOptions);
    }

    private createRequestOptionsGeneric(params?: HttpParams) {

        let options = {
            headers: new HttpHeaders()
                .set("Access-Control-Allow-Origin", "*")
                .set('Access-Control-Allow-Methods', 'GET')
                .set("Access-Control-Allow-Headers", "Content-Type, Accept")
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json'),
            params: params
        }

        return options;
    }

    public put(metodo: string, params: any) {

        let options = this.createRequestOptions(params);
        let body = JSON.stringify(params);

        return this.http.put(this.apiUrl + metodo, body, options);
    }

    public putWithParameters(metodo: string, body: any, params: any) {

        let options = this.createRequestOptions(params);
        let bodyConverted = JSON.stringify(body);

        return this.http.put(this.apiUrl + metodo, bodyConverted, options);
    }

    public postWithParameters(metodo: string, body: any, params: any) {

        let options = this.createRequestOptions(params);
        let bodyConverted = JSON.stringify(body);

        return this.http.post(this.apiUrl + metodo, bodyConverted, options);
    }

    public delete(metodo: string, params: any) {

        var options = this.createRequestOptions(params);
        return this.http.delete(this.apiUrl + metodo, options);
    }

    public getImage(metodo: string, params: any) {

        var options = this.createRequestOptions(params);

        return this.http.get(this.apiUrl + metodo, options);
    }

    public createRequestOptions(params?: HttpParams) {

        let options = {
            headers: new HttpHeaders().set('Content-Type', 'application/json')
                .set('Authorization', `Bearer ${this.getToken()}`),
            params: params
        }

        return options;
    }

    public autenticar(usuario: string, senha: string) {

        let options = {
            headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
        }

        let dt = "grant_type=password&nomeUsuario=" + usuario + "&senha=" + senha;
        let url = this.apiUrl + 'api/v1/usuarios';

        return this.http.post(url, dt, options);
    }

    public generateHashValidator() {
        const salt = 'cf1b4906-f809-4abe-a7b8-ba6c3e218e6a';
        const encriptKey = new Date().getDate();
        return Md5.hashStr(encriptKey + salt).toString();
    }
}