import { NgModule } from "@angular/core";
import { AuthService } from "./services/auth.service";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { RegistroComponent } from "./pages/registro/registro.component";
import { LoginComponent } from "./pages/login/login.component";
import { HomeComponent } from "./pages/home/home.component";
import { ListaPacienteComponent } from "./pages/paciente/lista-paciente/lista-paciente.component";
import { IdosoeComponent } from "./pages/paciente/idosoe-page/idosoe-page.component";
import { EventosComponent } from './pages/eventos/eventos.component';
import { EtapasAtendimentoComponent } from './pages/atendimento/etapas-atendimento/etapas-atendimento.component';
import { ProntuarioComponent } from './pages/atendimento/prontuario/prontuario.component';
import { RelatoriosComponent } from './pages/relatorios/relatorios.component';
import { CuidadoralComponent } from './pages/cuidadora/cuidadoral/cuidadoral.component';
import { CuidadoraeComponent } from './pages/cuidadora/cuidadorae/cuidadorae.component';
import { ProntuariolComponent } from './pages/atendimento/prontuario/prontuariol/prontuariol.component';


const routes: Routes = [
  {
    path: "",
    canActivate: [AuthService],
    redirectTo: "home",
    pathMatch: "full"
  },
  {
    path: "registro",
    component: RegistroComponent
  },
  { path: "login", component: LoginComponent },
  { path: "home", component: HomeComponent },
  { path: "lista-idoso", component: ListaPacienteComponent },
  { path: "idosoe", component: IdosoeComponent },
  { path: "eventos", component: EventosComponent },
  { path: "idosoe/:guid", component: IdosoeComponent },
  { path: "etapas-atendimento", component: EtapasAtendimentoComponent },
  { path: "prontuario", component: ProntuarioComponent },
  { path: "relatorios", component: RelatoriosComponent },
  { path: "cuidadoral", component: CuidadoralComponent },
  { path: "cuidadorae", component: CuidadoraeComponent },
  { path: "prontuariol", component: ProntuariolComponent }


];

@NgModule({
  exports: [RouterModule],
  declarations: [],
  imports: [CommonModule, RouterModule.forRoot(routes)]
})
export class AppRoutingModule { }
