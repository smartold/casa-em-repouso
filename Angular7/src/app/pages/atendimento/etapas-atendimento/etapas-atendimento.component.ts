import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

declare var $: any;

@Component({
  selector: 'app-etapas-atendimento',
  templateUrl: './etapas-atendimento.component.html',
  styleUrls: ['./etapas-atendimento.component.css']
})
export class EtapasAtendimentoComponent implements OnInit {

  public etapaAtendimentoAtual: any;
  public etapasAtendimentoConcluidas: any;
  public atendimentoPendente: any;
  public idoso: any;

  constructor(
    private toastr: ToastrService
  ) {
    this.atendimentoPendente = {
      id: 0
    }

    this.etapaAtendimentoAtual = {
      idoso: true,
      prontuario: false,
      responsavel: false,
      confirmacao: false
    }

    this.etapasAtendimentoConcluidas = {
      idoso: false,
      prontuario: false,
      responsavel: false,
      confirmacao: false
    }

    this.idoso = {

      id: 0
    }
  }

  ngOnInit() {
  }

  private validarAtendimentoPendente() {

    if (this.atendimentoPendente != undefined) {

      //Etapa em que o atendimento foi interrompido
      let etapaAtual = this.atendimentoPendente.estagioAtendimento;
      this.reiniciarEtapaAtual();
      this.toastr.info('Você possui um atendimento não finalizado.', 'Atendimento Pendente');

      switch (etapaAtual) {
        case 0:
          this.etapaAtendimentoAtual.cliente = true;
          break;
        case 1:
          this.etapaAtendimentoAtual.necessidade = true;
          break;
        case 2:
          this.etapaAtendimentoAtual.atendimento = true;
          break;
        case 3:
          this.etapaAtendimentoAtual.agendamento = true;
          break;
        case 4:
          this.etapaAtendimentoAtual.confirmacao = true;
          break;
        default:
          break;
      }

      this.marcarEtapasComoConcluidas(etapaAtual)
    }
  }

  /**Atualiza os valores da etapa atual para default - false para todos */
  private reiniciarEtapaAtual() {

    this.etapaAtendimentoAtual = {

      idoso: false,
      necessidade: false,
      prontuario: false,
      responsavel: false,
      confirmacao: false
    };
  }

  private reiniciarEtapaConcluida() {

    this.etapasAtendimentoConcluidas = {

      idoso: false,
      necessidade: false,
      prontuario: false,
      responsavel: false,
      confirmacao: false
    };
  }

  private marcarEtapasComoConcluidas(etapaAtual) {

    switch (etapaAtual) {
      case 1:
        this.etapasAtendimentoConcluidas.idoso = true;
        break;
      case 2:
        {
          this.etapasAtendimentoConcluidas.idoso = true;
          this.etapasAtendimentoConcluidas.prontuario = true;
        }
        break;
      case 3:
        {
          this.etapasAtendimentoConcluidas.idoso = true;
          this.etapasAtendimentoConcluidas.prontuario = true;
          this.etapasAtendimentoConcluidas.responsavel = true;
        }
        break;
      default:
        break;
    }
  }

  public pacienteSelecionado(idoso) {

    if (idoso != undefined) {
      this.idoso = idoso;

      this.etapaAtendimentoAtual.idoso = false;
      this.etapaAtendimentoAtual.prontuario = true;
      this.etapasAtendimentoConcluidas.idoso = true;
    }
  }




}
