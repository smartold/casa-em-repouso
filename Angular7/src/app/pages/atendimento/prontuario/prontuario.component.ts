import { Component, OnInit, Input } from '@angular/core';
import { PacienteService } from 'src/app/services/paciente.service';
import { ToastrService } from 'ngx-toastr';
import { RemedioService } from 'src/app/services/remedio.service';
import { ErroService } from 'src/app/services/erro.service';

declare var $: any;

@Component({
  selector: 'app-prontuario',
  templateUrl: './prontuario.component.html',
  styleUrls: ['./prontuario.component.css'],
  providers: [PacienteService, RemedioService]
})
export class ProntuarioComponent implements OnInit {
  @Input() idosoGuid;

  public liberarInfoBanho;
  public idosoSelecionado: any;

  public listaRemedio: any = [];

  public listaRemedioManha: any = [];
  public listaRemedioTarde: any = [];
  public listaRemedioNoite: any = [];
  public listaRemedioAntesDormir: any = [];

  constructor(
    private toastr: ToastrService,
    private pacienteService: PacienteService,
    private remedioService: RemedioService,
    private erroService: ErroService
  ) { }

  ngOnInit() {
    this.obterIdoso();
  }

  public obterIdoso() {


    if (this.idosoGuid != null) {

      this.pacienteService.obterPacientePorGuid(this.idosoGuid).subscribe(

        result => {

          this.idosoSelecionado = result;
          console.log("Informacao do idoso", this.idosoSelecionado)

          this.obterMedicamentoIdoso();


        },
        erro => {

          this.toastr.warning('Parece que não encontramos o idoso, entre em contato com o TI!', 'OPS!');
        }
      )
    }
    else
      this.toastr.warning('Parece que não encontramos o idoso, entre em contato com o TI!', 'OPS!');
  }

  public selecionarCheckBoxBanho() {
    $('ui.banho.checkbox').checkbox('attach events');
    this.liberarInfoBanho == true;
  }

  public selecionarCheckBoxEscovacao() {

    $('ui.escovacao.checkbox').checkbox('attach events');
  }

  public obterMedicamentoIdoso() {

    this.remedioService.obterMedicamentoPorIdosoId(this.idosoSelecionado.id).subscribe(

      result => {

        this.listaRemedio = result;
        console.log(this.listaRemedio)


      },
      erro => {

        this.erroService.tratarErro(erro);
      }
    )

  }


}
