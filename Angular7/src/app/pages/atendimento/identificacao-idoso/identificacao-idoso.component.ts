import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PacienteService } from '../../../services/paciente.service';
import { DatatablesService } from '../../../services/datatables.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { ToastrService } from 'ngx-toastr';
import { ErroService } from 'src/app/services/erro.service';
import { AtendimentoService } from 'src/app/services/atendimento.service';

declare var $: any;


@Component({
  selector: 'app-identificacao-idoso',
  templateUrl: './identificacao-idoso.component.html',
  styleUrls: ['./identificacao-idoso.component.css'],
  providers: [PacienteService, DatatablesService, AtendimentoService]
})
export class IdentificacaoIdosoComponent implements OnInit {

  @ViewChild(DataTableDirective) dtElement: DataTableDirective;

  @Output() pacienteSelecionado = new EventEmitter<any>();

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  public form: FormGroup;
  public listaPacientes: any;
  public quantPacienteExistente: number;
  public nomePacienteSelecionado: string;
  public idosoId: number;
  public atendimento: any;
  public atendente: any;
  public usuario: any;
  public escolhiPaciente: any;
  public segmentStyle: {};

  //modal 
  public reabrirModalVerMais = false;
  public abrirModalVerMais = false;
  public fecharModalVerMais = false;

  constructor(
    private formBuilder: FormBuilder,
    private pacienteService: PacienteService,
    private toastr: ToastrService,
    private erroService: ErroService,
    private dataTables: DatatablesService,
    private atendimentoService: AtendimentoService
  ) {
    this.quantPacienteExistente = 0;
    this.atendimento = {};
    this.atendente = {};
    this.idosoId = 0;
  }

  ngOnInit() {
    this.listaPacientes = [];
    this.idosoId = 0;

    this.definirFormulario();
    this.dataTables.rerender(this.dtElement, this.dtTrigger);

    this.segmentStyle = { 'margin-bottom': '28px' };

  }

  ngAfterViewChecked() {
    if (this.reabrirModalVerMais) {
      this.reabrirModalVerMais = false;
      this.closeVerMais();
      this.abrirModalVerMais = true;
    }
    if (this.abrirModalVerMais) {
      this.abrirModalVerMais = false;
      this.showVerMais();
    }
    if (this.fecharModalVerMais) {
      this.fecharModalVerMais = false;
      this.closeVerMais();
    }
  }

  public definirFormulario() {
    this.form = this.formBuilder.group({

      cpf: [''],
      nome: ['']

    })
  }

  public identificarPaciente() {
    this.moverPagina();
  }

  private validarIdentificacaoIdoso(): boolean {

    let nomeLength = this.form.value.nome.length;
    let cpfLength = this.form.value.cpf.length;

    if (nomeLength < 3 && cpfLength < 14)
      return false;

    return true;
  }

  public identificarPossivelIdoso() {

    if (this.validarIdentificacaoIdoso()) {

      this.pacienteService.identificarPaciente(this.form.value).subscribe(

        result => {

          this.dataTables.rerender(this.dtElement, this.dtTrigger);
          this.listaPacientes = result;

          this.quantPacienteExistente = this.listaPacientes.length;

          if (this.listaPacientes.length == 100)
            this.toastr.warning("A Busca realizada retornou muitos registros, por uma questão de performace, apenas os 100 primeiros estão sendo exibidos.", "Muitos  registros encontrados");
        },
        erro => {

          this.erroService.tratarErro(erro);
        }
      )
    }
    else
      this.quantPacienteExistente = 0;
  }

  public moverPagina() {

    let table = $('table');
    $('html,body').animate({ scrollTop: table.offset().top }, 'slow');
  }

  public configurarDataTables() {

    let table = $('table').DataTable();
    table.destroy();
    this.dtTrigger.next();

    this.dtOptions.language = this.dataTables.getTraducaoTable();
  }

  public atenderIdoso(idoso) {



    this.atendimento = {

      idosoId: idoso.id,
      atendenteId: this.atendente.id,
      estagioAtendimento: 1
    }

    //dispara uma mensagem para o componente pai informando que o cliente foi selecionado
    this.pacienteSelecionado.emit(idoso);

    // //Inicia um novo atendimento
    // this.atendimentoService.salvar(this.atendimento).subscribe(

    //   result => {

    //     //dispara uma mensagem para o componente pai informando que o cliente foi selecionado
    //     this.pacienteSelecionado.emit(idoso);
    //   },
    //   error => {

    //     this.erroService.tratarErro(error);
    //   }
    // )
  }


  public exibirModalAtendimento(paciente) {
    this.abrirModalVerMais = true;
    this.escolhiPaciente = paciente;

    if (paciente != undefined)
      this.nomePacienteSelecionado = paciente.nome;

  }

  public showVerMais() {
    $('.atendimento-modal').modal({
      blurring: false,
      detachable: false,
      onVisible: recarregar => {
        $(".atendimento-modal").modal('refresh');
      }

    }).modal('show', 'refresh');
  }

  public closeVerMais() {
    $('.atendimento-modal').modal('close');
  }

}
