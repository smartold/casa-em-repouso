import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { RegistroService } from '../../services/registro.service';
import { ToastrService } from 'ngx-toastr';
import { ErroService } from '../../services/erro.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css'],
  providers: [RegistroService]
})

export class RegistroComponent implements OnInit {

  public formulario: FormGroup;
  public passwaordMismatch = false;
  public registro: any = {};

  constructor(
    private formBuilder: FormBuilder,
    private registroService: RegistroService,
    private toastr: ToastrService,
    private erroService: ErroService
  ) { }

  ngOnInit() {
    this.definirFormulario();
  }

  public definirFormulario() {

    this.formulario = this.formBuilder.group({
      userName: ["", Validators.required],
      nomeCompleto: ["", Validators.required],
      email: ["", Validators.email],
      password: ["", Validators.required]
    });
  }

  submit() {

    this.toastr.info('Enviando Cadastro!', 'Aguarde!');
    this.registroService.registrar(this.formulario.value).subscribe(


      result => {
        this.toastr.success('Sucesso!', 'Conta Cadastrada!');
        this.registro = result;
        console.log("resultado", result);
      },
      erro => {
        this.erroService.tratarErro(erro);
      }

    )
  }


}
