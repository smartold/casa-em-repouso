import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ToastrService } from "ngx-toastr";
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { EventosService } from '../../services/eventos.service';
import { ValuesService } from '../../services/values.service';
import { ErroService } from '../../services/erro.service';

declare var $: any;

@Component({
  selector: 'app-eventos',
  templateUrl: './eventos.component.html',
  styleUrls: ['./eventos.component.css'],
  providers: [EventosService, ValuesService]
})
export class EventosComponent implements OnInit {

  FileToUpload: File = null;

  public evento: any = {};
  public formulario: FormGroup;
  public edicao: boolean = false;
  private guid: string;
  public userClains: any = [];
  public submitted: boolean = false;
  public segmentStyle: any = { 'margin-bottom': '28px' };
  public itensTipoServico = [];
  public urlFotoEvento: any = '/assets/Imagens/uploaddefault.png';
  public exibirModalAjuda = false;
  public paginasModalAjuda: string[] = [];

  public imagemSelecionada = false;



  @ViewChild('imgRef') img: ElementRef;
  @ViewChild("fileInput") fileInput;

  private imagemPreCarregada = false;

  constructor(private activatedRoute: ActivatedRoute,
    private router: Router,
    private eventosService: EventosService,
    private erroService: ErroService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private valuesService: ValuesService) {

  }

  ngOnInit() {
    // this.formulario.reset();
    this.definirFormulario();
    this.obterParametrosUrl();

    if (this.guid != undefined) {

      this.edicao = true;
      this.carregarEvento(this.guid);
    }
  }

  private carregarEvento(guid: string) {

    this.eventosService.obterEventoPorGuid(guid).subscribe(

      result => {

        this.evento = result;

        this.preencherFormulario(this.evento);

        if (this.evento.imagem == undefined || this.evento.imagem == '') {
          this.urlFotoEvento = this.valuesService.getStorageUrl() + 'usuarios/user.png';

          this.formulario.patchValue({
            urlFoto: this.urlFotoEvento
          })
        }
        else {
          this.urlFotoEvento = this.evento.imagem;
        }
      },
      erro => {

        this.erroService.tratarErro(erro);
      }
    )
  }


  private obterParametrosUrl() {

    this.activatedRoute.params.subscribe((params: Params) => {
      this.guid = params['guid'];
    });
  }

  public escolherImagem() {

    let input = $('#inputFile');
    let storageUrl = this.valuesService.getStorageUrl();
    input.click();
  }

  FileInput(file: FileList) {
    this.FileToUpload = file.item(0);

    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imagemSelecionada = true;
      this.urlFotoEvento = event.target.result;
    }
    reader.readAsDataURL(this.FileToUpload);
  }

  postImagem(): void {
    this.toastr.info('Enviando imagem!', 'Aguarde!');
    let fi = this.fileInput.nativeElement;
    if (fi.files.length > 0) {
      let fileToUpload = fi.files;
      this.eventosService.postImagem(fileToUpload).subscribe(
        result => {
          this.toastr.success("Imagem carregada", "Sucessso");
          this.urlFotoEvento = result;
          this.formulario.patchValue({
            imagem: result
          })
          this.submit();
        });
    }
    else {
      this.toastr.error('Não existe uma nova imagem para enviar!', 'Erro!');
    }
  }

  private definirFormulario() {

    this.formulario = this.formBuilder.group({
      id: [0],
      guid: [null],
      imagem: [''],
      titulo: ['',
        [
          Validators.required,
        ]],
      texto: ['',
        [
          Validators.required,
        ]]
    })
  }

  public preencherFormulario(tutorial) {

    this.formulario.patchValue({
      id: tutorial.id,
      guid: tutorial.guid,
      imagem: tutorial.imagem,
      titulo: tutorial.titulo,
      texto: tutorial.texto,
    })
  }

  public submit() {

    this.validarImg();

    if (this.formulario.value.id > 0) {
      this.eventosService.atualizar(this.formulario.value).subscribe(

        result => {

          this.toastr.success('Tutorial Atualizado!', 'Sucesso!');
          this.evento = result;
          this.preencherFormulario(this.evento);
        },
        erro => {
          this.erroService.tratarErro(erro);
        }
      )
    } else {
      this.formulario.value.imagem = this.urlFotoEvento;

      this.eventosService.salvar(this.formulario.value).subscribe(

        result => {

          this.toastr.success('Sucesso!g.', 'Tutorial cadastrado!');
          this.evento = result;
          this.edicao = true;
          this.preencherFormulario(this.evento);
        },
        erro => {
          this.erroService.tratarErro(erro);
        }
      )
    }
  }

  public fecharModal() {
    this.exibirModalAjuda = false;
  }


  public submitImg() {
    if (this.imagemSelecionada == false) {
      this.submit();
    } else {
      this.postImagem();
    }
  }

  public validarImg() {

    let storageUrl = this.valuesService.getStorageUrl();
    if (this.formulario.valid) {


      if (this.urlFotoEvento != "") {
        this.formulario.value.imagem = this.urlFotoEvento;
      }
      else {
        this.formulario.value.imagem = storageUrl + 'tutorialhome/no-cover.jpg';
      }
    }
  }

}
