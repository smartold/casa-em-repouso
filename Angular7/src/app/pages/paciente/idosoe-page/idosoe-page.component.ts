import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ErroService } from 'src/app/services/erro.service';
import { ToastrService } from 'ngx-toastr';
import { ValuesService } from 'src/app/services/values.service';
import { PacienteService } from 'src/app/services/paciente.service';
import { variable } from '@angular/compiler/src/output/output_ast';
import { DateTransform } from 'src/app/utils/datetime/date-transform';
import { RemedioService } from 'src/app/services/remedio.service';

declare var $: any;

@Component({
  selector: 'app-idosoe-page',
  templateUrl: './idosoe-page.component.html',
  styleUrls: ['./idosoe-page.component.css'],
  providers: [PacienteService, DateTransform, RemedioService]
})

export class IdosoeComponent implements OnInit {

  public formulario: FormGroup;
  FileToUpload: File = null;
  public urlFotoEvento: any = '/assets/Imagens/user.png';
  public imagemSelecionada = false

  @ViewChild('imgRef') img: ElementRef;
  @ViewChild("fileInput") fileInput;


  public paciente: any = {};
  public edicao: boolean = false;
  private guid: string;
  public userClains: any = [];
  public submitted: boolean = false;
  public segmentStyle: any = { 'margin-bottom': '28px' };
  public itensTipoServico = [];
  public abrirFormExcluir = false;
  public paginasModalAjuda: string[] = [];

  //mudar pra false como padrao
  public mostrarEstoque = true;

  //modal 
  public reabrirModalVerMais = false;
  public abrirModalVerMais = false;
  public fecharModalVerMais = false;

  //modal
  public pacienteGuid;
  public abrirModalRemedio = false;

  //formulario Remedio
  public listaQuantidade: any = [];
  public listaDosagem: any = [];
  public listaRemedio: any = [];
  public formRemedio: FormGroup;



  constructor(private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private pacienteService: PacienteService,
    private erroService: ErroService,
    private toastr: ToastrService,
    private valuesService: ValuesService,
    private dateTransform: DateTransform,
    private remedioService: RemedioService) { }

  ngOnInit() {
    this.obterParametrosUrl();
    this.definirFormulario();
    this.definirFormularioRemedio();
    this.carregarListaQuantidade();
    this.carregarListaDosagem();


    if (this.guid != undefined) {

      this.edicao = true;
      this.carregarIdoso(this.guid);
      this.mostrarEstoque = true;
    }


  }

  ngAfterViewChecked() {
    if (this.reabrirModalVerMais) {
      this.reabrirModalVerMais = false;
      this.closeVerMais();
      this.abrirModalVerMais = true;
    }
    if (this.abrirModalVerMais) {
      this.abrirModalVerMais = false;
      this.showVerMais();
    }
    if (this.fecharModalVerMais) {
      this.fecharModalVerMais = false;
      this.closeVerMais();
    }
  }

  private carregarListaQuantidade() {

    this.remedioService.ObterListaQuantidade().subscribe(
      result => {
        this.listaQuantidade = result;
        console.log("ListaQuantidade", this.listaQuantidade)
      },
      erro => {
        this.erroService.tratarErro(erro);
      }
    )
  }

  private carregarListaDosagem() {

    this.remedioService.ObterListaDosagem().subscribe(
      result => {
        this.listaDosagem = result;

      },
      erro => {
        this.erroService.tratarErro(erro);
      }
    )
  }

  private definirFormulario() {

    var dataDia = this.dateTransform.transformarComponenteData(new Date());

    this.formulario = this.formBuilder.group({
      id: [0],
      nome: ['', [Validators.required,]],
      nacionalidade: ['', [Validators.required,]],
      cpf: [],
      rg: [],
      dataNascimento: [dataDia, [Validators.required,]],
      sexo: [],
      tipoSanguineo: [''],
      dataDeEntrada: [dataDia],
      observacao: ['']

    })

  }



  private carregarIdoso(guid: string) {

    this.pacienteService.obterPacientePorGuid(guid).subscribe(

      result => {

        this.paciente = result;

        console.log("paciente", this.paciente)

        this.preencherFormulario(this.paciente);

        this.carregarMedicamentoIdosoId();

        if (this.paciente.imagem == undefined || this.paciente.imagem == '') {
          this.urlFotoEvento = this.valuesService.getStorageUrl() + 'usuarios/user.png';

          this.formulario.patchValue({
            urlFoto: this.urlFotoEvento
          })
        }
        else {
          this.urlFotoEvento = this.paciente.imagem;
        }
      },
      erro => {

        this.erroService.tratarErro(erro);
      }
    )
  }


  private obterParametrosUrl() {

    this.activatedRoute.params.subscribe((params: Params) => {

      this.guid = params['guid'];

    });
  }



  public escolherImagem() {

    this.toastr.warning('Parece que essa função ainda não está disponível!', 'OPS!');
  }

  public preencherFormulario(paciente) {

    this.formulario.patchValue({
      id: paciente.id,
      nome: paciente.nome,
      nacionalidade: paciente.nacionalidade,
      cpf: paciente.cpf,
      rg: paciente.rg,
      dataNascimento: this.dateTransform.toCsharpDate(paciente.dataNascimento),
      sexo: paciente.sexo,
      tipoSanguineo: paciente.tipoSanguineo,
      dataDeEntrada: this.dateTransform.toCsharpDate(paciente.dataDeEntrada),
      observacao: paciente.observacao
    })
  }

  public submit() {

    this.toastr.info('Realizando Cadastro!', 'Aguarde!');

    if (this.formulario.value.id > 0) {
      this.pacienteService.atualizar(this.formulario.value).subscribe(

        result => {

          this.toastr.success('Cadastro Atualizado!', 'Sucesso!');
          this.paciente = result;
          this.mostrarEstoque = true;
          this.preencherFormulario(this.paciente);
        },
        erro => {
          this.erroService.tratarErro(erro);
        }
      )
    } else {

      this.pacienteService.salvar(this.formulario.value).subscribe(

        result => {
          this.toastr.success('Sucesso!', 'Idoso cadastrado!');
          this.paciente = result;
          this.edicao = true;
          this.preencherFormulario(this.paciente);
        },
        erro => {
          this.erroService.tratarErro(erro);
        }
      )
    }
  }

  public excluir() {
    this.pacienteService.excluirPaciente(this.guid).subscribe(
      result => {
        this.toastr.success("Idoso Removido", "Sucesso");
        this.router.navigate(['/lista-idoso']);
      }
    )
  }

  public exibirModalExcluir() {
    this.abrirModalVerMais = true;
  }

  public showVerMais() {
    $('.vermais').modal({
      blurring: false,
      detachable: false,
      onVisible: recarregar => {
        $(".vermais").modal('refresh');
      }

    }).modal('show', 'refresh');
  }

  public closeVerMais() {
    $('.vermais').modal('close');
  }

  public exibirModalRemedio(pacienteGuid) {

    this.pacienteGuid = pacienteGuid;
    this.abrirModalRemedio = true;
  }

  public fecharModalRemedio() {
    this.abrirModalRemedio = false;
  }

  private definirFormularioRemedio() {

    var dataDia = this.dateTransform.transformarComponenteData(new Date());

    this.formRemedio = this.formBuilder.group({
      id: [0],
      nome: ['', [Validators.required,]],
      dosagemId: [],
      quantidadeId: [],
      idosoId: [],
      dataTermino: [dataDia, [Validators.required,]]
    })

  }

  public salvarRemedio() {

    this.formRemedio.value.idosoId = this.paciente.id;
    this.toastr.info('Cadastrando Remedio!', 'Aguarde!');


    this.remedioService.salvar(this.formRemedio.value).subscribe(

      result => {
        this.toastr.success('Sucesso!', 'Medicamento cadastrado!');
        this.paciente = result;
        this.edicao = true;
        this.preencherFormulario(this.paciente);
      },
      erro => {
        this.erroService.tratarErro(erro);
      }
    )

  }

  public carregarMedicamentoIdosoId() {


    this.remedioService.obterMedicamentoPorIdosoId(this.paciente.id).subscribe(

      result => {

        this.listaRemedio = result;
        console.log(this.listaRemedio)


      },
      erro => {

        this.erroService.tratarErro(erro);
      }
    )
  }




}
