import { Component, OnInit } from '@angular/core';
import { PacienteService } from 'src/app/services/paciente.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { ErroService } from 'src/app/services/erro.service';

@Component({
  selector: 'app-lista-paciente',
  templateUrl: './lista-paciente.component.html',
  providers: [PacienteService]
})
export class ListaPacienteComponent implements OnInit {
  public idosos;

  constructor(
    private pacienteService: PacienteService,
    private toastr: ToastrService,
    private router: Router,
    private erroService: ErroService
  ) { }

  ngOnInit() {

    this.carregarListaIdosos();
  }

  public carregarListaIdosos() {

    this.pacienteService.listarIdosos().subscribe(result => {

      this.idosos = result;

      this.setarSexo();
      console.log("Oque esta vindo da api", result)

    }, error => {
      this.erroService.tratarErro(error);
    });
  }

  public setarSexo() {
    if (this.idosos.length > 0) {
      this.idosos.forEach(idoso => {
        if (idoso.sexo == 0) {
          idoso.sexo = "Masculino";
        } else if (idoso.sexo == 1) {
          idoso.sexo = "Feminino";
        } else {
          idoso.sexo = "Outros"
        }
      });
    }
  }

}
