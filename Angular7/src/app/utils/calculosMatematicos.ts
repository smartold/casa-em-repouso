import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable()
export class CalculosMatematicos {
    public obterDistanciaEmMetrosPorCoordenadas(latA, lonA, latB, lonB) {

        let parte1 = Math.pow(latA - latB, 2);
        let parte2 = Math.pow(lonA - lonB, 2);
        let parte3 = Math.sqrt(parte1 + parte2)
        let parte4 = parte3 * 111 * 1000;

        return parte4;
    }
}