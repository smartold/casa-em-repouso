import { Injectable } from "@angular/core";

@Injectable()
export class DateTransform {

    constructor() {
    }

    public transformarComponenteData(data: any) {

        return new Date(data).toISOString().split("T")[0];
    }

    public transformarComponenteDataISO(data: any) {

        return data.split("T")[0];
    }

    public transformarDataAmericana(texto: any) {
        let hora = texto.split(' ')[1];
        let tipo = texto.split(' ')[2];
        let novoTexto = '';
        let novaHora;
        novaHora = parseInt(hora);
        if (tipo == 'PM') {
            novaHora += 12;
        }
        if (hora == 12 && tipo == 'PM') {
            novoTexto = '12:00';
        }
        if (hora == 12 && tipo == 'AM') {
            novoTexto = '00:00';
        }
        if (novoTexto.length == 0) {
            if (novaHora < 10) {
                novoTexto = '0' + novaHora.toString();
            } else {
                novoTexto = novaHora.toString();
            }
            if (novoTexto.length == 2) {
                novoTexto = novoTexto + ':00';
            }
        }

        return novoTexto;
    }

    public transformarComponenteDataTime(data: any) {

        let arrayData = new Date(data).toISOString().split("T");

        if (arrayData.length > 0) {

            let dataTime = arrayData[0] + ' ' + arrayData[1].substring(0, 8);

            return dataTime;
        }
    }

    public static transformarComponenteDataTime(data: any) {

        let arrayData = new Date(data).toISOString().split("T");

        if (arrayData.length > 0) {

            let dataTime = arrayData[0] + ' ' + arrayData[1].substring(0, 8);

            return dataTime;
        }
    }

    public pegarHoraDaData(data: any) {

        //2018-12-14T15:05:20.1194678

        if (data.length > 0) {
            return data.substring(11, 19);
        } else {
            return "Sem data";
        }



    }

    public toLocaleString(data: any) {
        return data != null ? new Date(data).toLocaleDateString() : '';

    }

    public static toLocaleString(data: any) {

        return data != null ? new Date(data).toLocaleDateString() : '';

    }

    public toCsharpDate(date: any) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

    public obterDiaSemana(date: any) {
        const semana = ["Domingo", "Segunda-Feira", "Terça-Feira", "Quarta-Feira", "Quinta-Feira", "Sexta-Feira", "Sábado"];

        let data = new Date(date);

        let dia = data.getDay();

        return semana[dia];
    }

    public obterMes(date: any) {
        const meses = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];

        let data = new Date(date);

        let mes = data.getMonth();

        return meses[mes];
    }

    public validarDiferencaDias(date1, date2) {
        var date1Updated = new Date(date1.replace(/-/g, '/'));
        var date2Updated = new Date(date2.replace(/-/g, '/'));

        return date1Updated >= date2Updated;
    }

    public toHorasMinutos(date: any) {

        let retorno = date.split(':');

        return retorno[0];

    }

    public validarDiferencaHoras(start, end) {
        var regExp = /(\d{1,2})\:(\d{1,2})\:(\d{1,2})/;
        return parseInt(end.replace(regExp, "$1$2$3")) > parseInt(start.replace(regExp, "$1$2$3"));
    }
}