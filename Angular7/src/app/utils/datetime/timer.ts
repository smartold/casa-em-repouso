import { Injectable } from "@angular/core";

@Injectable()
export class Timer {

    constructor() {
    }

    public iniciar() {
        let timeStart = new Date().getTime();

        return {

            get ms() {
                const ms = (new Date().getTime() - timeStart);
                return ms;
            }
        }
    }
}