import { Injectable } from "@angular/core";

@Injectable()
export class DateValidator {

    constructor() {
    }

    public static validarIntervalo(dataFim, dataInicio) {
        let time = new Date(dataFim).getTime() - new Date(dataInicio).getTime();

        if (time >= 0) {
            return true;
        }
        else {
            return false;
        }
    }

    public static validarDataPosterior(data, minima) {
        // Parse the entries
        let startDate = new Date(minima).getTime();
        let endDate = new Date(data).getTime();

        // Make sure they are valid
        if (isNaN(startDate) || isNaN(endDate)) {
            return false;
        }
        // Check the date range, 86400000 is the number of milliseconds in one day
        return (endDate - startDate) > 0;
    }
}