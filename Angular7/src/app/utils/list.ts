import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable()
export class List {

    //#region variaveis

    public listaGenerica: Array<any> = [];
    private tipo: Object;

    //#endregion

    constructor(tipo) {

        this.tipo = tipo;
    }

    /** Incluir item na lista. */
    public push(item) {

        this.listaGenerica.push(item);
    }

    //#region single

    private msingle(lambda, tipo) {

        let elemento = this.listaGenerica.filter(lambda)[0];

        return elemento as typeof tipo;
    }

    /** Obter unico item da lista. */
    public single(lambda) {

        return this.msingle(lambda, this.tipo);

    }

    //#endregion

    //#region singleOrDefault

    public msingleOrDefault(lambda, tipo) {

        if (this.listaGenerica.length > 0)
            return this.listaGenerica.filter(lambda)[0] as typeof tipo;
        else
            return undefined;
    }

    /** Obter unico item da lista ou objeto indefinido. */
    public singleOrDefault(lambda) {

        return this.msingleOrDefault(lambda, this.tipo);
    }

    //#endregion

    //#region first

    /** Obter primeiro item da lista. */
    public first(lambda) {
        return this.msingle(lambda, this.tipo);
    }

    /** Obter primeiro item da lista. */
    public firstOrDefault(lambda) {
        return this.msingleOrDefault(lambda, this.tipo);
    }

    //#endregion

    //#region last

    public mlastOrDefault(lambda, tipo) {

        if (this.listaGenerica.length > 0)
            return this.listaGenerica.filter(lambda)[this.listaGenerica.length - 1] as typeof tipo;
        else
            return undefined;
    }

    /** Obter ultimo item da lista. */
    public lastOrDefault(lambda) {
        return this.mlastOrDefault(lambda, this.tipo);
    }

    public mlast(lambda, tipo) {

        if (this.listaGenerica.length > 0)
            return this.listaGenerica.filter(lambda)[this.listaGenerica.length - 1] as typeof tipo;
    }

    /** Obter ultimo item da lista. */
    public last(lambda) {
        return this.mlast(lambda, this.tipo);
    }

    //#endregion


    public select(lambdaWhere, lambdaElement) {
        var listaAux: Array<any> = [];

        this.mwhere(item => (item.ativo), this.tipo).forEach(element => {
            listaAux.push(element[lambdaElement]);
        });

        return listaAux;
    }

    //#region where

    private mwhere(lambda, tipo) {

        if (this.listaGenerica.length > 0)
            return this.listaGenerica.filter(lambda) as typeof tipo;
        else
            return undefined;
    }

    /** Obter colecao de itens. */
    public where(lambda) {

        return this.mwhere(lambda, this.tipo);

    }

    //#endregion

    public contains(item) {

        if (this.listaGenerica.length > 0 && this.listaGenerica.filter(x => x == item)[0] != undefined)
            return true;
        else
            return false;

    }

    public clear() {
        this.listaGenerica = [];
    }

    public count() {
        return this.listaGenerica.length;
    }

    /** Obter todos os itens da lista. */
    public itens() {

        return this.listaGenerica;
    }
}
