import { Injectable } from '@angular/core';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';

@Injectable()
export class PdfService {

    constructor() { }


    public exportPdfChecklist(checklist, fileName: string) {

        var pdf = new jsPDF();

        pdf.setFontSize(12);

        //Logo da Empresa
        pdf.addImage
            (
                'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAAH0CAYAAADL1t+KAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAPp9JREFUeNrsnX1wVed9558jJDBggWQJuwETX9rG8raxLdK46UJayzadNjsbTGZw7OAQC8dxh3RnYmbWO/1jJ7b7V7feGdszm6VxbCO/gN0mMwbvzKbdEFtuA0ljN8g4SS3cmIt5aQwILggJkJDunt+594K4urr3POf9POfzmZGvAd235557Puf7vPweSwFAKnjwx6/k7JvcZX9ZlP9YPQ3vXKz/z1bppr/G7+W//dm787Q+QPKxaAKAmAT9k1e67Zu28k93+a+vs6VakfbUv68jahdf46LLE0Gx4SMN2D+Fiuzt3z9QfoTK3xe+/YdfHODTBUDoAMbwtX9+pZKcK7e3lqXZU/fLV9R4kuiFPsPj1nwN/eXbN6f+2RZ+P0cHAEIHSJa0f/qypOmcI+2iWlhK1FYledeVYAaEXu8+hXLal59TZdnnv/1HX8xzVAEgdICw5d1TEra6rnzb40luCL0R/WXRS3f+gC15Ej0AQgfwKO+3Lsr75vJtd0MRIfSghF6LAft+Ivl3HMnfiuQBEDpAFQ+8/XLOKo1r31xO3d2eRITQwxT6tPtZpRTfX5Z8/9/cSnc9IHSAjAl8W3d5qdetZXmL0P2LCKFHLfRq8qok+TfLgme2PSB0AKME/i/bcvbNGkfgpSTeVn3oI3QjhF5NoZzgRfDbSfCA0AHSJ3CZYS7ivrN8m2skIIRupNCr7m/ly4Lf4ST4nrsKfFsAoQMkTeI/21aadV68KHEtASH0TAi9+m+myp3ueUDoADFKvJLC11xM4R6li9AzKfSpSHrfLoK35d7PtwsQOkDYEt+zbY0j8aIj8bagpIvQMy/0qRSmyH073zpA6ACBSXyrLW+rksTb6koBoSN0/0JH7oDQAYLiq3u29tg391kXJW65kwJCR+jBCr2W3J+nWx4QOkA9iQ9slYlt95W703OXH6AIHaHHLvSpryVflvtTf3PbXXm+vYDQAYkPbJUudBH4N9TUCm3TpIfQEXqihD4VmSH/lAjeljtL4QChQ8ZE/s5WWWJ2n/2/ve6kh9ARemKFPpU+JV3yt9ElDwgdzJZ4W1ngksZzbsSM0BF6yoReIV9O7X2kdkDoYFYaVzOkcYSO0M0UOqkdEDoYJPK9W0tpvDjD7mUIHaFnQ+gVnLF2W+x9nB0AoUMKJP6SLDF7SJW61dsanvwQOkLPjtArSBe8dMc/SXc8IHRInsjffSlnn+QeUU63ukv5InSEnk2hT0XS+mMsfQOEDrFz/7sv9dgHkYi8Z8aTO0JH6Ai90eP1O2K/nXF2QOgQg8hVWeRWo5M7QkfoCN3t4yF2QOgQkch//lJvuWs9N7MMEDpCR+g+Hy9fFnsfZx1A6BC8yFVZ5MVGMkDoCB2hB/R4iB0QOoQg8kYnVoSO0BF6WI+H2AGhg0eR/+IlmeS25TKRI3SEjtDjEnoFWcu+iTF2QOjgTuTTZq0jdISO0BMi9Ar9islzgNChpsh/+WKunMh7Lh4WCB2hI/SkCn2q2DfYYs9zFgOEjshz5UTeO+3kjNAROkJPutAr9JUTO2JH6JBBkUtZ1hlKtCJ0hI7QUyZ04VJJ2dspKYvQIRNs+OWLvZLKreoJbwgdoSP0NAu9Ql4xIx6hg+Ei/9cXZdezJ+yTR0/NDx+hI3SEboLQK23Tb/9s2nz7XQOc/RA6mCNy6VKXcfKH6goFoSN0hG6S0Cs8KYl9M93wxtNEExgv8177Zv9FmQNA1pDv/v6Nr3+3l6YgoUM6RV7qXlel7nVXCZGETkInoZuY0KfSr+iGR+iQEpG/d3H2+iPuZYnQETpCz4jQKzxmP+eTm++gGx6hQ0Jl/oKdxq1L5VoROkJH6Ah95ufMy2nDlno/Z0+EDkkR+eALbfaXU7rXey/7SBE6QkfoCL3xZ9enpBv+jrWk9ZTDpLj0y3yNkklvlsgcAEAbOXfs3/jD762hKUjoEFcqV07t9TUzpjUSOgmdhE5C1/vstiunG560TkKHaFP5VJkDAPhnDWmdhA5RiHzf1LFyF2mNhE5CJ6GT0L1/dn2KsXUSOoQi8x77Zs+MMgcACBY51+yx03oPTYHQIahv1b4XHrVv3lDVm6kAAISLnHPesKX+KE2RfOhyT7LI338hp4rqVft/u111idLlTpe7x8eky93fa0na41nhfHZSXe4Lm+9Ym+fsTEIHPZn3qlIXezetAQAJQM5F0gXfS1OQ0MGdyGU52qWJb0WNBEVCJ6GT0Eno4SX0qfQpJsyR0KGezJ+XK+A3FBPfACDhpytVGlunBxGhQw2Z95ZlzhcEANJAd1nqBBCEDhdl/m/PS8U3+WmjNQAgRTgVK22pb6Ep4ocx9HhFnrNvnFnsjcafGUNnDN3VZ+7xMRlD9/dakvZ4EYyh10Jmwd/GuDoJPYsy71HMYgcAc5Bz2X4K0SD0bMn8V88/pErj5XSxA4BJyDlNxtUfoikQehZkLmNNT9ASAGAwTzCuHj2MoUcn8jY1dRZ79ZgZY+gNXiNj6K4+c4+PyRi6v9eStMeLaQy9Foyrk9CNk7lTYUkxXg4A2aJSXY5zH0JPP/f96vk1RTZWAYDsIue+N9hjHaGnW+YfOMViZFkak98AIMvIOfBVitAg9LTKvFIsBgAASlCEJkSYFBe4yPvaSiK3St1LDSegVf+ZSXG1XyOT4lx95h4fk0lx/l5L0h4vQZPiZqJPsbkLQk+BzMsz2S2X8kToCB2hI/TMCV1gBnzA0OUelMz39+Xs7wWbqwAAuKOyuUuOpkDoSZI5y9IAALxJnWVtCD1RMqeMKwCANyrlYpE6Qo9V5muQOQBAYFJnrTpCj0Hm+b5eZbHGHAAgQKmzVh2hxyBz1pgDAITBFqSO0JE5AABSR+iAzAEAkDpCN1/mB5wxc2QOAIDUEXqqZU4yBwBA6gg9vXzlwBZkDgCA1BE6MgcAAKSO0JE5AAAgdYSOzAEAjJP6TqReC7ZPrZb5h7bMi5fL3Kq3JWmjbVDZPtX5+3mzZqsV7cuc24YUax2gLg/VgLbm9Hd/y9+XL+Fbc/p930G9h9hOXj7b88e/PqCGzo2E+hmlZPtUv5/Bhs2r1vZhLYQ+k8yljvCr04WF0P0K/Z7Fv6dWdXZxkEHmGTo3qv77T/4vQg/mouoLttS3c1SVoMv9ksxlpx+62UNi6dx2GgHApuOKeerCr4eUmpikMfwj3e/s0obQp8mcXdMAIBomi2rixGlVHL9AW/ijtEsbUkfojswPbskhcwCIGpH55OkRpB6c1HMIPdsyd7brQ+YAEIvUz4+rycIZut+DkfqrttQzfS7PrNDLMpdkTlcNAMSa1C8cO+l0w4MvustJPbNSz3JCfwKZA0AikDH1oVN0vwcj9ScQerbSucxm7+XYB4AkJXXG1AOh107pmVyxlDmhf+XQc73IHAASKXXG1IOUeubO85kSui1zKRzDWvMYGJ0YoxEAXCZ1Z0wd/CJr1Ndk6Q1nplLc+kPPdVuV5WnF2tXYZvozleIatU/j97B8wbVq1VXXK3V+gkOVuU+ZZt9HR9Rr777d+JhoslTzonalZjWF820xo1JcIwr2z22bV60dQOjmyFxmPe6x32yudDAg9KiF7jA+qYr5Ux6/mOHWcreilGwx6MeK8WscY7vpv+sEn+5maEdrTotqWjBfWS3NCN37cZe3f5bbUi+Y7rqsdLlLMs+RDWKmpUlZn2h3kgcAuPCWjKmfHmFM3R+5sgOMx3ihrz/8nIyZszwtQVjXtjpyBwB3UmdM3TfdWZj5bvRZ1Zb5Q4oZ7cljzixlXTNfqWakDuCKySIbuvhHZr4/ZHRYMljmParSzTJtHJox9PqvKaQx9GpGL6jikTMuK2Qxhu7qM488Psb3XFkYQ5/2DlqaVVPblQ3H1BlDr4tMkutH6OmRuUyC268qNdoRejKFXv634vsngztUETpCN1jozrtwMVEOoddFJsctM3GSnKl9nuyelqaryo8vcLrhAcCFy2Si3IlhGsI7lX08jMM4oa8/wiS41CFj6ovmKTW3mbYAcCP1iQnG1P1h5CQ5o4Ruy7xXMQkundgytzrmsqQNwC2yocuJ09R+945x5WGNOXvaMne2zlNOJbjqy9mqN8sYeoPXFO0Y+mXUKj6jc6gyhh5xVIzvubI6hj7tXdUYU2cM3TVGVZIzIqHbMpcxkS2KcfP0I8VncgtZpw7g1m1s6OIHxx2m7KFuylmTvc1Nk/rV8xlTB3ArdTZ08YMxe6invst9/b8/16uKVTuo0eVe/3cT0OW++pobG7wPVdrIZWTc87HxYeG4Gh0bU4PHDjf+Injswuu6eonqWrQ4suP94Mnj6qD9vo6PDNf+zCM3SXzPRZd7jXc4a5aa1bnQvm3SeM5Md7lPZcPmVWv7EHpsMn82Z9/ssQXdhtDTJfRnbloX6bEiEtyVf0/tObL/kgwDEPrq373F+YmaIfs97Dm8X+0cfMd+P2cQOkK/9C7ntKhZbjZ0QejVyHi6bOKST6sT097l/qpi3BxcsLStU93T/Vn1P/7TevVfVnzO+XOa6ZjfqlZdf5P6q8+vV/d/5nY1b/YcPmQoue78uJoonFFFxtR1aSs7JbWkVuh2On9UMW4OHli+ZJl69I+/qO6MIVmHwYplXeqRP7lLLW3v5MOFktQZU/eKrE9/FKFHK/Me++YRjj3ww+rfucVJ6/Na0p9uJbE/fPtqpA6XmCyq8V8PkdT1ecSWeg9Cj0bmlSVqAIGk9ftvud2I9yIXJvd/5jY+VLhM6hSf8UQql7KlTuhFy1lekON4gyClvtqQ7neZG7D6k7fwocKlc6Yt84nTIyR1PcQxqVvKliqhf/nXz65RlHaFEBChd85vNeK9rOq6kUlycLnUz48zpq6PlIZdg9DDkTld7RAqd3d/1oj3IV3vMlEO4DIYU/dCqrre05TQKe0KoSJd76akdHkvALWkzpi6FqkKkqkQermrfQ3HFoTNqk/cbMT76Lp6MR8m1MQZUz81gtTdsyYtXe+JFzpd7YAIeS8QsNTHxm2pn3ESO7giFV3vyd/9Qma1F+lqzzqDx46ov35ze2OJdS5RH2/rcJK2l+5zmSUuY9Cj4+dDey+Pv7FDDR493EDGS+zX0qH++PqbnTXmXpA16YNHj3DwwIxJffzYCdWyqMOOdhYNUh9xkMx630BC95rOP3IKyPRyLIF78R9WP3h/r3rsB3/n1G/3KsLY34ct/J377PfxD97fhwkFcyBk7IR+4USB7nd39Ca94ExihW7LnK528Iwk7P+1+/uekrYk4yS9j2/96Puh9hgASX3i9Bmk7o5Ed70nOaE/pCggAz44Xt6RLO3JVt7HwCH998EYOriWujOmPqyKExM0Rn1yZTchdI10LpuuUKsdfCNj7ybg5cIEQDepXzhO8RkXSK33RG4MltSE/gTHDASVbnVJ4taqo+NjfJgQPlJ85qPjJPWUOipxQrfTuXRn9HC8QFxQNhWyLvWJkxSfaUCPndJ7EXo9mR99RiYb0NUOABAjFyfKkdTrpvSkTZBLWkJ/QlmsOQcAiF3qY+OMqdcncQE0MUK307lMMujlGAEASAiMqTfioSRNkEtSQmciHABAAqXOmHo63JUIodvpXJJ5D8cFAEDyYEy9LomZIBe70JkIBwCQAqlL8ZmhUzREbR5JwgS5JCR0KsIBAKRB6nZCZ0y9JjmVgApysQr9y8eekUb4BscCAEBKYEx9Jr4Rd0qPO6E/wjI1AICUJXU2dKlFZYvV7Am9nM57OQYAAFIodTZ0qYVssZrLYkJna1QAgJQndYrPJMdtsQj93mPP9CiWqQEApJ/Jorpw9ARJ/RKyjC0Wv8WV0FmmBgBgSlK3ZT5RGGZMPWbHRS500jkAgIFSZ0w99pQeR0KnxCsAgIlSZ0w9VtdFKvR7jzslXrv5nAEADIUNXSp0R10SNuqEztg5AEAGpE7xmeidF5nQy+k8x5EOAGA+bOjikIsypUeZ0EnnAABZkvrYOGPqEbovEqHfO0Q6BwDIJIypR5bSo0ropHMAgAxLPeNj6pE4MHSh2+l8DekcACDbZHxDl1wU69KjSOhsjwrgk875rdr3GR0/T8NBsqReLj4jiZ2UnjKh3zv0Hbki6eEwBvBH16LF2vc5eHKIhoNEJvXxY0NZlHro1ePCTuiMnQP4ZGlbp1qx7AYaAsxBNnQ5Uchi93uoTgxN6HY6z5HOAfwhXe33f+Z2T/cdGjlNA0Kik3oGx9R7wtwvvZl0DhB14u5o+DvzWmar5UuWqe5rl9n/P8fT8xwfGaaxIdlSL4+pz2pfoKxZs7KU0jekRuj3nrDTeVH1crgCTOee5Z+N5HkGjx6hsSEVSV2Kz7Rc05mVt9xrp/RNm1etLQT9wGF1uSNzgBhB5pAqsld85qEwHjQsobNUDSBG9hzeTyNA6qQ+URjOyph6KI4MXOj3nviOpPM2jk6AeJD157v3D9IQkDoqY+oZSOptYZSDDSOhk84BYkRkPjpGURlIqdTLY+qk9JiFbqfzHvumm0MSIL50/trP36YhIN1kY0y9O+hCM0En9Ps4EgHi41v/9PekczBG6hnY0CVQZwYmdDudy7h5L0chQDxs+efXmd0ORnGx+Iy5SV2WsAU25yzIhI7MAWJgaGRYPfYP31W7mAgHJkpdJsoNnTL5LQbmziALyzAZDiBidu7bq1752S4aAsyWup3QZUy9ubPdxIpy4s4nEyP0dSe/06OK7HkO4IaDheNqdGys5r9J7fYOja1SGS+HzFAeU5+1sFVZzc0mvTNnr/TNq9b2JyWhMxkOwCWv7NmlBo8ervlvXVcvUQ/fdqfrx1ra3kmDQnaSenlMfVbrlcpqMUrq4lDfQvc9hm6ncybDAQTETKKfia6rF9NokC2pm1l8JpDJcUFMikPmAAEiXfJukZ3YOjW66AFMSepO8ZnJoklva00ShE53O0CgKV1v6RkpHTKJLfMLJwomrVP3PbHcl9DXnfxOTlEZDiC2hC4wjg5ZTurOOnUzpC6V43JxJnSWqgEEzIcn9YROQodMS92sMXVfTvUr9DUcTgDBJ3Spye46obeR0IGkbsiGLr6c6lno605+R544x6EEEILUTw6R0gF0MGNDl5yfDVv8JPQ7OYIAwmHwmO7ytSU0GoAUnykMp31M3fNEcz9Cp7sdICyha850X9reQaMBqPKYero3dPHsVk9CX1d4Wp6wjUMHIBwOMjEOwJfUUzym3rZx5/c8Sd1rQqe7HSBEZFKc7KLmFgrMAFSR7jF1T471WgyX7naAkJEysCuW3aCV0o9HuIXqvNlz1Ipcl3MbykXN2Hm1c3AvBwL4kvrFDV3SVftdHLshdKHT3Q4QDR8WjqsVGr/vFJiJUOgrlnWpe5avDPU5ZOhBdz4BwFQubuiyIFUbujjd7ptXrd2ucycvXe50twNEwMFCspeuSTc/QCqkLhPl7KSestrv2q71InS62wEiQHfnNSkwE1b3N0DqpT4xocaPDaVJ6tqu1RI63e0AUUtdc/laG8vXAGYkXRu6tOkWmdFN6LdyRABEh+5GLRSYAWiQ1NO1oYtWt7uu0OluB4g0oetWjGM9OkBDqadnQxct57oW+rrC07JNao5DASC5CZ2KcQDuk3oKis9IbXfXW5TrJPQeDgGAaDk+Mqy185rMPGd/dACXyJj60RNJT+qu3asjdJarAcSA7sS4jzMxDsB9UrdlnvANXVy715XQ1516uo2EDhAPTIwDCFnqyR5T79m483uuVpe5TejIHFKJCfXN2XkNIAKpJ3tM3ZWD3Qqd7nZIHTKevPp3bjFA6BSYAYiE5G7o4srBJHQwUuQrczeo/9Zzp6eErrt1aRRoz3ZnHB3As9SlTGzCxtRdObhhpfp1p57OKZarQcx0LVqsnl379UieS2dWeWRCty8yJHm7bq+rl7CpCYBHLm7o0taqrFmzkvCSZPlabvOqtXm/CZ1iMpApdPYhj4rBY3pypsAMgE+py0S5oVOpSuluhE65V8gUHxaS1+X+4UkKzABELnXZ0CU5Y+oNx9HdCL2HjxWylM4PFpI5hk6BGYAYSM6Yur+Evu6UU+6V3dUgM+w5vD+xr+3gSb390SkwAxBQUk/Ghi5tjcrANkropHPIDJKAd77/TmJf3+Ax3Y1aKDADEJjUk1F8pse70C3GzyE77Ny316mdntyEzjg6QNxJ3Sk+M1mM6yXc6iehd/MRQhbYnX9PvfaLtxL9GrUrxlFgBiB4ZEOXE4W4ut+9dbmvO836c8gGu2yZP/fT1xP/OmVIQHdJHQVmAMJJ6jGNqTvr0b0k9B4+NjAZEeRzb73u/KQF3TKwjKMDhCT1+MbUZ3RzvUpxN/ORgYnIErAfvL/XmdGexKpw9ZA18iu0hB5egZm0tR1AGEldxtRbrol0iejNXoROQofwkubIUaXOXdCYXGJ5f67y7PD3jh7RXs/dCOkC163iNjrm/fllHF1nLD1M6e4c3KsOngh3zT7layHxlDd0ae5sj6pMbI/2WXLd6adLZ9rK+bb6vFu8/K7WtH+veoqqx7Fq/m75X2Z8rJmey6r7+9Oe0+XvX3pFluvfnel9T2voRveb8bXXu1ysun+x3u/Ve02Wq/tPe8UN3sO0hzk0bBvH7RiUS6F7nHxqRTlptRj0Y1kqNmJsN/13HWM7JakdjYjGyXtJ1uwWNWvBlcpqaQ79uTavWlvzYG6aQeakcwj/C3Btq7I65tIQAJD+awwZU5eJchGMqW/c+b0e10JXLFeDqGid7YgdAMAEqTvr1MOnW0foTIiDaGixD8G5zaWfJov2AIB0Ux5TDzmp30xCh8TipPQ5s2gIADBC6iFv6KKV0BE6xCJ1xtQBwARCLj7jTuhMiINYkTH1RfNoBwBIv9Rlopyd1MOg1sS4JtI5JAoZU5/foqyrSOoAYIDUJybCGlPvdiP06/gIIHapX3VFaaJcSxPtAQDpJpwx9etI6JAarCWtympltzAAMCCpBz+m7iqh99D0kBipL5itrE7G1AHAAKkHu6FLT12hl7dMBUgOzU3KupIxdQAwJ6kHVXymeivV6qKzCB0iYfU1N9Y54qck9PLt8bERtevdvap4fkJjQ5fg6ZzfqjrmL1Bdi/ztYnbw5HF1fGTY2SzGcxv+7i1qekvVeU77ufYc2h9YWyxt71TLlyzTus/uD95z3rfntr+y9eKWsF7LEH14csjZVEc+A4BYsM9hF46eULM6Fvrd0EWcna8tdMuO8GwSAHELvQaDpz9Su696X6kTZ1Xx7IVIX6uIZNUnbnbk1TE/2DK1shvagC3ZH+zbqy331Z+8Ra8Njx4JVOjSHqs/+Wm91/DRYS2hS9uv+M0b1MplXcG3/diY0/a79w/ar4td3SDipD4xoSYKw343dOmxf/prCr2oigstRflNSCB2Mp88PKyafrtdWbbQ5f/DZl7LHCcFr7r+plCfY8WyG5wf2Z/9b/f8yLXwRNA6+513BixEL4/ndjvUebPnqFVdN2lfMGi1/ezZ9sVCl/Mjr2vLj99wkjtAZFIvj6nPal/gNakvnPqH6klxzHCHRDP5bydVcXwy9DH1pW2d6uHb7gxV5rUS7zf/5IsXu5QbMTSiV7Ai6ISr+3hu94GXC4WHb18dqsyrkQujb37uLkfuAJFK3d+YejdCh3R/AU6dU2rClnpIG7pUZC63USOJXZ7bzdj08VH9NCnj3oG1U3uH1u8fLAy5Subf/NO7An2dOol9wx/chtQhhqTieUOXukJvo2Uh8ZyfUMWRcaXmtigr4A1dRKj3//7tzm2cbPjM7Q27tN12X1/+/mYH2lZaQncxCU2SuUg9Tu7+1MpYLigAqXsoPtNWU+hfGv52Dy0KqeHCpCqeOKusxa2Bdr9LF3scybyWLDf8/h11f0e3y11w253f+HH0Z/k3mhuw+sZbEiFSSep3/94KvmMQOReLz2gk9ak13amrCem+qP3VydJa9QCKz4hEoxwzdyPNegL2svxLZBWM9PRTdL2E7kyCu/7GRLV997XL+IJB9FIfG/c8pj5V6CR0SOcXQMbUJ4ulpN7s/Rp1xbKu2Lvaa/UY1EO32z2o3gcvj1NvDH35klzsXe3VrGQsHWJLKlpj6iR0MAgpNnN23Plfy8dmLitzNyTurcnkuHoXGbKOXYeglq51epjhXm+Wu6w1Txrd1ybvIgOyJXXdMfWpZ79baUFIbUo/e6E0pt4+V1kL9U/CcuJOwth5LeqNV+tWOwtq6Zru4zSa4e5lTD4qqQPEdl5zt6HLrSR0MPOi9siwUnOatcfU/ZZyDZN6E8W8lI4NYuKZ7pK1egVbkirzoNoKwJfUy8Vn3JS8nloproemAyO+AKfOKWv+bGdMXVK7qxO3j3S+O/+eU+VNxrNrdYHLxLbli5d5nnBX77V5mhgXwNI13bkG9V6nH2lK6dZdHwyqfdL2Nbr0u64pTW5z5kd46D7XvXABCCupjx8bUi2LOmrV3uipJXQAM5Ax9aZxpVrnOMVnpLKcLHOrKyiPY6Vbfvq62mULvR6DRw87P/J7UjRGV4b1ft/LBiNygeFlDbufRC013ANv+5+8rnbbMq//vEecH/m9/3qH/hr3pE2ShAwjG7qcKKhZC1tnrP3udLl/afjbVIgDs65oZUxdkrqMqS9ofFL2ktAlmTeS+WXyLRxXr/3ircBTom63e1BL13So1+XutSZ8I5lXX/j87c92k9Ah9Um91pj6xp3f674odEWFODA0qcuYutV+hWpa3Br4w0s3u/ZFwP5B7fs0Som63e5+J/95KU5T7zV6mai387292vcZCHCnOYDYpF4eU69a0taG0CETSPGZ4vD5wDd08dJtLWPsQe/opdvt7nfpmm7C99O9H9RFjNP2Y2N2Ww3xhQAjknpV8ZnLhE6XO5j9BZA91APe0EV3DbgfGdVD9wLB79I13YQfxpakXuYO+PnMAJKXVC4rPnNZlzuA2Ujt93MXSmPqAW/oEjdeLhD8pHTd+x5nj3GA0KQuxWcmz55TU4V+HS0DxlMZU/9YqyN2U/DSpe0npWsXlfGYpgGgMc5EucLwdVOFnqNZIDMXtR+UNnRpWrzAmPek263tde23l/uR0AFCJzdV6ADZuqo9dU4VR8aUNbclsDH1ONGVptf11V6K0pDQAaKBWe6QTcYmHKGrK5qVNSf99ZV0u929llvVXbKGzAEigVnukHFkotzJs86Yuog9zQyNnNZL2h6Ly+jej+52gEhgljuAMPnBCaXOXUj1e4iquIzu/RrtsgYAwYHQAQzAizi9LF3rZIY7QHKF/qXhb+doBoB0IzuN6RZN8bJ0Tfc+tXZAA4DgeeDFzTlJ6AgdwISUrlnWVHcJmpcla2GUfQWAmuTocgcwRei6u65pb+OqNyFuiAlxAJGC0AEMQXdinO7SNd0la8xwB0DoAOAloZ8Md1/0JOyyBgAIHcB4hkJeuqa9y9qZ03woABELvYdmAEg/Ye+6xi5rAImmh4QOYBC63dw6y9B0l6zR5Q4QfUIHAEMIa9c13SVrrD8HQOgA4IOwdl3TXbJGyVcAhA4APhg8eljr990uXWOXNQCEDgARotvl7nYpGrusASB0AIiQsHZd095ljYQOgNABwB+6JWDdLEfT3mWNMXQAhA4A0aZ0N8vRdJasOTu/McsdAKEDgM+EHvCua7pL1kjnAAgdAIIQesC7rrHLGgBCB4AYCHrXNXZZA0DoABBHQg941zXtXdY+OsyHAIDQASAQqWt0uzdakqa9yxoJHQChA0AwjI6Naab0mcfR2WUNAKEDQEzo7nS2tK1jxn/TWbLGDmsA8dFMEwB4Y/Xv3uLpfrqJ1wtDI6e1X9NgLdG3J7O7ffWNtygroW0PgNABMiL0KNAvLrOg5t/rLlk7HpnQP80BCFCFdLn30wwAZqHd5d5eu8udXdYAUkM/Y+gAhqLT/T1TcRl2WQNIV0IHAAPRketMxWXYZQ0AoQNAzGiXgK2xdE0noSNzAIQOADEn9FIa7/CV0OluB4hf6HmaAcDAhH7S377o7IEOkCryTS+3/hlCBzBR6JqCrV661qErdLrcAWLjmfUb83S5AxjK6Nh5NTp+3vXvVy9d0y0qI88HAPGB0AFMTukn3af06qVrjfZJr4ayrwDJEPoATQFgoNA1ZrpXL11rtE/6VNhhDSBWHIdXSr8WaA8APR7v3+Hpfvd0r9Re3+0VL7uuVbrOdZasRT3D/fEf7vBQy91Sd39q5YxV8QBSTGGq0AFAk8GjhyORrP/X6L7uuSxdq3Sd61x0RN3dPvjREU9C15lTAJA2Kl3ueZoCwDx0u8IrS9V0l6wNnTlNYwPER36q0A/QHgDm4XXXNd0laxSVAYiVA1OFDgCGotMdXhlf1l2yxgx3gPhhljuA4XjZdU1nyRrrzwFiZ2Cq0JnlDmAoXnZd01myRslXgNgpIHSADKA7G1+WrrHLGkBKhf5y65/R5Q5gKKPjesvkZOkau6wBpIdn1m+8rMsdAAxFN0F3Xb0k1McHgHCYKvR+mgPAUKn7KAHb+LEZQweIkf5aQgcAQ9HpFtcpjers6MYsd4DEJfQ3aQ4AQxO6j13XSOcAieZNEjpAloReCGecm13WAJKZ0PtpDgAzCWsmOjPcAWKnn4QOkKWEHtJM9MGPDtO4AElL6C+3/hkJHcBgwugep8sdIF6eWb9xxoROxTgAQwmje5wud4BYKdRM6GWoGAdgKEHviMYOawCxM4DQATLI0MjpgB+PdA6QWKFbyjpF+wCYSdDd43S3A8TOqZkTepGlawCmEnQXeVhr2wHANf0zC12pPO0DYC6j48GVaSWhA8TOZc5unvqHbQsezK87/TRNBImja9Fi9ezar/t+nK9+939nuh2lBKzu5iszPxYJHSBOnlm/MV8voU+L8ABgkNAD6ianux0gdqa5upbQmekOYChBdZPT3Q4QOwNuhH6AdgIwNKEH1E2us3sbAITCARI6QJaFHtB2p3S5A6QgoW9b8GA/7QRgJqNj5wOZ6S6PAwDxMbWGe72ETkoHMDmlB9BdTtlXgGSlc4QOkEH8lmyl5CtAuoT+Du0FYCZ+Z6gzwx0gdt4hoQOAGjx62Of96W4HSE1CZ2IcgLn473I/TSMCxEitCXFCc4MrgG6aDkJJiSNHlTp3QanJost7WKG+niiXYcW95Eu6zP2k7A8Lwa5Bj7KELOvnwdR0Xvcsue7000/YNw+pyvm2+rxbvPyu1rR/r3qKqsexav5u+V9mfKyZnsuq+/vTntPl7196RZbr353pfU9r6Eb3m/G11/mYi1X3L9b7vXqvyXJ1/2mvuMF7mPYwh+ykOHohWKEXvX1DrGKEX8di0I9lxXdqibHd9N91jO2UpHY0gWy315N2Qt9U6x+a6tyJiXEQKta1rcrqmEtDAAC4Z0Y31xN6P+0GodM62xE7AAC4ol9b6LKVqmJ/dAibFvsQnNtcSuotTbQHAMDM5Ku3THWb0AWWr0E0XHWFUs0IHQDAq5Prn0GL6k3aD6KCMXUAgLq86V3ojKND1MiY+qJ5tAMAgKaT6wp928IHJd4XaEOIDBlHn9+irKtI6gAAUyg8s36jjy53UjrEJXUZU5/bzEQ5AACXLnZztmQcHWLBWtKqrNY5NAQAgFI7ghD6dtoRYpO6ndSblrBOHQBI6L6Fvm0h69EhZlqaGFMHgCxTd/15hWaNK4Ne2hSC4uHfWqVfj/nshdBej2wQ8srAj2r+29K2TnXP8s9qPd7u/e+pXfn3vLfPbWv0Xn/Bfv0/2+X69+/51ErnfYXB6Nh5+/UMXWxX2Qwmyg1YatF1zWLn/c6bPUfNtX+Wtnd4epyhM8MXd6uTDW5ksxd5vwBxp3Mdoe9A6BDoCXb+1fp3ujKe1yoS6Fq0WOs+fvcc77p6cajvSeQW5nMsv3bZNMmLAHfvH1R7Du2PTOIrlt2guq/NOZ9hIEw5bD9/8WJwSO0c3KsGDuWRO4TFDje/pJPQAQA8XxSJ5OVHEu5r776ldtlyD4OO+a1qw3+8PfSLoosXR3ba3/AHt9kyH1NbfvKGLfb9fOAQS0J3tSZo28IHC0gdAAIT7h/crh6+487gknOZFb/Zpb75ubsik/nlFy2z1Z//0Z84cgcIUubPrN/oqh6MziLfHbQrAASFSPevPn+vnXCDGcsXmcuFQtAXCd5eB1KHwHDtXh2hk9ABIOBUO0c9fPtq1Tnf39JESf13f2plYt6XSH31jZ/mA4ZAEnrgQt/W5pSBzdO2ABC01P/8D//U12PImHncybyaz9tC75hPDQXwRb5RuVevCV2gyAwABI50u6/qusnTfWU2exxj5m4gpYNPtJyrK3TG0QEgHPl98tOeUrbXC4EokK53Ujr4QMu5WkLf1vZgv2L3NQAIAWdp25Kc9n26q9a8J43lCX99kFhkd7X+MBO6dhcAAIBbdOV8fUK72qciQwIAUbjWi9DpdgeA0NKsTrf7x9s7E/+e0nDRAYlE27XNunfY1vbg9nWFp6XbvY32Bq88sHebKh4aVmrUXX32Z9f+udbjDx47ov663/0FrlXkM2n4mb2yefpfFmtLWca1vU5UW9rW4ZSJdfW7HmuyS1W3Hw7uVXsO5RvWmZcxcEnZqz3OWpeCM3KRQllY0EC62yNJ6J66AgCmSXTRPKUWzKYhDENqtT/+wx1OfXMvdF2zxPXvzvUwiU5k/j9/+Jp67d23XW0aI6Vqd38wqP5ix1anXrunixSPFx6QWTw51qvQ6XYH/8yZpawFc5TVwdaoJiK7v7lN2lPRKTLjpSDN39qvy+vub1KrXS4IAELGk2M9CV263RWz3SEI5jYrddUVtIOh7P5AfwtZnW5tL13gXlN2Kd2fV/s8XKQAaOCpu91PQvfcJQBQC+sT7SW5g1EMJkx+0n3udyw77r3dwXg8u9WP0J+n3SFQqTOmbhzHbYHyegCicatnoW9r/1q/orY7BEl5TB2pA0BGyesWkwkqofvqGgCoydxmZbVdQfc7AGQRX071K/SnaH8IJakvof41AGQOX071JfRt7V/L2zcDfAYQBiJ1i+53AMgGA8+s35iPM6GT0iE8pPu9dU5pXB0AgHQeutAZR4dwpb4QoQOA8fh2qW+hb2v/mhSY6eOzgNCYM4s2AACT6Xtm/UbfxdqaAnoxrEkHAACI0aGBCJ016QAAAJ7wtfY8jIQuMDkOAAAgJncGKfQ+PhcAAIB43BmY0LdexeQ4AAAAHZkHMRkujIQuMDkOAAAgBmcGKnQ7pfcrKscBAAA0YiCoyXBhJXSByXEAAAARuzJwodspvc++KfBZAQAA1KRgp/O+xAudlA4AABC9I8MS+pN8XgAAANE5MhShs4QNAACgJoEuVYsioQuP8bkBAABE48bQhL6142t5+6afzw4AAMCh307n+dQJnZQOAAAQnRNDFbqd0vtJ6QAAAE46D9WHTRG8CVI6AACQzkMmdKFv7XhArkjyfJYAAJBR8mGn86gSOikdAABI5yYI3U7pfaR0AADIaDrvM0bopHQAACCdGyL0rZ2kdAAAIJ2bkNBJ6QAAQDo3QejllD7AZwwAAIYzEGU6jyOhC5v4nAEAwHAid13kQt+6yFmX3s9nDQAAhtIfxbrzJCR0gbF0AAAwlVgcF4vQSekAAEA6NyOhCxv43AEAwDBic1tsQn9p0QN5+6aPzx4AAAyhL8z9zpOc0IXHVFEVOAYAACDliMtiXcUVq9DLKf0pjgMAAEg5T9npPNaA2pSARnhSURIWAADSS77ssliJXegvXf2AXNGwjA0AANLKY3Gn86QkdJF6n2IZGwAApI/+qEu8JlroZSgJCwAAaSMx7kqM0O2ULpu2PMmxAQAAKeFJO50nZsOxpoQ1DsvYAAAgDSRu/leihF6eIEfXOwAAJJ1NSZgIl+SErl665qt9iglyAACQXBIzES7RQq9c+XC8AAAAjkq50O2ULpMMWJsOAABJ47EkTYRLQ0IXqCAHAABJIq8SvBorsUK3U7pMNmCLVQAASAobkjYRLi0JXaTer9hiFQAA4ke2Ru1P8gtsSnwTFp3JB6xNBwCAuEjFkurEC/2l36DrHQAAYiXRXe3pSeglqW+3b7ZzTAEAQMRst2WeCv80pahRJaXT9Q4AAFGRqh7i1AidrncAAIg6SKahqz2NCb3S9d7HMQYAACHTl5au9lQKXbBKs97zHGsAABAS4pjUlSBPndBf/Bhd72A2nfNbaQSAeElVV3tqhV6Wer+i1jsYKuWO+Qu07zM6fp5Gd8HStk4aARrxWNILyBgl9LLUH7VvBjj2wDRxLG3r0L7PwZNDNLoL5s2ebf/M8ffZtnNRYDADtswfTeuLb0p5439BsZQNQubgyeOehN519RJPz7dy2Q3mXNiELL/RMf2eiVVdN/q4IJijrr96MV8KMymUnZJaUi10O6XnlbLYOx3ClYbH7ux7uleqeS16aXDV9Td5SvdDI6cT2Xb3fGpluBdbBf2eiTu6bvJ8oXH3p1Y4KR+MZJOdzvNpfgPNaf8EXvzY/X3rjzx3q/2/vRyPEJ44jmuLVn7/r/7zl9Vrv3hLDR494jzGTEiaF5kvX7LM0+s7PjKcmLaS+QNddopd5UOcYSJC/ubn1qqdg++qgUP71eBHR1yl8lU33Oi8L0/HD0MiSUeWqPWl/U00G/JhSErvLv8AJELojgzshH5P92dDf31ywRA2z9yzMXGfyz5bxl4lK13vfrrf3TI6NuZpaAAiY0ClcIlaLZpMeBMvLr6/spSN8XRIrTBNfG3e0qz7OQsfepjfEPlFh2Gfj2E47kjjEjVjhV6WujFXWZA89hzez2tLoNDTIMtGXfoQKzJubsxqqSaTPhlb6n2K0rAQAjIxLqnilHFgsy6e8u4/l7HziX//ewz7fAzCiHFzY4Velrp0vbM+HQJn5/t7E/eadu9/L1ET4oKQn+54887BvYl9P7s/GFRDBn0+BiHrzY2rONpk6Id1m2I8HQJm8OhhNXgsOd2n0mvw2s/fMuuiyYOcpUs7qfMIXnv3bb44yaNQdoRxGCn0F5fcn/oCAZBMtvz0h4kpsyoyNymdi8y9innLj19P3Ezy/2PLnHSeSL5gyiS4rCR0kXq/YpIcBIwI9PE3dsT+OqSrfee+vca0q0yEe+3n3tOsiHPLT95IzPuRrnbSeSLZlNY67ZkWelnqTyomyUHQ8ikcV4/374gtqYvIn/vp68a0p4ybP/76a74TtkyO+9Y//n3sSV1knqSLC7iITIJ70uQ32GT6J2hLnUlyEDgynv6X/+/vIh1TlxT6rR99X72y50dGtKGI95Wf7VLf+qfgJCxS/8vvfzeWme/O5/OP/4DMk4mRk+Cqac7IhykTIPbYPzmOawiKUvf7dqds68pcl+pesky7drurBHt4vxqwf3btf8+YRC7CleVpYaTpklj/3ik7K+Vnu6/N+d5hrf5FRN55T5LMIZHklaGT4KqxsvKJrj/0XLf9ZuXSuU0Vq952UdX9szW1mRr87qU/W5f9eVpDN7pf1d9bM91v2u/Wea0zPWaD165Uo/Zx9x7cPNZMr3H1b9ykisP2yX980rVsd+XdC9AqBnOcidw757WqDh/7oV/qBTjijC3X7dovevsar1zW5Wnfdc+SPXPa+Ux8zUYvej95idzlM/l4QLXlpZyrDL0EVjSmqCCc9nJmtJtUPAahl/nKoefW2DevIvT0CV3+r3h4WBXPXgjlxGgVE3kycvlYMX6NY2w3/Xed4NMdQg+rvWRG+/asNEtTlo6BF669Xz7YDXwbUnr1uaRVWVfNpSEAwA0bsiTzzAm9LPU+xcz39Ep9wWzVtKSVhgCAehhX1hWhzyT1pRs2IPWU0mwfsnObS0m9uYn2AIBaMs9kT2yWz4hSdIblbGlN6u1XKGt+C1IHgKlketfNzJ4N7ZReqeeL1NMq9c55ylowh4YAgIrMbzO1rCtCdyd1qfnORi5plXrrbEfsAJBpnHN5lmWeeaGXpZ5X7M6WXpqbnK53Zr8DZFrmkszzWW8IBiBF6h/fMIDUUy51GVOf28yYOkA2Zc7QKUKfJnXWqKcYa3ErY+oA2WIDMkfoM0mdwjNpl7qd1JsWs04dICMy304zIPR6Uu9D6ilnziykDmC+zPtoBoTeWOrXIfV0H9VWqfiMjKk3WbQHADJH6EgdqacZZ0zdTusAYJDMv4zMZzzn0QT1+cqBLb2WsrZc/At2W/O029q8WbPV0rnt9dugxt9byt1rqEfx/IQqHj+bnC9CMWGPk9LXkNbd1o6fGXb2bE/cZ4nMEXoWuO9AX699swWhexf6PYt/T63q7OJggswjMv+LHVsROjIPHLrcXfD8db1yINH97oNp6Rwgo3TMZ8ImMkfo8Uu9iNQBAJA5Qk+/1HMkdQAAZI7QkToAACBzhI7UAQCQOSD0MKReZOtVAICAKG2BiswReixSX9YrdYTZpQ0AwL/Mb7NlTm12hB6r1Nl6FQDAv8zZNQ2hJ0bqy+0fDsgZGJ0YoxEAoBrn3InMg4FKcQFy3wd9bfbNG/ZPN5XiLv/95QuuVauuul6p8xMcqlQFyzT7PjqiXnv3bY6JkswlmdO7idATLfUn7KbtRehVfy811T887fFE5vJQ9XhitKI8oRaDfqwYv8Yxtltaa7lzkefQZ/9sQuYIPSVif15qv/ci9Om/X/yV/R2eLCJ0hI7Qsyn0PlvkLPsNAcbQQ+L537xPDlgO2lqn1cVXKtXCoQeQQTYgc4SeVqn32TesVa9mbrOyrpmvVDOHH0BGYI05QjdA6r9133artKwtT2vUkDoAmI6c+1hjHgGMoUdE76+enzIDXmV6DL3634rvnwzuUGUMPVoYQ09fO0YLM9kRutFiv2yyHEJXpdnvH400WNKG0BE6Qk8ZTH6LGLrcoz7Cf8uZLLeJlpjCnFnKumqucwsARrAJmZPQs5PU/+35HvvmVfunLfMJvcLZC6p45MylJW0kdBI6CT1tVCa/9XOWR+hZk/qlcXWEXmJ8UhXzpxA6Qkfo6YPxcoQOtti32F/oXoQ+ReqHh51bhI7QEXoqeNIWOUOJMcMYegLo++2LRWi4shVampTVOc9Z2gYAiUbOWRuQOUKHqVL/hFOERtars+uQML9FWW1XMFEOILlUutj7aAqEDtOlXtlbnS9IWepNH7uSdgBI4OlKsYd54mAMPaH0vv9Cr5Jd24qlWfCZGkOvfo0XJtXkh8MzzH5X7p+r3heBMfT434vmczGGHgvSxb6JVI7QQV/qOfuLLkvbujMtdOHshJo8OlI1UQ6hI3SEHiGSxmVJWp6zM0IHr2Lf98Kj9gf1SKaFbv9fcWRcFU+crV9RDqEjdIQeBo/ZIn+UszFChwDYsO+FHvtGlrflsip0B1vmkwdPI3SEjtCjQdL4BgrFpAMmxaWELdd/Rb5Qy1XWJ8zNmaWafrudAwIgfORcsxyZk9AhzLQ++MIaJ61XysZmKaFXOGcn9V+fcSbMkdBJ6CT0QKmsLWe7UxI6hJ7Wu74iX7Rl9k92v3CS1K+er1QzhzBAgDjnFmROQoe403qWEnrlfmcvlJJ6ZUkbCZ2ETkInlZPQIdVpvZjRsfW5zapp6QIOBADv9JHKSeiQtLT+3gs99kcqaT2XmYReQWa/HxtxxtZJ6CR0Eror8ooZ7CR0SGhav+HiTPjHMvfm58xSVvtcZbGhC4Ab5BzBDHYSOqQirf/ri91KSscq1ZOJhF5htGpMnYROQiehT0UEvoka7Agd0in23rLY2zIhdPl7qf1+4BRCR+gI/RLUYM8AdLkbzpb/sF6+wLLE7cnMvOnmJtV03UL76OZ6FaD83V+GzEnoYFZaL3XDF0vd8MYm9Mr/Su33kw1qv5PQSejmJvR+Rfc6QgfDxf7LF3vtm0csVVUX3jCh60gdoSN0g4SeV6XNVEjkCB2ywP2/fFHG1B+yf76hKuPrBgrdQZa0HTqN0BG66UKXcfKn7J8nbZkXOMshdMie2CWly9asvcYKvczk/sKMs98ROkJPudAljW9C5AgdoCT2olNCtsdUoTtlYo+OTN/QBaEj9PQKvV+VisPkOYsBQofLxf6Ll3rKib3HOKHXkTpCR+gpE7qI/DEKwwBCB3diLzrr17uNErr8+zlb6keGETpCT6PQZcb6JkQOCB30xf7zl3rLiT1njNDLjzf5wQmEjtDTIvS8YuY6IHQIXOyGCF2NTajJo2ecW4SO0BMqdEQOCB1CFHvx8sSeWqHLH0fGVHFoVFnjkwgdoSepHaVr/SlEDggdwhf7u5cmz6VZ6A7nLqjivw9rb+iC0BF6CO+tXzHZDRA6xCV2a9qs+JQJXf5kJ/TJDyNawovQEfr094bIAaFDMvjquy/lyl3xvakUemWXtiOna65TR+gIPST6HJHfyzpyQOiQNLHvfanNPqxmKCmbcKGr0pi6On1eFc+OI3SEHhaXSrTeS2U3QOiQCrlv7XXEXqyxlj2hQq9I3dnQZWwinIZB6FkVemmi271MdAOEDmkV+ztbe+yb+5TTHZ98oTtcCHFMHaFnTegi8OdtkfdzNgCEDqaIva0sdemOzyVa6BWpHzoV/Ox3hJ4FoedVqVu9j251QOhgfmovzpDakyJ0QcrESvGZICfKIXSThU4aB4QOGRX7gJPa15RTe3fihK5CGFNH6KYJfaCcxreTxgGhA5TknlOlSXQi+FxShO5wfkJNHj6F0BF6hbwIXJUmueX59gJCB5hJ7ntKE+msUnpvi13o5ftO7j+B0LMr9EJZ4nSpA0IH8MIDe7baUrfuVBflHp/QnQ1dPhr2N6aO0NMk9IrEd9gS3863ERA6QGBy3yZSv7PcLd8WudAFmSh3zMdEOYSedKEjcUDoAJHK/Wfbehy5q0tj7pEIvSL1jzxu6ILQkyj0/BSJ9/PtAoQOEJ/cZYa8LIW707kNW+iCLfPJAycRenqFLuLeIbe2xAf4FgFCB0ia3P9lW1tZ6hW550IRuiBj6tL9rrOkDaHHJfR8lcRZYgYIHSBlghehS7f8rbYURPBtgQld/np0TKlT51XxnMsNXRB6VEIvXC7wr+f5NgBCBzBJ8G9L97zV4wi+VMwm50foDjpj6gg9LKGLsKXr/M2ywOlGB4QOkC3BvyxCF8HfrEpd9N2e5DUutd9d9OIi9KCEPlBO4O+QwAEQOkBNvvbWyxWx31y+7XY1g142dDnSYEMXhO6FAfu5BsryHnj23q/3c5QCIHQAb5L/6UXJX1e+7aklt+LouFKnz808po7QG9GvSun7gCPvdcgbAKEDhC/5nCrNopclcwtLore6i6NjbcWCbOhyAaHXplCWtvycKks8b8s7z1EFgNABkiX7N1/smfz1aTUlyd9almBPRoReSdZvTv0ziRsAoQMYxVdf+ZZ027eVfyoT8a5Tl9bNT/37pAh9oJyuhbz9/Aeq/r5gC5vZ5QAIHQDqXADk1NQiOZeEHlTir5Wc889+iW5wgDTw/wUYAKIALMme2mylAAAAAElFTkSuQmCC',
                170,
                5,
                25,
                25
            )

        pdf.setFontStyle("bold");
        pdf.text(105, 10, "Checklist do Serviço", { align: "center" });

        pdf.setFontStyle("bold");
        pdf.text(20, 30, "Data da realização do serviço:");
        pdf.setFontStyle("regular");
        pdf.text(82, 30, checklist.dataRealizacao);

        pdf.setFontStyle("bold");
        pdf.text(20, 35, "Número da OS:");
        pdf.setFontStyle("regular");
        pdf.text(49, 35, checklist.protocolo);

        pdf.setFontStyle("bold");
        pdf.text(20, 40, "Técnico:");
        pdf.setFontStyle("regular");
        pdf.text(37, 40, checklist.nomeTecnico);

        pdf.setFontStyle("bold");
        pdf.text(20, 45, "Vistoria Inicial:");
        pdf.setFontStyle("regular");
        pdf.text(50, 45, checklist.avaliacaoRisco);

        pdf.setFontStyle("bold");
        pdf.text(20, 55, "Informação Adicional:");
        pdf.setFontStyle("regular");
        pdf.text(20, 60, checklist.infoAdicional);

        pdf.setFontStyle("bold");
        pdf.text(105, 80, "Identificação do Cliente ou Representante", { align: "center" });

        pdf.setFontStyle("bold");
        pdf.text(20, 90, "Nome Completo:");
        pdf.setFontStyle("regular");
        pdf.text(52, 90, checklist.nomeCliente);

        pdf.setFontStyle("bold");
        pdf.text(20, 95, "Documento:");
        pdf.setFontStyle("regular");
        pdf.text(43, 95, checklist.documentoCliente);

        pdf.setFontStyle("bold");
        pdf.text(20, 100, "Confirmo que o serviço foi devidamente prestado, ficando dentro das expectativas contratadas.");
        pdf.text(20, 105, "Estou ciente que o prazo limite para qualquer reclamação referente ao serviço é de 7 dias corridos");
        pdf.text(20, 110, "a partir da data de realização.");

        pdf.text(20, 120, "Recibo - Serviço Pago:");
        pdf.setFontStyle("regular");
        pdf.text(62, 120, checklist.isPago ? 'Sim' : 'Não');

        pdf.setFontStyle("bold");
        pdf.text(20, 130, "Forma de Pagamento:");
        pdf.setFontStyle("regular");
        pdf.text(62, 130, 'Dinheiro');

        pdf.setFontStyle("bold");
        pdf.text(20, 140, "Justifique:");
        pdf.setFontStyle("regular");
        pdf.text(40, 140, checklist.justificativa);

        pdf.setFontStyle("bold");
        pdf.text(20, 145, "A Dr. Lava Tudo Prestação de Serviços LTDA, afirma que recebeu o valor de: ");
        pdf.setFontStyle("regular");
        pdf.text(162, 145, 'R$ ' + checklist.valorPago.toString());

        pdf.addImage
            (
                checklist.assinatura,
                'PNG',
                50,
                155,
                100,
                50
            );

        //Linhas da Assinatura
        pdf.line(50, 155, 150, 155)
        pdf.line(50, 205, 150, 205)
        pdf.line(50, 155, 50, 205)
        pdf.line(150, 155, 150, 205)

        pdf.setFontStyle("italic");
        pdf.text(70, 209, 'Assinatura do cliente/representante')

        pdf.addPage();

        pdf.text(20, 10, 'Informações Importantes:')

        pdf.text(20, 20, '1 - O prazo para reclamações é de até 7 dias corridos após a realização do serviço,')
        pdf.text(20, 25, 'para caso de limpeza e hidratação de couro.')

        pdf.text(20, 35, '2 - O prazo de secagem dos estofados pode variar de acordo com o clima e a sujidade do estofado')
        pdf.text(20, 40, 'obrigando o técnico a usar mais água para remover sujeiras mais profundas.')

        pdf.text(20, 50, '3 - Não usar nem colocar nenhum objeto sobre o sofá até a finalização completa da secagem.')

        pdf.text(20, 60, '4 - Não garantimos a remoção de manchas.')

        pdf.text(20, 70, '5 - Não é recomendado colocar estofado no sol. Deixe-o secar naturalmente e indicamos aumentar a')
        pdf.text(20, 75, 'ventilação no local.')

        pdf.text(20, 85, '6 - O estado final do estofado do tecido, condições de conservação, tempo de uso e outras variáveis');
        pdf.text(20, 90, 'externas.');

        //pdf.text(20,100,'');

        pdf.text(20, 100, '7 - Recomenda-se aspiração semanal para manter o estofado limpo. A limpeza técnica deve ser');
        pdf.text(20, 105, 'feita a cada 6 meses.');

        pdf.text(20, 115, '8 - A hidratação de couro deve ser feita a cada 6 meses para evitar trincas e rasgos.');

        pdf.text(20, 125, '9 - A impermeabilização tem garantia de 1 ano, desde que observada as seguintes condições:');

        pdf.text(25, 130, 'a. Não tem garantia contra urina de animais ou pessoas.');
        pdf.text(25, 135, 'b. Deve-se, preferencialmente ser feita em sofá novos ou que foram lavados recentemente.');
        pdf.text(25, 140, 'c. No caso de queda de líquido, usasr um papel toalha para absorvê-lo, NUNCA PRESSIONAR');
        pdf.text(29, 145, 'O LÍQUIDO CONTRA O ESTOFADO!');

        pdf.text(25, 150, 'd. A impermeabilização não resiste a líquidos muito quentes ou ácidos.');

        pdf.text(25, 155, 'e. Não usar nem colocar nenhum objeto sobre o sofá até a finalização completa da secagem.');

        pdf.text(25, 160, 'f. A garantia não abrange o prazo para reclamação de danos causados, abrange apenas a');
        pdf.text(29, 165, 'ineficácia da impemeabilização.');

        pdf.text(20, 175, '10 - A Dr. Lava Tudo não se responsabiliza por manchas ou sujidades surgidas após o');
        pdf.text(20, 180, 'Técnico deixar a residência do cliente.');

        pdf.text(20, 190, '11 - Os retornos apenas serão realizados se constados vícios na prestação de serviços.');

        pdf.text(20, 200, '12 - Não garantimos retorno do técnico, no caso de eventuais acidentes nos itens que foram');
        pdf.text(20, 205, 'limpos, envolvendo animais de estimação. Caso o cliente exija esse retorno, será cobrada uma');
        pdf.text(20, 210, 'taxa mínima, no valor de R$50,00, para que seja feita a limpeza apenas no local que foi sujo');
        pdf.text(20, 215, 'pelo animal. Não será feita a limpeza completa do item, nesses casos.');

        pdf.addPage();

        pdf.setFontStyle("bold");
        pdf.text(100, 20, 'Na hipótese de danos causados, a Dr. Lava Tudo seguirá os passos listados abaixo:', { align: 'center' });

        pdf.setFontStyle('italic');
        pdf.text(20, 30, 'Autorizo a Dr. Lava Tudo a trabalhar com o prazo de 60 dias para solucionar o vício na');
        pdf.text(20, 35, 'hipótese de danos causados.');

        pdf.text(20, 45, '1º - Dentro do prazo acima ou no prazo estipulado pelo CDC, a Dr. Lava Tudo poderá');
        pdf.text(20, 50, 'realizar qualquer procedimento, por quantas vezes achar necessário, para tentar sanar o vício');

        pdf.text(20, 65, '2º - Constando que os procedimentos não solucionaram o vício, a Dr. Lava Tudo enviará um');
        pdf.text(20, 70, 'profissional de confiança para analisar a possibilidade de reforma. Sendo necessária a');
        pdf.text(20, 75, 'reforma, o local da compra do material e a escolha do reformador será feita exclusivamente');
        pdf.text(20, 80, 'pela Dr. Lava Tudo, resguardando para tanto, todos os direitos previstos no código');
        pdf.text(20, 85, 'de defesa do consumidor.');

        pdf.text(20, 100, '3º - As opções contidas no artigo 18 do CDC (a substituição do bem danificado, a restituição');
        pdf.text(20, 105, 'da quantia paga ou o abatimento proporcional no preço) ocorrerá apenas, se após o prazo');
        pdf.text(20, 110, 'previsto para solução, não conseguirmos retornar o objeto ao estado anterior.');

        pdf.text(20, 125, '4º - A recusa da realização dos procedimentos ou da reforma do bem danificado (dentro do prazo),');
        pdf.text(20, 130, 'exime a responsabilidade da Dr. Lava Tudo, uma vez que retira nosso direito de sanar o');
        pdf.text(20, 135, 'vício da melhor forma.');

        pdf.text(20, 150, '5º - Valores não relacionados com a prestação de serviços não serão custeados, como por');
        pdf.text(20, 155, 'exemplo, custos com arquiteta, decorador, dentre outros.');

        pdf.setFontSize(16)
        pdf.setFontStyle('bold')
        pdf.text(107, 220, 'No caso de dúvidas, reclamações ou sugestões, favor entrar em contato:', { align: 'center' });
        pdf.text(106, 230, '0800 944 9999 | contato@drlavatudo.com', { align: 'center' });

        //pdf.save('Teste-Checklist.pdf');
        return pdf.output('blob', { filename: fileName })

    }

    public exportPdfFolhaComissao(list, fileName: string): void {

        var doc = new jsPDF();

        for (let index = 0; index < list.length; index++) {
            const element = list[index];

            doc.text(20, 20, element.titulo);
            doc.text(50, 50, '___________________________________');
            doc.text(50, 60, element.nome);
            doc.text(20, 80, element.resumo1);
            doc.text(20, 90, element.resumo2);
            doc.text(20, 100, element.resumo3);
            doc.autoTable(element.header, element.dados, { startY: 110 });
            doc.addPage();
        }
        doc.save(fileName + '.pdf')
    }
}