import { Injectable } from "@angular/core";

@Injectable()
export class ValidateForm {

    constructor() {

    }

    public validarFormVazio(form): boolean {

        let preenchido = false;

        Object.keys(form.controls).forEach(element => {

            if (form.controls[element].value != "")
                preenchido = true;
        });

        return preenchido;
    }

    public static obterItemUnicoDropDown(elemento) {

        if (elemento != undefined && elemento[0] != undefined && elemento != "") {

            return elemento[0].split(':')[1].trim();
        }
        else
            return "";

    }

    public static obterMultiplosItensDropDown(elemento) {

        if (elemento != undefined && elemento[0] != undefined && elemento != "") {

            let listaAux: Array<any> = [];

            elemento.forEach(item => {

                let itemId = item.split(':')[1];

                itemId = itemId.trim();

                listaAux.push(itemId);

            });

            return listaAux;
        }
        else
            return "";

    }

    public static obterMultiplosItensDropDownToString(elemento) {

        let listaAux: Array<any> = [];

        listaAux.push(this.obterMultiplosItensDropDown(elemento));

        let listString = "";

        listaAux.forEach(element => {

            listString += element + ", ";

        });

        listString = listString.trim();

        listString = listString.substring(0, listString.length - 1);

        return listString;

    }
}