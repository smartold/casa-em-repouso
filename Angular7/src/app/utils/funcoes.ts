import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable()
export class Funcoes {
    public copyText(str) {
        const el = document.createElement('textarea');
        el.value = str;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    }

    public tokenGenerate() {
        return Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2);
    }
}