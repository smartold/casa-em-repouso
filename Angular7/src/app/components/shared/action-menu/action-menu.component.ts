import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';

@Component({
  selector: 'app-action-menu',
  templateUrl: './action-menu.component.html'
})
export class ActionMenuComponent implements OnInit {
  @Input() titulo: string;
  @Input() descricao: string;

  constructor() { }

  ngOnInit() {
  }

}
