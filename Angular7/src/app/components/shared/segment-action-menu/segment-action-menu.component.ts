import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-segment-action-menu',
  templateUrl: './segment-action-menu.component.html',
  styleUrls: ['./segment-action-menu.component.css']
})
export class SegmentActionMenuComponent implements OnInit {

  @Input() titulo;
  @Input() descricao;

  constructor() { }

  ngOnInit() {
  }

}
