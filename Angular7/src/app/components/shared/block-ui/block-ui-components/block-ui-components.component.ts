import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { BlockUiBallTriangleComponent } from '../block-ui-ball-triangle/block-ui-ball-triangle.component';
import { BlockUiGridComponent } from '../block-ui-grid/block-ui-grid.component';
import { BlockUiSpinnerComponent } from '../block-ui-spinner/block-ui-spinner.component';

@Component({
  selector: 'block-ui-components',
  templateUrl: 'block-ui-components.component.html'
})

export class BlockUiComponentsComponent {

  blockTemplateBallTriangle: BlockUiBallTriangleComponent = BlockUiBallTriangleComponent;
  blockTemplateGrid: BlockUiGridComponent = BlockUiGridComponent;
  blockTemplateSpinner: BlockUiSpinnerComponent = BlockUiSpinnerComponent;

  @BlockUI() static staticblockUI: NgBlockUI;
  //@BlockUI() nsblockUI: NgBlockUI;

  public static bloquearBlockUi = false;
  public static forcarBlockUi = false;

  constructor() {
  }

  public static start() {
    if (this.bloquearBlockUi != true || this.forcarBlockUi == true) {
      this.staticblockUI.start();
    }
  }

  public static stop() {
    if (this.forcarBlockUi != true) {
      if (this.bloquearBlockUi != true) {
        this.staticblockUI.stop();
      }
      else {
        this.bloquearBlockUi = false;
      }
    }
  }

  public static forceStop() {
    this.forcarBlockUi = false;
    this.stop();
  }

  // public nsstart() {
  //   this.nsblockUI.start("teste");
  // }

  // public nsstop() {
  //   this.nsblockUI.stop();
  // }

}