import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'block-ui-grid',
  styles: [`
  :host {
    text-align: center;
    color: #1976D2;
  }
`],
  templateUrl: 'block-ui-grid.component.html'

})
export class BlockUiGridComponent {

  constructor() {

  }

}