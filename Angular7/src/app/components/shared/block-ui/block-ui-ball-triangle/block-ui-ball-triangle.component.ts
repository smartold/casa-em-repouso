import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'block-ui-ball-triangle',
  styles: [`
  :host {
    text-align: center;
    color: #1976D2;
  }
`],
  templateUrl: 'block-ui-ball-triangle.component.html'

})
export class BlockUiBallTriangleComponent {

  constructor() {

  }

}