import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-dados-responsavel',
  templateUrl: './dados-responsavel.component.html',
  styleUrls: ['./dados-responsavel.component.css']
})
export class DadosResponsavelComponent implements OnInit {

  @Input() dadosresponsavel: any = [];

  constructor() { }

  ngOnInit() {
  }

}
