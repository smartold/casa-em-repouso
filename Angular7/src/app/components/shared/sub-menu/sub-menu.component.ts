import { Component, OnInit } from '@angular/core';
import { BlockUiComponentsComponent } from '../block-ui/block-ui-components/block-ui-components.component';
import { Menu } from '../../../security/menu';
import { ErroService } from '../../../services/erro.service';
import { SecurityService } from '../../../services/security.service';
import { UsuarioService } from '../../../services/usuario.service';

@Component({
  selector: 'app-sub-menu',
  templateUrl: './sub-menu.component.html',
  providers: [SecurityService]
})
export class SubMenuComponent implements OnInit {
  public menu: Menu;

  constructor(private usuarioService: UsuarioService,
    private securityService: SecurityService,
    private erroService: ErroService
  ) {
    this.menu = new Menu();
  }

  ngOnInit() {
    BlockUiComponentsComponent.bloquearBlockUi = true;
    //   this.usuarioService.obterNivelAcessoUsuario().subscribe(

    //     result => {

    //       let claim = result;
    //       this.menu = this.securityService.definirMenuUsuario(claim);
    //     },
    //     error => {

    //       this.erroService.tratarErro(error);
    //     }
    //   )

  }

}
