import { Component, OnInit } from '@angular/core';
import { Menu } from '../../../security/menu';
import { SearchService } from '../../../services/search.service';
import { SecurityService } from '../../../services/security.service';
import { BlockUiComponentsComponent } from '../block-ui/block-ui-components/block-ui-components.component';


declare var $: any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  providers: [SearchService, SecurityService]
})
export class NavbarComponent implements OnInit {
  [x: string]: any;

  public exibicaoMenu = false;
  public menu: Menu;

  public frase: string;
  public resultPesquisa: any;
  public resultAlerta: any;
  public resultAlertaNaoLidoLenght: number;
  public exibirAlerta: boolean = false;
  public resultPesquisaLenght: number;
  public static contAuxNotificacaoComputador: number = 0;
  private static liberarAtualizacao: boolean = true;
  public intervalo: any;

  public usuarioId: number;


  private _hubConnection: any;



  //public tipoBlockUI: number;

  constructor(
    private searchService: SearchService,
    private securityService: SecurityService) {
    this.arquivo = {};
    this.menu = new Menu();
  }

  ngOnInit() {
    this.inicializarComponentes();
  }

  public alternarSelecaoAlerta(id) {

    $('.ui.accordion').accordion('refresh');

    if (id != undefined && !this.listaAlertas.contains(id))
      this.listaAlertas.push(id);

  }



  private inicializarComponentes() {

    $('#menu').popup({
      on: 'click',
      popup: $('#popup'),
      hoverable: true,
      position: 'bottom left',
      inline: 'true'
    });

    $('#alerta').popup({

      on: 'click',
      closable: 'true',
      popup: $('#popup-alerta'),
      hoverable: false,
      position: 'bottom right',
      inline: 'false'
      // 'onShow': (set => this.popAlertaAberto()),
      // 'onHidden': (set => this.liberarAtualizacao())
    });



  }


  public bloquearAtualizacao() {
    NavbarComponent.liberarAtualizacao = false;
  }

  public liberarAtualizacao() {
    NavbarComponent.liberarAtualizacao = true;
  }

  public popAlertaAberto(item) {
    if (item == null) {
      return;
    }

    var listaTemp = [item.id];

    if (item.lidaEm == null) {
      this.resultAlerta.filter(x => x.id == item.id)[0].lidaEm = new Date();
      BlockUiComponentsComponent.bloquearBlockUi = true;
      this.alertaUsuarioService.marcarComoLido(listaTemp).subscribe(
        result => {
          // this.verificarNotificacao();
          // this.bloquearAtualizacao();
          this.validarTitulo();
        }, error => {
          this.erroService.tratarErro(error);
        });
    }
  }

  public validarTitulo() {
    if (this.resultAlerta == null) {
      return;
    }
    this.resultAlertaNaoLidoLenght = this.resultAlerta.filter(x => x.lidaEm == null).length;
    if (this.resultAlertaNaoLidoLenght > 0) {
      this.appComponent.titulo = "Você possui " + this.resultAlertaNaoLidoLenght + (this.resultAlertaNaoLidoLenght > 1 ? " notificações" : " notificação");
    } else {
      this.appComponent.setarTituloPadrao();
    }
  }

  public exibirMenu() {
    this.exibicaoMenu = !this.exibicaoMenu;
  }
}

