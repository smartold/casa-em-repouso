import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { DateTransform } from 'src/app/utils/datetime/date-transform';
import { PacienteService } from 'src/app/services/paciente.service';
import { ModalTemplate, SuiModalService, TemplateModalConfig } from 'ng2-semantic-ui';
import { IContext } from 'src/app/app.module';

declare var $: any;

@Component({
  selector: 'app-modal-remedio',
  templateUrl: './modal-remedio.component.html',
  styleUrls: ['./modal-remedio.component.css'],
  providers: [DateTransform, PacienteService],
})
export class ModalRemedioComponent implements OnInit {

  @Input() abrirModalRemedio;
  @Input() pacienteGuid;
  @Output() fecharModalRemedio = new EventEmitter<any>();

  public pacienteSelecionado: any = null;

  @ViewChild('modal')
  public modal: ModalTemplate<IContext, string, string>

  constructor(
    private pacienteService: PacienteService,
    private dateTransform: DateTransform,
    public modalService: SuiModalService
  ) { }

  ngOnInit() {
  }

  ngOnChanges() {

    if (this.abrirModalRemedio) {
      this.obterPaciente(this.pacienteGuid);
    }
  }

  public mostrarModalRemedio() {

    const config = new TemplateModalConfig<IContext, string, string>(this.modal);

    config.closeResult = "closed!";
    config.context = { data: null };


    this.modalService
      .open(config)
      .onApprove(result => {
        this.fecharModalRemedio.emit();
      })
      .onDeny(result => {
        this.fecharModalRemedio.emit();
      });
  }

  public obterPaciente(guid) {
    if (guid != null) {
      this.pacienteService.obterPacientePorGuid(guid).subscribe(
        result => {
          this.pacienteSelecionado = result;
          console.log("informacao modal", result)
          this.mostrarModalRemedio();
        });
    }
  }

}
