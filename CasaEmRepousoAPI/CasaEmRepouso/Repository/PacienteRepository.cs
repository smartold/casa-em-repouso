﻿using CasaEmRepouso.Models;
using CasaEmRepousoAPI.Interface;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Text;
using CasaEmRepouso.Filtros;


namespace CasaEmRepousoAPI.Repository
{
    public class PacienteRepository : IPacienteRepository
    {
        private readonly IConfiguration _config;

        public PacienteRepository(IConfiguration config)
        {
            _config = config;
        }

        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_config.GetConnectionString("IdentityConnection"));
            }
        }



        public async Task<List<Paciente>> ObterListagemPacientes()
        {
            using (IDbConnection conn = Connection)
            {
                string sQuery = "SELECT * FROM Pacientes p ORDER BY p.nome";
                conn.Open();
                var listaPaciente = await conn.QueryAsync<Paciente>(sQuery) as List<Paciente>;
                return listaPaciente;
            }


        }


        public async Task<Paciente> ObterPacientePorGuid(string guid)
        {
            using (IDbConnection conn = Connection)
            {
                StringBuilder query = new StringBuilder();
                query.AppendLine("SELECT * FROM Pacientes p ");
                query.AppendLine($"WHERE p.Guid = '{guid}'");

                var consulta = query.ToString();
                return await conn.QueryFirstOrDefaultAsync<Paciente>(consulta) as Paciente;
            }
        }
        public async Task Excluir(string guid)
        {
            using (IDbConnection conn = Connection)
            {
                string query = $"DELETE FROM Pacientes WHERE Guid = '{guid}'";

                await conn.QueryAsync(query);

            }

        }

        public async Task<List<Paciente>> IdentificarPaciente(PacienteFilter filter)
        {
            var listaPacientes = new List<Paciente>();
            var query = new StringBuilder();

            try
            {
                query.Append("SELECT TOP 100 ID, NOME, GUID, CPF FROM Pacientes ");

                listaPacientes = await Filtrar(query, filter);
            }
            catch
            {

                throw;
            }

            return listaPacientes;
        }

        public async Task<List<Paciente>> Filtrar(StringBuilder query, PacienteFilter filtro)
        {
            using (IDbConnection conn = Connection)
            {
                try
                {
                    var parametros = new List<string>();

                    if (filtro != null)
                    {
                        filtro.Validar();

                        var queryWhere = new StringBuilder();

                        query.AppendLine("WHERE ");

                        if (filtro.nome != null)
                        {
                            var arrayNome = filtro.nome.Split(" ");

                            var queryNome = "";
                            foreach (var item in arrayNome)
                            {
                                if (queryNome != "")
                                {
                                    queryNome += " AND ";
                                }
                                queryNome += $"NOME LIKE '%{item}%'";
                            }

                            queryWhere.AppendLine($"NOME IS NULL OR ({queryNome}) ");

                        }

                        if (filtro.cpf != null)
                            queryWhere.AppendLine($"CPF LIKE '%{filtro.cpf}%' OR ");

                        query.AppendLine($"({queryWhere.ToString()})");
                    }

                    return await conn.QueryAsync<Paciente>(query.ToString()) as List<Paciente>;
                }
                catch
                {
                    throw;
                }
            }

        }

        public bool AtualizarPaciente(Paciente paciente)
        {
            using (IDbConnection conn = Connection)
            {

                int rowsAffected = conn.Execute($"UPDATE [Pacientes] SET [Nome] = '{paciente.Nome}', [Nacionalidade] = '{paciente.Nacionalidade}', [Cpf] = '{paciente.Cpf}', [Rg] = '{paciente.Rg}', [DataNascimento] = '{paciente.DataNascimento}', [Sexo] = '{paciente.Sexo}', [TipoSanguineo] = '{paciente.TipoSanguineo}', [DataDeEntrada] = '{paciente.DataDeEntrada}', [Observacao] = '{paciente.Observacao}' WHERE Id ='{paciente.Id}'");

                if (rowsAffected > 0)
                {
                    return true;
                }
                return false;
            }
        }


    }
}
