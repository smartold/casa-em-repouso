using CasaEmRepouso.Models;
using CasaEmRepousoAPI.Interface;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Text;
using CasaEmRepouso.Filtros;


namespace CasaEmRepousoAPI.Repository
{
    public class RemedioRepository : IRemedioRepository
    {
        private readonly IConfiguration _config;

        public RemedioRepository(IConfiguration config)
        {
            _config = config;
        }

        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_config.GetConnectionString("IdentityConnection"));
            }
        }

        public async Task<List<QuantidadexDia>> ObterListaQuantidade()
        {

            using (IDbConnection conn = Connection)
            {

                string sQuery = "SELECT * FROM QuantidadexDia qnt";
                conn.Open();
                var listaQuantidade = await conn.QueryAsync<QuantidadexDia>(sQuery) as List<QuantidadexDia>;
                return listaQuantidade;

            }
        }

        public async Task<List<Dosagem>> ObterListaDosagem()
        {

            using (IDbConnection conn = Connection)
            {

                string sQuery = "SELECT * FROM Dosagem dos";
                conn.Open();
                var listaDosagem = await conn.QueryAsync<Dosagem>(sQuery) as List<Dosagem>;
                return listaDosagem;

            }
        }

        public async Task<List<Remedio>> ObterListaRemedio(int id)
        {
            using (IDbConnection conn = Connection)
            {

                var query = new StringBuilder();
                query.AppendLine("SELECT * from Remedios re");
                query.AppendLine("LEFT JOIN Dosagem do on do.Id = re.DosagemId");
                query.AppendLine("LEFT JOIN QuantidadexDia qnt on qnt.Id = re.QuantidadeId");
                query.AppendLine($"WHERE IdosoId = {id}");

                var consulta = query.ToString();

                var listaRemedio = await conn.QueryAsync<Remedio, Dosagem, QuantidadexDia, Remedio>(consulta,
                (remedio, dosagem, quantidade) =>
                    {
                        remedio.Dosagem = dosagem;
                        remedio.Quantidade = quantidade;
                        return remedio;

                    }) as List<Remedio>;

                return listaRemedio;
            }
        }

        public Task Excluir(string guid)
        {
            throw new NotImplementedException();
        }
    }
}
