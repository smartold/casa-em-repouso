using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Security.Claims;
using System.Text;
using Doctor.Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

public class RepositoryBase
{
    protected SqlConnection DapperConnection = null;
    protected SqlConnection DapperReportConnection = null;
    private readonly IHttpContextAccessor _httpContextAccessor;
    protected DbContext Context = null;

    // public RepositoryBase(IDapperDbConnection dapperDbConnection)
    // {
    //     DapperConnection = ((DapperDbConnection)dapperDbConnection).GetConnection();
    //     DapperReportConnection = ((DapperDbConnection)dapperDbConnection).GetReportConnection();
    //     _httpContextAccessor = new HttpContextAccessor();
    // }

    // public RepositoryBase(DoctorDbContext context, IDapperDbConnection dapperDbConnection)
    // {
    //     DapperConnection = ((DapperDbConnection)dapperDbConnection).GetConnection();
    //     DapperReportConnection = ((DapperDbConnection)dapperDbConnection).GetReportConnection();
    //     Context = context;
    // }
    // protected T QueryFirstOrDefault<T>(string sql, object parameters = null)
    // {
    //     using (var connection = CreateConnection())
    //     {
    //         return connection.QueryFirstOrDefault<T>(sql, parameters);
    //     }
    // }

    // protected List<T> Query<T>(string sql, object parameters = null)
    // {
    //     using (var connection = CreateConnection())
    //     {
    //         return connection.Query<T>(sql, parameters).ToList();
    //     }
    // }

    // protected int Execute(string sql, object parameters = null)
    // {
    //     using (var connection = CreateConnection())
    //     {
    //         return connection.Execute(sql, parameters);
    //     }
    // }

    // // Other Helpers...

    // private IDbConnection CreateConnection()
    // {
    //     var connection = new SqlConnection();
    //     connection.ConnectionString()
    //     // Properly initialize your connection here.
    //     return connection;
    // }
}