﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CasaEmRepousoAPI.Migrations
{
    public partial class EstoqueTirandoColunaRemedioID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dosagem_Remedios_RemedioId",
                table: "Dosagem");

            migrationBuilder.DropForeignKey(
                name: "FK_QuantidadexDia_Remedios_RemedioId",
                table: "QuantidadexDia");

            migrationBuilder.AlterColumn<int>(
                name: "RemedioId",
                table: "QuantidadexDia",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "RemedioId",
                table: "Dosagem",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Dosagem_Remedios_RemedioId",
                table: "Dosagem",
                column: "RemedioId",
                principalTable: "Remedios",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_QuantidadexDia_Remedios_RemedioId",
                table: "QuantidadexDia",
                column: "RemedioId",
                principalTable: "Remedios",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dosagem_Remedios_RemedioId",
                table: "Dosagem");

            migrationBuilder.DropForeignKey(
                name: "FK_QuantidadexDia_Remedios_RemedioId",
                table: "QuantidadexDia");

            migrationBuilder.AlterColumn<int>(
                name: "RemedioId",
                table: "QuantidadexDia",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RemedioId",
                table: "Dosagem",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Dosagem_Remedios_RemedioId",
                table: "Dosagem",
                column: "RemedioId",
                principalTable: "Remedios",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_QuantidadexDia_Remedios_RemedioId",
                table: "QuantidadexDia",
                column: "RemedioId",
                principalTable: "Remedios",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
