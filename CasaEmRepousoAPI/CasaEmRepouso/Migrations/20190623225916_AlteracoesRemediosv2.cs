﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CasaEmRepousoAPI.Migrations
{
    public partial class AlteracoesRemediosv2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Dosagem",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Guid = table.Column<Guid>(nullable: false),
                    Nome = table.Column<string>(type: "nvarchar(150)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dosagem", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "QuantidadexDia",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Guid = table.Column<Guid>(nullable: false),
                    Nome = table.Column<string>(type: "nvarchar(150)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuantidadexDia", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Remedios_DosagemId",
                table: "Remedios",
                column: "DosagemId");

            migrationBuilder.CreateIndex(
                name: "IX_Remedios_QuantidadeId",
                table: "Remedios",
                column: "QuantidadeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Remedios_Dosagem_DosagemId",
                table: "Remedios",
                column: "DosagemId",
                principalTable: "Dosagem",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Remedios_QuantidadexDia_QuantidadeId",
                table: "Remedios",
                column: "QuantidadeId",
                principalTable: "QuantidadexDia",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Remedios_Dosagem_DosagemId",
                table: "Remedios");

            migrationBuilder.DropForeignKey(
                name: "FK_Remedios_QuantidadexDia_QuantidadeId",
                table: "Remedios");

            migrationBuilder.DropTable(
                name: "Dosagem");

            migrationBuilder.DropTable(
                name: "QuantidadexDia");

            migrationBuilder.DropIndex(
                name: "IX_Remedios_DosagemId",
                table: "Remedios");

            migrationBuilder.DropIndex(
                name: "IX_Remedios_QuantidadeId",
                table: "Remedios");
        }
    }
}
