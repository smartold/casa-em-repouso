﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CasaEmRepousoAPI.Migrations
{
    public partial class ReconfigTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Remedios_Pacientes_PacienteId",
                table: "Remedios");

            migrationBuilder.DropForeignKey(
                name: "FK_Remedios_QuantidadexDia_QuantidadexDiaId",
                table: "Remedios");

            migrationBuilder.DropIndex(
                name: "IX_Remedios_PacienteId",
                table: "Remedios");

            migrationBuilder.DropIndex(
                name: "IX_Remedios_QuantidadexDiaId",
                table: "Remedios");

            migrationBuilder.DropColumn(
                name: "PacienteId",
                table: "Remedios");

            migrationBuilder.DropColumn(
                name: "QuantidadexDiaId",
                table: "Remedios");

            migrationBuilder.CreateIndex(
                name: "IX_Remedios_IdosoId",
                table: "Remedios",
                column: "IdosoId");

            migrationBuilder.CreateIndex(
                name: "IX_Remedios_QuantidadeId",
                table: "Remedios",
                column: "QuantidadeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Remedios_Pacientes_IdosoId",
                table: "Remedios",
                column: "IdosoId",
                principalTable: "Pacientes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Remedios_QuantidadexDia_QuantidadeId",
                table: "Remedios",
                column: "QuantidadeId",
                principalTable: "QuantidadexDia",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Remedios_Pacientes_IdosoId",
                table: "Remedios");

            migrationBuilder.DropForeignKey(
                name: "FK_Remedios_QuantidadexDia_QuantidadeId",
                table: "Remedios");

            migrationBuilder.DropIndex(
                name: "IX_Remedios_IdosoId",
                table: "Remedios");

            migrationBuilder.DropIndex(
                name: "IX_Remedios_QuantidadeId",
                table: "Remedios");

            migrationBuilder.AddColumn<int>(
                name: "PacienteId",
                table: "Remedios",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "QuantidadexDiaId",
                table: "Remedios",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Remedios_PacienteId",
                table: "Remedios",
                column: "PacienteId");

            migrationBuilder.CreateIndex(
                name: "IX_Remedios_QuantidadexDiaId",
                table: "Remedios",
                column: "QuantidadexDiaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Remedios_Pacientes_PacienteId",
                table: "Remedios",
                column: "PacienteId",
                principalTable: "Pacientes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Remedios_QuantidadexDia_QuantidadexDiaId",
                table: "Remedios",
                column: "QuantidadexDiaId",
                principalTable: "QuantidadexDia",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
