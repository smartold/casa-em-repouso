﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CasaEmRepousoAPI.Migrations
{
    public partial class AlteracaofinalRemedio : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Nome",
                table: "QuantidadexDia",
                newName: "Quantidade");

            migrationBuilder.RenameColumn(
                name: "Nome",
                table: "Dosagem",
                newName: "DosagemMg");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Quantidade",
                table: "QuantidadexDia",
                newName: "Nome");

            migrationBuilder.RenameColumn(
                name: "DosagemMg",
                table: "Dosagem",
                newName: "Nome");
        }
    }
}
