﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CasaEmRepousoAPI.Migrations
{
    public partial class AlteracoesRemedios : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Remedios_Pacientes_IdosoId",
                table: "Remedios");

            migrationBuilder.DropIndex(
                name: "IX_Remedios_IdosoId",
                table: "Remedios");

            migrationBuilder.AddColumn<int>(
                name: "PacienteId",
                table: "Remedios",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Remedios_PacienteId",
                table: "Remedios",
                column: "PacienteId");

            migrationBuilder.AddForeignKey(
                name: "FK_Remedios_Pacientes_PacienteId",
                table: "Remedios",
                column: "PacienteId",
                principalTable: "Pacientes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Remedios_Pacientes_PacienteId",
                table: "Remedios");

            migrationBuilder.DropIndex(
                name: "IX_Remedios_PacienteId",
                table: "Remedios");

            migrationBuilder.DropColumn(
                name: "PacienteId",
                table: "Remedios");

            migrationBuilder.CreateIndex(
                name: "IX_Remedios_IdosoId",
                table: "Remedios",
                column: "IdosoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Remedios_Pacientes_IdosoId",
                table: "Remedios",
                column: "IdosoId",
                principalTable: "Pacientes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
