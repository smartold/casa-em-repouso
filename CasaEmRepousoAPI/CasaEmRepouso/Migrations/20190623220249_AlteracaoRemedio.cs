﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CasaEmRepousoAPI.Migrations
{
    public partial class AlteracaoRemedio : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dosagem_Remedios_RemedioId",
                table: "Dosagem");

            migrationBuilder.DropForeignKey(
                name: "FK_QuantidadexDia_Remedios_RemedioId",
                table: "QuantidadexDia");

            migrationBuilder.DropIndex(
                name: "IX_QuantidadexDia_RemedioId",
                table: "QuantidadexDia");

            migrationBuilder.DropIndex(
                name: "IX_Dosagem_RemedioId",
                table: "Dosagem");

            migrationBuilder.DropColumn(
                name: "RemedioId",
                table: "QuantidadexDia");

            migrationBuilder.DropColumn(
                name: "RemedioId",
                table: "Dosagem");

            migrationBuilder.AddColumn<int>(
                name: "DosagemId",
                table: "Remedios",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "IdosoId",
                table: "Remedios",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PacienteId",
                table: "Remedios",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "QuantidadeId",
                table: "Remedios",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "QuantidadexDiaId",
                table: "Remedios",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Remedios_DosagemId",
                table: "Remedios",
                column: "DosagemId");

            migrationBuilder.CreateIndex(
                name: "IX_Remedios_PacienteId",
                table: "Remedios",
                column: "PacienteId");

            migrationBuilder.CreateIndex(
                name: "IX_Remedios_QuantidadexDiaId",
                table: "Remedios",
                column: "QuantidadexDiaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Remedios_Dosagem_DosagemId",
                table: "Remedios",
                column: "DosagemId",
                principalTable: "Dosagem",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Remedios_Pacientes_PacienteId",
                table: "Remedios",
                column: "PacienteId",
                principalTable: "Pacientes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Remedios_QuantidadexDia_QuantidadexDiaId",
                table: "Remedios",
                column: "QuantidadexDiaId",
                principalTable: "QuantidadexDia",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Remedios_Dosagem_DosagemId",
                table: "Remedios");

            migrationBuilder.DropForeignKey(
                name: "FK_Remedios_Pacientes_PacienteId",
                table: "Remedios");

            migrationBuilder.DropForeignKey(
                name: "FK_Remedios_QuantidadexDia_QuantidadexDiaId",
                table: "Remedios");

            migrationBuilder.DropIndex(
                name: "IX_Remedios_DosagemId",
                table: "Remedios");

            migrationBuilder.DropIndex(
                name: "IX_Remedios_PacienteId",
                table: "Remedios");

            migrationBuilder.DropIndex(
                name: "IX_Remedios_QuantidadexDiaId",
                table: "Remedios");

            migrationBuilder.DropColumn(
                name: "DosagemId",
                table: "Remedios");

            migrationBuilder.DropColumn(
                name: "IdosoId",
                table: "Remedios");

            migrationBuilder.DropColumn(
                name: "PacienteId",
                table: "Remedios");

            migrationBuilder.DropColumn(
                name: "QuantidadeId",
                table: "Remedios");

            migrationBuilder.DropColumn(
                name: "QuantidadexDiaId",
                table: "Remedios");

            migrationBuilder.AddColumn<int>(
                name: "RemedioId",
                table: "QuantidadexDia",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RemedioId",
                table: "Dosagem",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_QuantidadexDia_RemedioId",
                table: "QuantidadexDia",
                column: "RemedioId");

            migrationBuilder.CreateIndex(
                name: "IX_Dosagem_RemedioId",
                table: "Dosagem",
                column: "RemedioId");

            migrationBuilder.AddForeignKey(
                name: "FK_Dosagem_Remedios_RemedioId",
                table: "Dosagem",
                column: "RemedioId",
                principalTable: "Remedios",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_QuantidadexDia_Remedios_RemedioId",
                table: "QuantidadexDia",
                column: "RemedioId",
                principalTable: "Remedios",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
