﻿ALTER TABLE [Dosagem] DROP CONSTRAINT [FK_Dosagem_Remedios_RemedioId];

GO

ALTER TABLE [QuantidadexDia] DROP CONSTRAINT [FK_QuantidadexDia_Remedios_RemedioId];

GO

DROP INDEX [IX_QuantidadexDia_RemedioId] ON [QuantidadexDia];

GO

DROP INDEX [IX_Dosagem_RemedioId] ON [Dosagem];

GO

DECLARE @var7 sysname;
SELECT @var7 = [d].[name]
FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[QuantidadexDia]') AND [c].[name] = N'RemedioId');
IF @var7 IS NOT NULL EXEC(N'ALTER TABLE [QuantidadexDia] DROP CONSTRAINT [' + @var7 + '];');
ALTER TABLE [QuantidadexDia] DROP COLUMN [RemedioId];

GO

DECLARE @var8 sysname;
SELECT @var8 = [d].[name]
FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Dosagem]') AND [c].[name] = N'RemedioId');
IF @var8 IS NOT NULL EXEC(N'ALTER TABLE [Dosagem] DROP CONSTRAINT [' + @var8 + '];');
ALTER TABLE [Dosagem] DROP COLUMN [RemedioId];

GO

ALTER TABLE [Remedios] ADD [DosagemId] int NOT NULL DEFAULT 0;

GO

ALTER TABLE [Remedios] ADD [IdosoId] int NOT NULL DEFAULT 0;

GO

ALTER TABLE [Remedios] ADD [PacienteId] int NULL;

GO

ALTER TABLE [Remedios] ADD [QuantidadeId] int NOT NULL DEFAULT 0;

GO

ALTER TABLE [Remedios] ADD [QuantidadexDiaId] int NULL;

GO

CREATE INDEX [IX_Remedios_DosagemId] ON [Remedios] ([DosagemId]);

GO

CREATE INDEX [IX_Remedios_PacienteId] ON [Remedios] ([PacienteId]);

GO

CREATE INDEX [IX_Remedios_QuantidadexDiaId] ON [Remedios] ([QuantidadexDiaId]);

GO

ALTER TABLE [Remedios] ADD CONSTRAINT [FK_Remedios_Dosagem_DosagemId] FOREIGN KEY ([DosagemId]) REFERENCES [Dosagem] ([Id]) ON DELETE CASCADE;

GO

ALTER TABLE [Remedios] ADD CONSTRAINT [FK_Remedios_Pacientes_PacienteId] FOREIGN KEY ([PacienteId]) REFERENCES [Pacientes] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Remedios] ADD CONSTRAINT [FK_Remedios_QuantidadexDia_QuantidadexDiaId] FOREIGN KEY ([QuantidadexDiaId]) REFERENCES [QuantidadexDia] ([Id]) ON DELETE NO ACTION;

GO

INSERT INTO [__EFMigrationsHistory]
    ([MigrationId], [ProductVersion])
VALUES
    (N'20190623220249_AlteracaoRemedio', N'2.2.3-servicing-35854');

GO

