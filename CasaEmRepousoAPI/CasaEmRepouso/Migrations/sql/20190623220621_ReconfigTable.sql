﻿ALTER TABLE [Remedios] DROP CONSTRAINT [FK_Remedios_Pacientes_PacienteId];

GO

ALTER TABLE [Remedios] DROP CONSTRAINT [FK_Remedios_QuantidadexDia_QuantidadexDiaId];

GO

DROP INDEX [IX_Remedios_PacienteId] ON [Remedios];

GO

DROP INDEX [IX_Remedios_QuantidadexDiaId] ON [Remedios];

GO

DECLARE @var9 sysname;
SELECT @var9 = [d].[name]
FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Remedios]') AND [c].[name] = N'PacienteId');
IF @var9 IS NOT NULL EXEC(N'ALTER TABLE [Remedios] DROP CONSTRAINT [' + @var9 + '];');
ALTER TABLE [Remedios] DROP COLUMN [PacienteId];

GO

DECLARE @var10 sysname;
SELECT @var10 = [d].[name]
FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Remedios]') AND [c].[name] = N'QuantidadexDiaId');
IF @var10 IS NOT NULL EXEC(N'ALTER TABLE [Remedios] DROP CONSTRAINT [' + @var10 + '];');
ALTER TABLE [Remedios] DROP COLUMN [QuantidadexDiaId];

GO

CREATE INDEX [IX_Remedios_IdosoId] ON [Remedios] ([IdosoId]);

GO

CREATE INDEX [IX_Remedios_QuantidadeId] ON [Remedios] ([QuantidadeId]);

GO

ALTER TABLE [Remedios] ADD CONSTRAINT [FK_Remedios_Pacientes_IdosoId] FOREIGN KEY ([IdosoId]) REFERENCES [Pacientes] ([Id]) ON DELETE CASCADE;

GO

ALTER TABLE [Remedios] ADD CONSTRAINT [FK_Remedios_QuantidadexDia_QuantidadeId] FOREIGN KEY ([QuantidadeId]) REFERENCES [QuantidadexDia] ([Id]) ON DELETE CASCADE;

GO

INSERT INTO [__EFMigrationsHistory]
    ([MigrationId], [ProductVersion])
VALUES
    (N'20190623220621_ReconfigTable', N'2.2.3-servicing-35854');

GO

