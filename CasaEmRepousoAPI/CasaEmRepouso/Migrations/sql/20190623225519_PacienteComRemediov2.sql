﻿CREATE TABLE [Dosagem]
(
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL,
    [Nome] nvarchar(150) NULL,
    CONSTRAINT [PK_Dosagem] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [QuantidadexDia]
(
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL,
    [Nome] nvarchar(150) NULL,
    CONSTRAINT [PK_QuantidadexDia] PRIMARY KEY ([Id])
);

GO

CREATE INDEX [IX_Remedios_DosagemId] ON [Remedios] ([DosagemId]);

GO

CREATE INDEX [IX_Remedios_QuantidadeId] ON [Remedios] ([QuantidadeId]);

GO

ALTER TABLE [Remedios] ADD CONSTRAINT [FK_Remedios_Dosagem_DosagemId] FOREIGN KEY ([DosagemId]) REFERENCES [Dosagem] ([Id]) ON DELETE CASCADE;

GO

ALTER TABLE [Remedios] ADD CONSTRAINT [FK_Remedios_QuantidadexDia_QuantidadeId] FOREIGN KEY ([QuantidadeId]) REFERENCES [QuantidadexDia] ([Id]) ON DELETE CASCADE;

GO

INSERT INTO [__EFMigrationsHistory]
    ([MigrationId], [ProductVersion])
VALUES
    (N'20190623225916_AlteracoesRemediosv2', N'2.2.3-servicing-35854');

GO

