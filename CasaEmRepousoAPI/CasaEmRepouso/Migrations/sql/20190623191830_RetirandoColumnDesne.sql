﻿

INSERT INTO [__EFMigrationsHistory]
    ([MigrationId], [ProductVersion])
VALUES
    (N'20190623193305_Estoque', N'2.2.3-servicing-35854');

GO

ALTER TABLE [Dosagem] DROP CONSTRAINT [FK_Dosagem_Remedios_RemedioId];

GO

ALTER TABLE [QuantidadexDia] DROP CONSTRAINT [FK_QuantidadexDia_Remedios_RemedioId];

GO

DECLARE @var5 sysname;
SELECT @var5 = [d].[name]
FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[QuantidadexDia]') AND [c].[name] = N'RemedioId');
IF @var5 IS NOT NULL EXEC(N'ALTER TABLE [QuantidadexDia] DROP CONSTRAINT [' + @var5 + '];');
ALTER TABLE [QuantidadexDia] ALTER COLUMN [RemedioId] int NULL;

GO

DECLARE @var6 sysname;
SELECT @var6 = [d].[name]
FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Dosagem]') AND [c].[name] = N'RemedioId');
IF @var6 IS NOT NULL EXEC(N'ALTER TABLE [Dosagem] DROP CONSTRAINT [' + @var6 + '];');
ALTER TABLE [Dosagem] ALTER COLUMN [RemedioId] int NULL;

GO

ALTER TABLE [Dosagem] ADD CONSTRAINT [FK_Dosagem_Remedios_RemedioId] FOREIGN KEY ([RemedioId]) REFERENCES [Remedios] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [QuantidadexDia] ADD CONSTRAINT [FK_QuantidadexDia_Remedios_RemedioId] FOREIGN KEY ([RemedioId]) REFERENCES [Remedios] ([Id]) ON DELETE NO ACTION;

GO

INSERT INTO [__EFMigrationsHistory]
    ([MigrationId], [ProductVersion])
VALUES
    (N'20190623193958_EstoqueTirandoColunaRemedioID', N'2.2.3-servicing-35854');

GO

INSERT INTO [__EFMigrationsHistory]
    ([MigrationId], [ProductVersion])
VALUES
    (N'20190623194435_RetirandoColumnDesne', N'2.2.3-servicing-35854');

GO

