﻿ALTER TABLE [Remedios] DROP CONSTRAINT [FK_Remedios_Pacientes_IdosoId];

GO

DROP INDEX [IX_Remedios_IdosoId] ON [Remedios];

GO

ALTER TABLE [Remedios] ADD [PacienteId] int NULL;

GO

CREATE INDEX [IX_Remedios_PacienteId] ON [Remedios] ([PacienteId]);

GO

ALTER TABLE [Remedios] ADD CONSTRAINT [FK_Remedios_Pacientes_PacienteId] FOREIGN KEY ([PacienteId]) REFERENCES [Pacientes] ([Id]) ON DELETE NO ACTION;

GO

INSERT INTO [__EFMigrationsHistory]
    ([MigrationId], [ProductVersion])
VALUES
    (N'20190623224523_AlteracoesRemedios', N'2.2.3-servicing-35854');

GO

