﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [AplicacaoUsuarios] (
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL,
    [UserName] nvarchar(150) NULL,
    [Email] nvarchar(150) NULL,
    [Password] nvarchar(150) NULL,
    [NomeCompleto] nvarchar(150) NULL,
    CONSTRAINT [PK_AplicacaoUsuarios] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [AspNetRoles] (
    [Id] nvarchar(450) NOT NULL,
    [Name] nvarchar(256) NULL,
    [NormalizedName] nvarchar(256) NULL,
    [ConcurrencyStamp] nvarchar(max) NULL,
    CONSTRAINT [PK_AspNetRoles] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [AspNetUsers] (
    [Id] nvarchar(450) NOT NULL,
    [UserName] nvarchar(256) NULL,
    [NormalizedUserName] nvarchar(256) NULL,
    [Email] nvarchar(256) NULL,
    [NormalizedEmail] nvarchar(256) NULL,
    [EmailConfirmed] bit NOT NULL,
    [PasswordHash] nvarchar(max) NULL,
    [SecurityStamp] nvarchar(max) NULL,
    [ConcurrencyStamp] nvarchar(max) NULL,
    [PhoneNumber] nvarchar(max) NULL,
    [PhoneNumberConfirmed] bit NOT NULL,
    [TwoFactorEnabled] bit NOT NULL,
    [LockoutEnd] datetimeoffset NULL,
    [LockoutEnabled] bit NOT NULL,
    [AccessFailedCount] int NOT NULL,
    CONSTRAINT [PK_AspNetUsers] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Pacientes] (
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL,
    [Nome] nvarchar(150) NULL,
    [Nacionalidade] nvarchar(150) NULL,
    [Cpf] int NOT NULL,
    [Rg] int NOT NULL,
    [DataNascimento] date NOT NULL,
    [Sexo] int NULL,
    [TipoSanguineo] nvarchar(150) NULL,
    [CuidadoraId] int NULL,
    [DataDeEntrada] date NOT NULL,
    [Observacao] nvarchar(3000) NULL,
    CONSTRAINT [PK_Pacientes] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [AspNetRoleClaims] (
    [Id] int NOT NULL IDENTITY,
    [RoleId] nvarchar(450) NOT NULL,
    [ClaimType] nvarchar(max) NULL,
    [ClaimValue] nvarchar(max) NULL,
    CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [AspNetUserClaims] (
    [Id] int NOT NULL IDENTITY,
    [UserId] nvarchar(450) NOT NULL,
    [ClaimType] nvarchar(max) NULL,
    [ClaimValue] nvarchar(max) NULL,
    CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [AspNetUserLogins] (
    [LoginProvider] nvarchar(450) NOT NULL,
    [ProviderKey] nvarchar(450) NOT NULL,
    [ProviderDisplayName] nvarchar(max) NULL,
    [UserId] nvarchar(450) NOT NULL,
    CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY ([LoginProvider], [ProviderKey]),
    CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [AspNetUserRoles] (
    [UserId] nvarchar(450) NOT NULL,
    [RoleId] nvarchar(450) NOT NULL,
    CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY ([UserId], [RoleId]),
    CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [AspNetUserTokens] (
    [UserId] nvarchar(450) NOT NULL,
    [LoginProvider] nvarchar(450) NOT NULL,
    [Name] nvarchar(450) NOT NULL,
    [Value] nvarchar(max) NULL,
    CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY ([UserId], [LoginProvider], [Name]),
    CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE INDEX [IX_AspNetRoleClaims_RoleId] ON [AspNetRoleClaims] ([RoleId]);

GO

CREATE UNIQUE INDEX [RoleNameIndex] ON [AspNetRoles] ([NormalizedName]) WHERE [NormalizedName] IS NOT NULL;

GO

CREATE INDEX [IX_AspNetUserClaims_UserId] ON [AspNetUserClaims] ([UserId]);

GO

CREATE INDEX [IX_AspNetUserLogins_UserId] ON [AspNetUserLogins] ([UserId]);

GO

CREATE INDEX [IX_AspNetUserRoles_RoleId] ON [AspNetUserRoles] ([RoleId]);

GO

CREATE INDEX [EmailIndex] ON [AspNetUsers] ([NormalizedEmail]);

GO

CREATE UNIQUE INDEX [UserNameIndex] ON [AspNetUsers] ([NormalizedUserName]) WHERE [NormalizedUserName] IS NOT NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190522015222_CreateDatabase', N'2.2.3-servicing-35854');

GO

DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Pacientes]') AND [c].[name] = N'Cpf');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Pacientes] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [Pacientes] ALTER COLUMN [Cpf] nvarchar(11) NULL;

GO

DECLARE @var1 sysname;
SELECT @var1 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[AspNetUserTokens]') AND [c].[name] = N'Name');
IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [AspNetUserTokens] DROP CONSTRAINT [' + @var1 + '];');
ALTER TABLE [AspNetUserTokens] ALTER COLUMN [Name] nvarchar(128) NOT NULL;

GO

DECLARE @var2 sysname;
SELECT @var2 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[AspNetUserTokens]') AND [c].[name] = N'LoginProvider');
IF @var2 IS NOT NULL EXEC(N'ALTER TABLE [AspNetUserTokens] DROP CONSTRAINT [' + @var2 + '];');
ALTER TABLE [AspNetUserTokens] ALTER COLUMN [LoginProvider] nvarchar(128) NOT NULL;

GO

DECLARE @var3 sysname;
SELECT @var3 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[AspNetUserLogins]') AND [c].[name] = N'ProviderKey');
IF @var3 IS NOT NULL EXEC(N'ALTER TABLE [AspNetUserLogins] DROP CONSTRAINT [' + @var3 + '];');
ALTER TABLE [AspNetUserLogins] ALTER COLUMN [ProviderKey] nvarchar(128) NOT NULL;

GO

DECLARE @var4 sysname;
SELECT @var4 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[AspNetUserLogins]') AND [c].[name] = N'LoginProvider');
IF @var4 IS NOT NULL EXEC(N'ALTER TABLE [AspNetUserLogins] DROP CONSTRAINT [' + @var4 + '];');
ALTER TABLE [AspNetUserLogins] ALTER COLUMN [LoginProvider] nvarchar(128) NOT NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190524021740_Paciente', N'2.2.3-servicing-35854');

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190524023039_pacienteAlteracao', N'2.2.3-servicing-35854');

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190528194714_AlteracaoGuidPaciente', N'2.2.3-servicing-35854');

GO

CREATE TABLE [AplicacaoUsuarios] (
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL,
    [UserName] nvarchar(150) NULL,
    [Email] nvarchar(150) NULL,
    [Password] nvarchar(150) NULL,
    [NomeCompleto] nvarchar(150) NULL,
    CONSTRAINT [PK_AplicacaoUsuarios] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [AspNetRoles] (
    [Id] nvarchar(450) NOT NULL,
    [Name] nvarchar(256) NULL,
    [NormalizedName] nvarchar(256) NULL,
    [ConcurrencyStamp] nvarchar(max) NULL,
    CONSTRAINT [PK_AspNetRoles] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [AspNetUsers] (
    [Id] nvarchar(450) NOT NULL,
    [UserName] nvarchar(256) NULL,
    [NormalizedUserName] nvarchar(256) NULL,
    [Email] nvarchar(256) NULL,
    [NormalizedEmail] nvarchar(256) NULL,
    [EmailConfirmed] bit NOT NULL,
    [PasswordHash] nvarchar(max) NULL,
    [SecurityStamp] nvarchar(max) NULL,
    [ConcurrencyStamp] nvarchar(max) NULL,
    [PhoneNumber] nvarchar(max) NULL,
    [PhoneNumberConfirmed] bit NOT NULL,
    [TwoFactorEnabled] bit NOT NULL,
    [LockoutEnd] datetimeoffset NULL,
    [LockoutEnabled] bit NOT NULL,
    [AccessFailedCount] int NOT NULL,
    CONSTRAINT [PK_AspNetUsers] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Pacientes] (
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL,
    [Nome] nvarchar(150) NULL,
    [Nacionalidade] nvarchar(150) NULL,
    [Cpf] nvarchar(11) NULL,
    [Rg] nvarchar(20) NOT NULL,
    [DataNascimento] date NOT NULL,
    [Sexo] int NULL,
    [TipoSanguineo] nvarchar(150) NULL,
    [DataDeEntrada] date NOT NULL,
    [Observacao] nvarchar(3000) NULL,
    CONSTRAINT [PK_Pacientes] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Remedios] (
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL,
    [Nome] nvarchar(150) NULL,
    [DataTermino] date NOT NULL,
    CONSTRAINT [PK_Remedios] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Dosagem] (
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL,
    [Nome] nvarchar(150) NULL,
    [RemedioId] int NOT NULL,
    CONSTRAINT [PK_Dosagem] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Dosagem_Remedios_RemedioId] FOREIGN KEY ([RemedioId]) REFERENCES [Remedios] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [QuantidadexDia] (
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL,
    [Nome] nvarchar(150) NULL,
    [RemedioId] int NOT NULL,
    CONSTRAINT [PK_QuantidadexDia] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_QuantidadexDia_Remedios_RemedioId] FOREIGN KEY ([RemedioId]) REFERENCES [Remedios] ([Id]) ON DELETE CASCADE
);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190623193305_Estoque', N'2.2.3-servicing-35854');

GO

ALTER TABLE [Dosagem] DROP CONSTRAINT [FK_Dosagem_Remedios_RemedioId];

GO

ALTER TABLE [QuantidadexDia] DROP CONSTRAINT [FK_QuantidadexDia_Remedios_RemedioId];

GO

DECLARE @var5 sysname;
SELECT @var5 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[QuantidadexDia]') AND [c].[name] = N'RemedioId');
IF @var5 IS NOT NULL EXEC(N'ALTER TABLE [QuantidadexDia] DROP CONSTRAINT [' + @var5 + '];');
ALTER TABLE [QuantidadexDia] ALTER COLUMN [RemedioId] int NULL;

GO

DECLARE @var6 sysname;
SELECT @var6 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Dosagem]') AND [c].[name] = N'RemedioId');
IF @var6 IS NOT NULL EXEC(N'ALTER TABLE [Dosagem] DROP CONSTRAINT [' + @var6 + '];');
ALTER TABLE [Dosagem] ALTER COLUMN [RemedioId] int NULL;

GO

ALTER TABLE [Dosagem] ADD CONSTRAINT [FK_Dosagem_Remedios_RemedioId] FOREIGN KEY ([RemedioId]) REFERENCES [Remedios] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [QuantidadexDia] ADD CONSTRAINT [FK_QuantidadexDia_Remedios_RemedioId] FOREIGN KEY ([RemedioId]) REFERENCES [Remedios] ([Id]) ON DELETE NO ACTION;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190623193958_EstoqueTirandoColunaRemedioID', N'2.2.3-servicing-35854');

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190623194435_RetirandoColumnDesne', N'2.2.3-servicing-35854');

GO

ALTER TABLE [Dosagem] DROP CONSTRAINT [FK_Dosagem_Remedios_RemedioId];

GO

ALTER TABLE [QuantidadexDia] DROP CONSTRAINT [FK_QuantidadexDia_Remedios_RemedioId];

GO

DROP INDEX [IX_QuantidadexDia_RemedioId] ON [QuantidadexDia];

GO

DROP INDEX [IX_Dosagem_RemedioId] ON [Dosagem];

GO

DECLARE @var7 sysname;
SELECT @var7 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[QuantidadexDia]') AND [c].[name] = N'RemedioId');
IF @var7 IS NOT NULL EXEC(N'ALTER TABLE [QuantidadexDia] DROP CONSTRAINT [' + @var7 + '];');
ALTER TABLE [QuantidadexDia] DROP COLUMN [RemedioId];

GO

DECLARE @var8 sysname;
SELECT @var8 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Dosagem]') AND [c].[name] = N'RemedioId');
IF @var8 IS NOT NULL EXEC(N'ALTER TABLE [Dosagem] DROP CONSTRAINT [' + @var8 + '];');
ALTER TABLE [Dosagem] DROP COLUMN [RemedioId];

GO

ALTER TABLE [Remedios] ADD [DosagemId] int NOT NULL DEFAULT 0;

GO

ALTER TABLE [Remedios] ADD [IdosoId] int NOT NULL DEFAULT 0;

GO

ALTER TABLE [Remedios] ADD [PacienteId] int NULL;

GO

ALTER TABLE [Remedios] ADD [QuantidadeId] int NOT NULL DEFAULT 0;

GO

ALTER TABLE [Remedios] ADD [QuantidadexDiaId] int NULL;

GO

CREATE INDEX [IX_Remedios_DosagemId] ON [Remedios] ([DosagemId]);

GO

CREATE INDEX [IX_Remedios_PacienteId] ON [Remedios] ([PacienteId]);

GO

CREATE INDEX [IX_Remedios_QuantidadexDiaId] ON [Remedios] ([QuantidadexDiaId]);

GO

ALTER TABLE [Remedios] ADD CONSTRAINT [FK_Remedios_Dosagem_DosagemId] FOREIGN KEY ([DosagemId]) REFERENCES [Dosagem] ([Id]) ON DELETE CASCADE;

GO

ALTER TABLE [Remedios] ADD CONSTRAINT [FK_Remedios_Pacientes_PacienteId] FOREIGN KEY ([PacienteId]) REFERENCES [Pacientes] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Remedios] ADD CONSTRAINT [FK_Remedios_QuantidadexDia_QuantidadexDiaId] FOREIGN KEY ([QuantidadexDiaId]) REFERENCES [QuantidadexDia] ([Id]) ON DELETE NO ACTION;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190623220249_AlteracaoRemedio', N'2.2.3-servicing-35854');

GO

ALTER TABLE [Remedios] DROP CONSTRAINT [FK_Remedios_Pacientes_PacienteId];

GO

ALTER TABLE [Remedios] DROP CONSTRAINT [FK_Remedios_QuantidadexDia_QuantidadexDiaId];

GO

DROP INDEX [IX_Remedios_PacienteId] ON [Remedios];

GO

DROP INDEX [IX_Remedios_QuantidadexDiaId] ON [Remedios];

GO

DECLARE @var9 sysname;
SELECT @var9 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Remedios]') AND [c].[name] = N'PacienteId');
IF @var9 IS NOT NULL EXEC(N'ALTER TABLE [Remedios] DROP CONSTRAINT [' + @var9 + '];');
ALTER TABLE [Remedios] DROP COLUMN [PacienteId];

GO

DECLARE @var10 sysname;
SELECT @var10 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Remedios]') AND [c].[name] = N'QuantidadexDiaId');
IF @var10 IS NOT NULL EXEC(N'ALTER TABLE [Remedios] DROP CONSTRAINT [' + @var10 + '];');
ALTER TABLE [Remedios] DROP COLUMN [QuantidadexDiaId];

GO

CREATE INDEX [IX_Remedios_IdosoId] ON [Remedios] ([IdosoId]);

GO

CREATE INDEX [IX_Remedios_QuantidadeId] ON [Remedios] ([QuantidadeId]);

GO

ALTER TABLE [Remedios] ADD CONSTRAINT [FK_Remedios_Pacientes_IdosoId] FOREIGN KEY ([IdosoId]) REFERENCES [Pacientes] ([Id]) ON DELETE CASCADE;

GO

ALTER TABLE [Remedios] ADD CONSTRAINT [FK_Remedios_QuantidadexDia_QuantidadeId] FOREIGN KEY ([QuantidadeId]) REFERENCES [QuantidadexDia] ([Id]) ON DELETE CASCADE;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190623220621_ReconfigTable', N'2.2.3-servicing-35854');

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190623224220_RemedionoPaciente', N'2.2.3-servicing-35854');

GO

