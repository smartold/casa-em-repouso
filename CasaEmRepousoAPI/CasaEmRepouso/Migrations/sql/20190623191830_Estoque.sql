﻿CREATE TABLE [Remedios]
(
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL,
    [Nome] nvarchar(150) NULL,
    [DataTermino] date NOT NULL,
    CONSTRAINT [PK_Remedios] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Dosagem]
(
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL,
    [Nome] nvarchar(150) NULL,
    [RemedioId] int NOT NULL,
    CONSTRAINT [PK_Dosagem] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Dosagem_Remedios_RemedioId] FOREIGN KEY ([RemedioId]) REFERENCES [Remedios] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [QuantidadexDia]
(
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL,
    [Nome] nvarchar(150) NULL,
    [RemedioId] int NOT NULL,
    CONSTRAINT [PK_QuantidadexDia] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_QuantidadexDia_Remedios_RemedioId] FOREIGN KEY ([RemedioId]) REFERENCES [Remedios] ([Id]) ON DELETE CASCADE
);

