﻿ALTER TABLE [Remedios] DROP CONSTRAINT [FK_Remedios_Dosagem_DosagemId];

GO

ALTER TABLE [Remedios] DROP CONSTRAINT [FK_Remedios_QuantidadexDia_QuantidadeId];

GO

DROP TABLE [Dosagem];

GO

DROP TABLE [QuantidadexDia];

GO

DROP INDEX [IX_Remedios_DosagemId] ON [Remedios];

GO

DROP INDEX [IX_Remedios_QuantidadeId] ON [Remedios];

GO

INSERT INTO [__EFMigrationsHistory]
    ([MigrationId], [ProductVersion])
VALUES
    (N'20190623225519_PacienteComRemedio', N'2.2.3-servicing-35854');

GO

