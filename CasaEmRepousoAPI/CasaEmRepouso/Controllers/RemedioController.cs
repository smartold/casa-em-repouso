using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using AutoMapper.Configuration;
using CasaEmRepouso.Filtros;
using CasaEmRepouso.Models;
using CasaEmRepousoAPI.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CasaEmRepouso.Controllers
{
    [Route("api/remedio")]
    [ApiController]
    public class RemedioController : ControllerBase
    {
        private readonly AutenticacaoContext _context = null;

        private readonly IRemedioRepository _remedioRepository;

        public RemedioController(AutenticacaoContext context, IRemedioRepository remedioRepository)
        {
            _context = context;
            _remedioRepository = remedioRepository;

        }


        [HttpPost]
        [Route("salvar")]
        public async Task<ActionResult<Remedio>> SalvarPaciente(Remedio remedio)
        {
            try
            {
                //  não existe retorno pra isso
                await _context.Remedios.AddAsync(remedio);

                await _context.SaveChangesAsync(); // qualquer alteração nas entidades usando entity framework precisam ser commitadas com essa parada

                // nesse caso vc retorna o próprio paciente, o entity framework usa ele de ref e altera as infos sozinho (guid, id e etc)
                return Ok(remedio);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpGet]
        [Route("obterListagemDosagem")]
        public async Task<ActionResult<Dosagem>> ObterListaDosagem()
        {

            var listaDosagem = await _remedioRepository.ObterListaDosagem();

            return Ok(listaDosagem);


        }

        [HttpGet]
        [Route("obterListagemQuantidade")]
        public async Task<ActionResult<QuantidadexDia>> ObterListaQuantidade()
        {

            var listaQuantidade = await _remedioRepository.ObterListaQuantidade();

            return Ok(listaQuantidade);

        }


        [HttpGet]
        [Route("obterListaRemedioIdosoId")]

        public async Task<ActionResult<Remedio>> ObterListaRemedio(int id)
        {

            var listaRemedios = await _remedioRepository.ObterListaRemedio(id);

            return Ok(listaRemedios);

        }








    }

}

