using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using AutoMapper.Configuration;
using CasaEmRepouso.Filtros;
using CasaEmRepouso.Models;
using CasaEmRepousoAPI.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CasaEmRepouso.Controllers
{
    [Route("api/paciente")]
    [ApiController]
    public class PacienteController : ControllerBase
    {
        private readonly AutenticacaoContext _context = null;

        private readonly IPacienteRepository _pacienteRepository;

        public PacienteController(AutenticacaoContext context, IPacienteRepository pacienteRepository)
        {
            _context = context;
            _pacienteRepository = pacienteRepository;

        }

        [HttpGet]
        [Route("identificar")]
        public async Task<IActionResult> IdentificarPaciente(string cpf, string nome) // vc tá fazendo um get recebendo objeto
        {
            try
            {
                PacienteFilter filter = new PacienteFilter(cpf, nome);
                var listaPaciente = await _pacienteRepository.IdentificarPaciente(filter);

                return Ok(listaPaciente);
            }
            catch
            {
                throw;
            }
        }


        [HttpPost]
        [Route("salvar")]
        public async Task<ActionResult<Paciente>> SalvarPaciente(Paciente paciente)
        {
            try
            {
                //  não existe retorno pra isso
                await _context.Pacientes.AddAsync(paciente);

                await _context.SaveChangesAsync(); // qualquer alteração nas entidades usando entity framework precisam ser commitadas com essa parada

                // nesse caso vc retorna o próprio paciente, o entity framework usa ele de ref e altera as infos sozinho (guid, id e etc)
                return Ok(paciente);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpGet]
        [Route("obterListagemPacientes")]

        public async Task<ActionResult<Paciente>> ObterListagemPacientes()
        {
            var listaPaciente = await _pacienteRepository.ObterListagemPacientes();

            return Ok(listaPaciente);
        }

        [HttpGet]
        [Route("guid")]

        public async Task<Paciente> ObterPacientePorGuid(string guid)
        {
            try
            {
                var paciente = await _pacienteRepository.ObterPacientePorGuid(guid);

                if (paciente == null)
                {
                    throw new Exception();
                }

                return paciente;

            }
            catch (Exception)
            {

                throw;
            }

        }

        [HttpDelete]
        public async Task<IActionResult> Excluir(string guid)
        {
            try
            {
                await _pacienteRepository.Excluir(guid);
                return Ok();
            }
            catch
            {

                throw;
            }
        }

        [HttpPut]
        [Route("atualizar")]
        public bool Atualizar(Paciente paciente)
        {
            var response = _pacienteRepository.AtualizarPaciente(paciente);

            return response;
        }

    }

}

