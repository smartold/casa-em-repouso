// using System;
// using System.Linq;
// using System.Threading.Tasks;
// using Microsoft.AspNetCore.Authorization;
// using Microsoft.AspNetCore.Mvc;

// namespace Doctor.Api.Controllers
// {
//     [Produces("application/json")]
//     [Route("api/registro")]
//     public class RegistroCuidadoraController : ControllerBase
//     {
//         private readonly ITutorialHomeService _tutorialHomeService;

//         public TutorialHomeController(ITutorialHomeService tutorialHomeService,
//             IErroService erroService,
//             IMapper mapper,
//             INotificacao notificacao) : base(erroService, mapper, notificacao)
//         {
//             _tutorialHomeService = tutorialHomeService;

//         }

//         [HttpGet]
//         [Authorize]
//         public async Task<IActionResult> ObterListaTutorial()
//         {
//             try
//             {
//                 var listaTutorial = await _tutorialHomeService.ObterListaTutorial();

//                 return CriarResponse(listaTutorial);
//             }
//             catch (Exception ex)
//             {
//                 return await TratarErro(ex);
//             }
//         }

//         [HttpGet]
//         [Route("guid")]
//         [Authorize(Policy = Policies.POLICY_FRANQUIA)]
//         public async Task<IActionResult> ObterTutorialPorGuid(string guid)
//         {
//             try
//             {
//                 var tutorial = await _tutorialHomeService.ObterTutorialPorGuid(guid);

//                 if (tutorial == null)
//                     throw new EntidadeNaoEncontradaException();

//                 return CriarResponse(tutorial);
//             }
//             catch (Exception ex)
//             {
//                 return await TratarErro(ex);
//             }
//         }

//         [HttpPost]
//         [Authorize(Policy = Policies.POLICY_ATENDENTE)]
//         public async Task<IActionResult> Salvar([FromBody] TutorialHomeViewModel TutorialHomeViewModel)
//         {
//             var tutorialHome = new TutorialHome();

//             try
//             {
//                 if (ModelState.IsValid)
//                 {
//                     tutorialHome = _mapper.Map<TutorialHomeViewModel, TutorialHome>(TutorialHomeViewModel);

//                     await _tutorialHomeService.Salvar(tutorialHome);
//                 }
//                 else
//                     throw new EntidadeInvalidaException();

//                 return CriarResponse(tutorialHome);
//             }
//             catch (Exception ex)
//             {
//                 return await TratarErro(ex);
//             }
//         }

//         [HttpPut]
//         [Authorize(Policy = Policies.POLICY_ATENDENTE)]
//         public async Task<IActionResult> Atualizar([FromBody] TutorialHomeViewModel TutorialHomeViewModel)
//         {
//             var TutorialHome = new TutorialHome();

//             try
//             {
//                 if (ModelState.IsValid)
//                 {
//                     TutorialHome = _mapper.Map<TutorialHomeViewModel, TutorialHome>(TutorialHomeViewModel);

//                     await _tutorialHomeService.Atualizar(TutorialHome);
//                 }
//                 else
//                     throw new EntidadeInvalidaException();

//                 return CriarResponse(TutorialHome);
//             }
//             catch (Exception ex)
//             {
//                 return await TratarErro(ex);
//             }
//         }

//         [HttpDelete]
//         [Authorize(Policy = Policies.POLICY_ATENDENTE)]
//         public async Task<IActionResult> Excluir(string guid)
//         {
//             try
//             {
//                 await _tutorialHomeService.Excluir(guid);

//                 return new NoContentResult();
//             }
//             catch (Exception ex)
//             {
//                 return await TratarErro(ex);
//             }
//         }

//         [HttpPost]
//         [Route("salvarfoto")]
//         [Authorize(Policy = Policies.POLICY_FRANQUIA)]
//         public async Task<IActionResult> PostImagem()
//         {
//             var imagem = Request.Form.Files;
//             try
//             {
//                 if (imagem != null && imagem.Count > 0 && imagem[0].ContentType.ToLower().StartsWith("image/"))
//                 {

//                     var stringUrl = "";

//                     stringUrl = await _tutorialHomeService.UploadImagem(imagem.SingleOrDefault().OpenReadStream());

//                     return CriarResponse(stringUrl);
//                 }
//                 else
//                     return BadRequest();
//             }
//             catch (Exception ex)
//             {
//                 return await TratarErro(ex);
//             }
//         }
//     }
// }