﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CasaEmRepouso.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CasaEmRepouso.Controllers
{
    [Route("api/registro")]
    [ApiController]
    public class AplicacaoUsuarioController : ControllerBase
    {

        private readonly AutenticacaoContext _context;

        public AplicacaoUsuarioController(AutenticacaoContext context)
        {
            _context = context;

        }
        [HttpPost]
        public async Task<Object> PostAplicacaoUsuario(AplicacaoUsuario model)
        {
            var aplicacaoUsuario = new AplicacaoUsuario()
            {
                UserName = model.UserName,
                Email = model.Email,
                Password = model.Password,
                NomeCompleto = model.NomeCompleto

            };

            try
            {
                var resultado = await _context.AplicacaoUsuarios.AddAsync(aplicacaoUsuario);
                return Ok(resultado);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}