using System;

namespace CasaEmRepouso.Filtros
{
    public class BaseFilter
    {
        public int Id { get; set; }
        public Guid Guid { get; set; }
    }
}