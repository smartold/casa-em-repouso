namespace CasaEmRepouso.Filtros
{
    public class PacienteFilter : BaseFilter
    {
        public PacienteFilter(string cpfRef, string nomeRef)
        {
            cpf = cpfRef;
            nome = nomeRef;
        }
        public string nome { get; set; }
        public string cpf { get; set; }

        public void Validar()
        {
            if (ValidarString(nome))
                nome = null;

            if (ValidarString(cpf))
                cpf = null;

        }

        public bool ValidarString(string validar)
        {
            return validar == "undefined" || validar == "null" || validar == "invalid";
        }

    }

}
