using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace CasaEmRepouso.Models
{
    public class Paciente : BaseEntity
    {

        [Column(TypeName = "nvarchar(150)")]
        public string Nome { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string Nacionalidade { get; set; }

        [Column(TypeName = "nvarchar(11)")]
        public string Cpf { get; set; }

        [Column(TypeName = "nvarchar(20)")]
        public int Rg { get; set; }

        [Column(TypeName = "date")]
        public DateTime DataNascimento { get; set; }

        [Column(TypeName = "int")]
        public int? Sexo { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string TipoSanguineo { get; set; }

        [Column(TypeName = "date")]
        public DateTime DataDeEntrada { get; set; }

        [Column(TypeName = "nvarchar(3000)")]
        public string Observacao { get; set; }

        #region Relacionamentos
        public List<Remedio> Remedios { get; set; }

        #endregion

    }
}
