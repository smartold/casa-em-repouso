using System.Collections.Generic;
using CasaEmRepouso.Models;
using Microsoft.AspNetCore.Identity;

namespace Doctor.Domain.Entities
{
    public class Evento : BaseEntity
    {
        public string Imagem { get; set; }
        public string Titulo { get; set; }
        public string Texto { get; set; }

    }
}