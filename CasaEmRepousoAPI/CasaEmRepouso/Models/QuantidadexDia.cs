using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace CasaEmRepouso.Models
{
    public class QuantidadexDia : BaseEntity
    {
        [Column(TypeName = "nvarchar(150)")]
        public string Quantidade { get; set; }



    }
}
