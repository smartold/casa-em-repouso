using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace CasaEmRepouso.Models
{
    public class Remedio : BaseEntity
    {
        [Column(TypeName = "nvarchar(150)")]
        public string Nome { get; set; }

        [Column(TypeName = "date")]
        public DateTime DataTermino { get; set; }

        [Column(TypeName = "int")]
        public int DosagemId { get; set; }

        [Column(TypeName = "int")]
        public int QuantidadeId { get; set; }

        [Column(TypeName = "int")]
        public int IdosoId { get; set; }


        #region Relacionamentos
        public Dosagem Dosagem { get; set; }
        public QuantidadexDia Quantidade { get; set; }
        public Paciente Paciente { get; set; }

        #endregion


    }
}