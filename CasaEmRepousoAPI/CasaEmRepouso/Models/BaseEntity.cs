using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace CasaEmRepouso.Models
{
    public class BaseEntity
    {
        public BaseEntity()
        {
            this.Guid = Guid.NewGuid();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Guid { get; set; }

        [Column(TypeName = "int")]
        public int Id { get; set; }

    }
}
