﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CasaEmRepouso.Models
{
    public class AplicacaoUsuario : BaseEntity
    {
        [Column(TypeName = "nvarchar(150)")]
        public string UserName { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string Email { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string Password { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string NomeCompleto { get; set; }

    }
}
