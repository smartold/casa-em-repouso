using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace CasaEmRepouso.Models
{
    public class Dosagem : BaseEntity
    {
        [Column(TypeName = "nvarchar(150)")]
        public string DosagemMg { get; set; }


    }
}