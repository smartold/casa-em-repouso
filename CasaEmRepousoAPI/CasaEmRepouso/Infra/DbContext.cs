﻿using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace CasaEmRepouso.Models
{
    public class AutenticacaoContext : IdentityDbContext
    {

        private IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;


        public AutenticacaoContext(DbContextOptions options, IConfiguration configuration, IHttpContextAccessor httpContextAccessor) : base(options)
        {
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;

        }

        public DbSet<AplicacaoUsuario> AplicacaoUsuarios { get; set; }
        public DbSet<Paciente> Pacientes { get; set; }
        public DbSet<Remedio> Remedios { get; set; }

        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_configuration.GetConnectionString("IdentityConnection"));
            }
        }
    }
}
