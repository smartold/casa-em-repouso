﻿using CasaEmRepouso.Filtros;
using CasaEmRepouso.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CasaEmRepousoAPI.Interface
{
    public interface IPacienteRepository
    {
        Task<List<Paciente>> ObterListagemPacientes();
        Task<List<Paciente>> IdentificarPaciente(PacienteFilter filter);
        Task<Paciente> ObterPacientePorGuid(string guid);
        Task Excluir(string guid);
        bool AtualizarPaciente(Paciente paciente);


    }
}
