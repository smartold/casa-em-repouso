using CasaEmRepouso.Filtros;
using CasaEmRepouso.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CasaEmRepousoAPI.Interface
{
    public interface IRemedioRepository
    {

        Task<List<QuantidadexDia>> ObterListaQuantidade();
        Task<List<Dosagem>> ObterListaDosagem();
        Task<List<Remedio>> ObterListaRemedio(int id);

        Task Excluir(string guid);

    }
}
