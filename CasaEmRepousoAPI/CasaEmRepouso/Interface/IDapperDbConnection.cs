using System;

namespace CasaEmRepouso.CasaEmRepousoAPI
{
    public interface IDapperDbConnection : IDisposable
    {
        void OpenConnection();
        void OpenReportConnection();
    }

}
